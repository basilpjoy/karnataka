var webpack = require('webpack');
module.exports = {
  context: __dirname,
  entry: {
        'home': "./assets/js/home",
		'schemes': "./assets/js/schemes",
		'single-scheme': "./assets/js/single-scheme",
		'notifications': "./assets/js/notifications",
		'single-notification': "./assets/js/single-notification",
		'gallery': "./assets/js/gallery",
		'single-gallery': "./assets/js/single-gallery",
		'circular': "./assets/js/circular",
		'pages': "./assets/js/pages",
		'hostels': "./assets/js/hostels",
		'single-hostels': "./assets/js/single-hostels",
		'residential-schools': "./assets/js/residential-schools",
		'rti': "./assets/js/rti",
		'component-plan': "./assets/js/component-plan",
		'single-component-plan': "./assets/js/single-component-plan",
		'organisational-structure': "./assets/js/organisational-structure",
		'report': "./assets/js/report",
		'pin': "./assets/js/pin",
		'contactus': "./assets/js/contactus",
		'single-pin': "./assets/js/single-pin",
		'show-hostel': "./assets/js/show-hostel",
		'pin-hostel': "./assets/js/pin-hostel",
		'pin-community-halls':"./assets/js/pin-community-halls",
		'single-community': "./assets/js/single-community",
    'single-pin-colony': "./assets/js/single-pin-colony",
    },
  output: {
    path: __dirname+"/assets/js/packages",
    filename: "[name].min.js"
  },
   module: {
        loaders: [
            { test: /\.css$/,loader: 'style-loader!css-loader?sourceMap'},
			{ test: /\.png$/, loader: "url-loader?limit=100000" },
			{ test: /\.jpg$/, loader: "url-loader?limit=100000" },
            { test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,loader : 'file-loader'},
            { test: /\.(png|woff)$/, loader: 'url-loader?limit=100000' }
        ]
    },
  plugins: [
	new webpack.ProvidePlugin({$: "jquery",jQuery: "jquery","window.jQuery": "jquery"
}),
  ]
};
