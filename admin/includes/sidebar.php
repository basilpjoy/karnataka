<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header<?php echo ($page=="dashboard")?' active':''; ?>"><a href="?page=dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
      <li class="treeview<?php echo ($page=="pages"||$page=="add-page")?' active':''; ?>"><a href=""><i class="fa fa-list"></i> <span>Pages</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=pages"> All Pages</a></li>
          <li><a href="?page=add-page"> Add New</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="add-notifications"||$page=="notifications")?' active':''; ?>"><a href=""><i class="fa fa-comments"></i> <span>Notifications</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=notifications"> View All</a></li>
          <li><a href="?page=add-notification"> Add New</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="add-scheme"||$page=="schemes"||$page=="scheme-files")?' active':''; ?>"><a href=""><i class="fa fa-sitemap"></i><span>Schemes</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=schemes"> View All</a></li>
          <li><a href="?page=add-scheme"> Add New</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="add-circular"||$page=="circulars")?' active':''; ?>"><a href=""><i class="fa fa-file-pdf-o"></i><span>Circulars</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=circulars"> View All</a></li>
          <li><a href="?page=add-circular"> Add New</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="rti" || $page=="rti_main")?' active':''; ?>"><a href=""><i class="fa fa-files-o"></i><span>RTI</span></a>
        <ul class="treeview-menu">
            <li><a href="?page=rti_main"> RTI Main</a></li>
            <li><a href="?page=rti"> RTI Files</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="reports" || $page=="report-categories")?' active':''; ?>"><a href=""><i class="fa fa-file-text"></i> <span>Reports</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=reports"> Reports</a></li>
          <li><a href="?page=report-categories"> Categories</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="pdf-files"||$page=="websites")?' active':''; ?>"><a href=""><i class="fa fa-link"></i><span>Usefull Links</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=websites"> Website Links</a></li>
          <li><a href="?page=pdf-files"> PDF Files</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="hostels" || $page=="hostel-categories")?' active':''; ?>"><a href=""><i class="fa fa-building"></i> <span>Hostels</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=hostels"> Hostels</a></li>
          <li><a href="?page=hostel-categories"> Categories</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="colonies" || $page=="geo_categories" || $page=="geo_sub_categories" || $page=="basic_livings" || $page=="community_halls" || $page=="colony-images" || $page=="community_halls-images")?' active':''; ?>"><a href=""><i class="fa fa-map-marker"></i> <span>Colonies</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=colonies"> Colonies</a></li>
          <li><a href="?page=community_halls"> Community Halls</a></li>
          <li><a href="?page=geo_categories"> Geo Categories</a></li>
          <li><a href="?page=geo_sub_categories"> Grama Panchayath</a></li>
          <li><a href="?page=basic_livings"> Basic Livings</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="component-plan" || $page=="add-plan")?' active':''; ?>"><a href=""><i class="fa fa-map"></i> <span>Componant Plans</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=component-plans"> View All</a></li>
          <li><a href="?page=add-plan"> Add New</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="contact-offices" || $page=="contact-categories")?' active':''; ?>"><a href=""><i class="fa fa-users"></i> <span>Contact Officers</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=contact-offices"> Officers</a></li>
          <li><a href="?page=contact-categories"> Categories</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="add-gallery-category" || $page=="create-gallery" || $page=="home-slider")?' active':''; ?>"><a href=""><i class="fa fa-picture-o"></i> <span>Gallery</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=add-gallery-category"> Gallery Category</a></li>
          <li><a href="?page=home-slider"> Home Slider</a></li>
        </ul>
      </li>
      <li class="treeview<?php echo ($page=="add-district" || $page=="add-taluk")?' active':''; ?>"><a href=""><i class="fa fa-cogs"></i> <span>Settings</span></a>
        <ul class="treeview-menu">
          <li><a href="?page=add-district"> Districts</a></li>
          <li><a href="?page=add-taluk"> Taluks</a></li>
        </ul>
      </li>
    </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
