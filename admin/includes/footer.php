 <footer class="main-footer">
        <div class="pull-right hidden-xs">
          corecipher
        </div>
         <strong>Copyright &copy; <?php echo date("Y") ?> <a href="#">Spacegap</a>.</strong> All rights reserved.
      </footer>
	 <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo SITE_ROOT ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo SITE_ROOT ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo SITE_ROOT ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SITE_ROOT ?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo SITE_ROOT ?>dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo SITE_ROOT ?>dist/js/demo.js"></script>
  </body>
</html>