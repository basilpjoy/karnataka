<?php
@session_start();
include_once("../../config/site_root.php");
$objCommunity                 =	new community_hall();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['gcat_id'],$_POST['ch_name'])&&$_POST['gcat_id']!=""&&$_POST['ch_name']!=""){
	$_POST['latitude']    =   $objCommon->map2Degree($_POST['latitude']);
	$_POST['longitude']    =   $objCommon->map2Degree($_POST['longitude']);
	if($editId){
		$objCommunity->update($_POST,"colony_id=".$editId);
		$objCommon->addMsg("Community Hall details updated successfully",1);
	}else{
		//$_POST['colony_alias']			=	$objCommon->getAlias($_POST['colony_name']);
		$objCommunity->insert($_POST);
		$objCommon->addMsg("Community Hall details added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>