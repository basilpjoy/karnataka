<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new useful_links();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['link_title'])&&$_POST['link_title']!=""){
	if($_POST['link_category']==2){
		if($_FILES['pdfFile']['name']){
			if(pathinfo($_FILES['pdfFile']['name'], PATHINFO_EXTENSION)=='pdf'){
				$filename	=	$_POST['link_url']	=	$objCommon->getAlias($_POST['link_title']).time().'.'.pathinfo($_FILES['pdfFile']['name'], PATHINFO_EXTENSION);
				move_uploaded_file($_FILES['pdfFile']['tmp_name'], DIR_ROOT . 'assets/uploads/pdf/' . $filename);
				if($editId) {
					$currRowDetails = $objMain->getRow("link_id=$editId");
					if(file_exists(DIR_ROOT . 'assets/uploads/pdf/' . $currRowDetails['link_url'])){
						unlink(DIR_ROOT . 'assets/uploads/pdf/' . $currRowDetails['link_url']);
					}
				}
			}else{
				$objCommon->addMsg("Only PDF files will support",0);
				header("location:".$_SERVER['HTTP_REFERER']);
				exit();
			}
		}
	}
	if($editId){
		if($_POST['link_category']==2&&$_FILES['pdfFile']['name']==""){
			$currRowDetails = $objMain->getRow("link_id=$editId");
			$_POST['link_url']	=	$currRowDetails['link_url'];
		}
		$objMain->update($_POST,"link_id=".$editId);
		$objCommon->addMsg("Website  updated successfully",1);
	}else{
		$objMain->insert($_POST);
		$objCommon->addMsg("Website  added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>