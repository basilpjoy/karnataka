<?php
@session_start();
include_once("../../config/site_root.php");
$objCommon					=	new common();
$objGalleryImages			  	=	new gallery_images();
$dId						=	$objCommon->esc($_GET['dId']);
if($dId){
	$currentImage		=	$objGalleryImages->getRow("gi_id=".$dId);
	if(file_exists(DIR_ROOT."assets/uploads/gallery/".$currentImage['gi_url'])){
		$imageName	=	explode("/",$currentImage['gi_url']);
		$folderName	=	$imageName[0];
		$imageName	=	$imageName[1];
		unlink(DIR_ROOT."assets/uploads/gallery/".$folderName."/thumb/".$imageName);
		unlink(DIR_ROOT."assets/uploads/gallery/".$folderName."/original/".$imageName);
		unlink(DIR_ROOT."assets/uploads/gallery/".$folderName."/".$imageName);
	}
	$objGalleryImages->delete("gi_id=".$dId);
	$objCommon->addMsg("Image  deleted successfully",1);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>