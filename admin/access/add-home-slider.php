<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new home_slider();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
$path						=	DIR_ROOT.'assets/uploads/home-slider/';
$valid_formats				= 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
if
($_FILES['hs_img']['tmp_name'] || $editId !=''){
	$name 				=	$_FILES['hs_img']['name'];
	$size 				=	$_FILES['hs_img']['size'];
	$_POST['hs_title']				=	$objMain->esc($_POST['hs_title']);
	$_POST['hs_order']				=	$objMain->esc($_POST['hs_order']);
	if(strlen($name))
	{
		$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
		if(in_array($ext,$valid_formats))
		{
			if($size<(3072*10240))
			{
				$randomDigit					=	$objCommon->randStrGenDigit(4);
				$time							=	time();
				$actual_image_name_ext_no		=	$time.'_'.$randomDigit;
				$actual_image_name				= 	$time.'_'.$randomDigit.".".$ext;
				$tmpName = $_FILES['hs_img']['tmp_name'];
				list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName);
				if(move_uploaded_file($_FILES['hs_img']['tmp_name'], $path.$actual_image_name)){
					$_POST['hs_img']				=	$actual_image_name;
					if($editId==''){
						$objMain->insert($_POST);
						$objCommon->addMsg("Slider images  added successfully",1);
					}
				}
			}
		}
	};
	if($editId){
		$objMain->update($_POST,"hs_id=".$editId );
		$objCommon->addMsg("Slider images  updated successfully",1);
	}
	header("location:../index.php?page=home-slider");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>