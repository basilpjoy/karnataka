<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new geo_sub_categories();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['gscat_name'])&&$_POST['gscat_name']!=""){
	if($editId){
		$objMain->update($_POST,"gscat_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$objMain->insert($_POST);
		$objCommon->addMsg("Category  added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);