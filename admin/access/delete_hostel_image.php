<?php
@session_start();
include_once("../../config/site_root.php");
$objCommon					=	new common();
$objHostelImages			  	=	new hostel_images();
$dId						=	$objCommon->esc($_GET['dId']);
if($dId){
	$currentImage		=	$objHostelImages->getRow("hi_id=".$dId);
	if(count($currentImage)>1) {
		if(file_exists(DIR_ROOT."assets/uploads/hostelImages/".$currentImage['hi_image'])){
			$imageName	=	explode("/",$currentImage['hi_image']);
			$folderName	=	$imageName[0];
			$imageName	=	$imageName[1];
			unlink(DIR_ROOT."assets/uploads/hostelImages/".$folderName."/thumb/".$imageName);
			unlink(DIR_ROOT."assets/uploads/hostelImages/".$folderName."/original/".$imageName);
			unlink(DIR_ROOT."assets/uploads/hostelImages/".$folderName."/".$imageName);
		}
		$objHostelImages->delete("hi_id=" . $dId);
		$objCommon->addMsg("Image  deleted successfully", 1);
	}
}
header("location:".$_SERVER['HTTP_REFERER']);