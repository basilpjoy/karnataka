<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new hostel_categories();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['cat_name'])&&$_POST['cat_name']!=""){
	if($editId){
		$objMain->update($_POST,"cat_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$_POST['cat_alias']	=	$objCommon->getAlias($_POST['cat_name']);
		$objMain->insert($_POST);
		$objCommon->addMsg("Category  added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);