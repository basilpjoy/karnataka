<?php
@session_start();
include_once("../../config/site_root.php");
$objNotification		  =	new notifications();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['notification_title'],$_POST['notification_content'])&&$_POST['notification_title']!=""&&$_POST['notification_content']!=""){
	$_POST['notification_date']				=	$objCommon->backEndDate($_POST['notification_date']);
	$_POST['notification_status']			=	(isset($_POST['notification_status']))?1:2;
	$_POST['status']   =	1;
	if($_FILES['notificationFile']['name']){
		$extention	=	pathinfo($_FILES['notificationFile']['name'], PATHINFO_EXTENSION);
		$existFiles	=	array("pdf","jpg","png","jpeg");
		if(in_array($extention, $existFiles)){
			echo $filename	=	$_POST['notification_file']	=	$objCommon->getAlias($_POST['notification_title']).time().'.'.$extention;
			move_uploaded_file($_FILES['notificationFile']['tmp_name'], DIR_ROOT . 'assets/uploads/reports/' . $filename);
			if($editId) {
				$currRowDetails = $objNotification->getRow("note_id=$editId");
				if(file_exists(DIR_ROOT . 'assets/uploads/reports/' . $currRowDetails['notification_file'])){
					unlink(DIR_ROOT . 'assets/uploads/reports/' . $currRowDetails['notification_file']);
				}
			}
		}else{
			$objCommon->addMsg("Only PDF or images files will support",0);
			header("location:".$_SERVER['HTTP_REFERER']);
			exit();
		}
	}
	if($editId){
		$objNotification->update($_POST,"note_id=".$editId);
		$objCommon->addMsg("Notification updated successfully",1);
	}else{
		$_POST['notification_alias']			=	$objCommon->getAlias($_POST['notification_title']);
		$objNotification->insert($_POST);
		$objCommon->addMsg("Notification added successfully",1);
	}
	header("location:../index.php?page=notifications");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);