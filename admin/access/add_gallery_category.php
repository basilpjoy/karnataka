<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new gallery_category();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['gc_name'])&&$_POST['gc_name']!=""){
	$_POST['gc_name']		=	$objCommon->esc($_POST['gc_name']);
	if($editId){
		$objMain->update($_POST,"gc_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$_POST['gc_alias']	=	$objCommon->getAlias($_POST['gc_name']);
		$objMain->insert($_POST);
		$objCommon->addMsg("Category  added successfully",1);
	}
	header("location:../index.php?page=add-gallery-category");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>
