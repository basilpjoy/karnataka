<?php
@session_start();
include_once("../../config/site_root.php");
$objHostels		  =	new hostels();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['t_id'],$_POST['hostel_strength'])&&$_POST['t_id']!=""&&$_POST['hostel_strength']!=""){
	$_POST['hostel_godate']    =   $objCommon->backEndDate($_POST['hostel_godate']);
	$_POST['hostel_latitude']    =   $objCommon->map2Degree($_POST['hostel_latitude']);
	$_POST['hostel_longitude']    =   $objCommon->map2Degree($_POST['hostel_longitude']);
	if($editId){
		$objHostels->update($_POST,"hostel_id=".$editId);
		$objCommon->addMsg("Hostel details updated successfully",1);
	}else{
		$_POST['hostel_alias']			=	$objCommon->getAlias($_POST['hostel_name']);
		$objHostels->insert($_POST);
		$objCommon->addMsg("Hostel details added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>