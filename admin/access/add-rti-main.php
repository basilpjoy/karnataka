<?php
@session_start();
include_once("../../config/site_root.php");
$objRtiMain		  =	new rti_main();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['rtmain_title'])&&$_POST['rtmain_title']!=""){
	if($editId){
		$objRtiMain->update($_POST,"rtmain_id=".$editId);
		$objCommon->addMsg("RTI Title updated successfully",1);
	}else{
		$_POST['rtmain_alias'] = $objCommon->getAlias($_POST['rtmain_title']);
		$objRtiMain->insert($_POST);
		$objCommon->addMsg("RTI Title added successfully", 1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>
