<?php
@session_start();
include_once("../../config/site_root.php");
$objSchemeFiles		  =	new scheme_files();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['sp_title'],$_POST['scheme_id'])&&$_POST['sp_title']!=""&&$_POST['scheme_id']!=""){
	if($_FILES['pdfFile']['name']){
		if(pathinfo($_FILES['pdfFile']['name'], PATHINFO_EXTENSION)=='pdf'){
			$filename	=	$_POST['sp_file']	=	$objCommon->getAlias($_POST['sp_title']).time().'.'.pathinfo($_FILES['pdfFile']['name'], PATHINFO_EXTENSION);
			move_uploaded_file($_FILES['pdfFile']['tmp_name'], DIR_ROOT . 'assets/uploads/pdf/' . $filename);
			if($editId) {
				$currRowDetails = $objSchemeFiles->getRow("sp_id=$editId");
				if(file_exists(DIR_ROOT . 'assets/uploads/pdf/' . $currRowDetails['sp_file'])){
					unlink(DIR_ROOT . 'assets/uploads/pdf/' . $currRowDetails['sp_file']);
				}
			}
		}else{
			$objCommon->addMsg("Only PDF files will support",0);
			header("location:".$_SERVER['HTTP_REFERER']);
			exit();
		}
	}
	if($editId){
		print_r($_POST);
		$objSchemeFiles->update($_POST,"sp_id=".$editId);
		$objCommon->addMsg("PDF details updated successfully",1);
	}else{
		if($_FILES['pdfFile']['name']) {
			$objSchemeFiles->insert($_POST);
			$objCommon->addMsg("PDF details added successfully", 1);
		}else{
			$objCommon->addMsg("Please upload pdf",0);
		}
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);