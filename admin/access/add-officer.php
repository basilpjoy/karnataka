<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new contact_offices();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['officer_info'],$_POST['contact_info'])&&$_POST['officer_info']!=""&&$_POST['contact_info']!=""){
	$_POST['cont_sidebar']				=	($_POST['cont_sidebar'])?$_POST['cont_sidebar']:0;
	if($_FILES['officerFile']['name']){
		$imageExtentions		=	array("jpg","jpeg","png","gif");
		$currentExtension		=	strtolower(pathinfo($_FILES['officerFile']['name'], PATHINFO_EXTENSION));
		if(in_array($currentExtension,$imageExtentions)){
			$filename	=	$_POST['officer_image']	=	time().'.'.$currentExtension;
			move_uploaded_file($_FILES['officerFile']['tmp_name'], DIR_ROOT . 'assets/uploads/officers/' . $filename);
			if($editId) {
				$currRowDetails = $objMain->getRow("officer_id=$editId");
				if(file_exists(DIR_ROOT . 'assets/uploads/officers/' . $currRowDetails['officer_image'])){
					unlink(DIR_ROOT . 'assets/uploads/officers/' . $currRowDetails['officer_image']);
				}
			}
		}else{
			$objCommon->addMsg("Only image files will support",0);
			header("location:".$_SERVER['HTTP_REFERER']);
			exit();
		}
	}
	if($editId){
		$objMain->update($_POST,"officer_id=".$editId);
		$objCommon->addMsg("Officer  updated successfully",1);
	}else{
		$_POST['cat_alias']	=	$objCommon->getAlias($_POST['cat_name']);
		$objMain->insert($_POST);
		$objCommon->addMsg("Officer  added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);