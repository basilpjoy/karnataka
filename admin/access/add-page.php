<?php
@session_start();
include_once("../../config/site_root.php");
$objPages		  =	new pages();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['page_title'],$_POST['page_content'])&&$_POST['page_title']!=""&&$_POST['page_content']!=""){
	$_POST['page_title']	=	$objCommon->esc($_POST['page_title']);
	$_POST['page_alias']	=	$objCommon->getAlias($_POST['page_title']);
	$_POST['created_on']	=	date("Y-m-d H:i:s");
	$_POST['page_status']   =	1;
	$_POST['footer_page']   =	(isset($_POST['footer_page']))?1:0;
	if($editId){
		$objPages->update($_POST,"page_id=".$editId);
		$objCommon->addMsg("Page  updated successfully",1);
	}else{
		$_POST['page_alias']	=	$objCommon->getAlias($_POST['page_title']);
		$objPages->insert($_POST);
		$objCommon->addMsg("Page  added successfully",1);
	}
	header("location:../index.php?page=pages");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>