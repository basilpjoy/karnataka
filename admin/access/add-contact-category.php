<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new contact_category();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['ccat_name'])&&$_POST['ccat_name']!=""){
	if($editId){
		$objMain->update($_POST,"ccat_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$_POST['ccat_alias']	=	$objCommon->getAlias($_POST['ccat_name']);
		$objMain->insert($_POST);
		$objCommon->addMsg("Category  added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);