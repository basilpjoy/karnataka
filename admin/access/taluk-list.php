<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new taluk();
$objCommon		 			=	new common();
$objCommon->adminCheck();
if(isset($_GET['dID'])){
	$dID		=	$objCommon->esc($_GET['dID']);
	$talukList	=	$objMain->getAll("d_id=$dID");
	$selResult	=	'<option value="">Select Taluk</option>';
	if(count($talukList)>0){
		foreach($talukList as $taluk){
			$selResult	.=	'<option value="'.$taluk['t_id'].'">'.$taluk['t_name'].'</option>';
		}
	}
	echo $selResult;
}