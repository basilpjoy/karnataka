<?php
@session_start();
include_once("../../config/site_root.php");
$objCommon					=	new common();
$objCommunityImages			  	=	new community_images();
$dId						=	$objCommon->esc($_GET['dId']);
if($dId){
	$currentImage		=	$objCommunityImages->getRow("chi_id=".$dId);
	if(count($currentImage)>1) {
		if(file_exists(DIR_ROOT."assets/uploads/colonyImages/".$currentImage['chi_image'])){
			$imageName	=	explode("/",$currentImage['chi_image']);
			$folderName	=	$imageName[0];
			$imageName	=	$imageName[1];
			unlink(DIR_ROOT."assets/uploads/colonyImages/".$folderName."/thumb/".$imageName);
			unlink(DIR_ROOT."assets/uploads/colonyImages/".$folderName."/original/".$imageName);
			unlink(DIR_ROOT."assets/uploads/colonyImages/".$folderName."/".$imageName);
		}
		$objCommunityImages->delete("chi_id=" . $dId);
		$objCommon->addMsg("Image  deleted successfully", 1);
	}
}
header("location:".$_SERVER['HTTP_REFERER']);