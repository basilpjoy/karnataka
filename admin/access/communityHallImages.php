<?php
include_once("../../config/site_root.php");
$objCommon					=	new common();
$objCommunityImages			  	=	new community_images();
$path						=	DIR_ROOT.'assets/uploads/colonyImages/';
$valid_formats				= 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
$uploadStatus				=	0;
$imgStr						=	'';
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST" && $_FILES['file']['tmp_name'] !='')
{
	$albumId					=	$objCommon->esc($_POST['chId']);
	$todayDate				=	date('Y-m-d');
	if(!file_exists($path.$todayDate)){
		@mkdir($path.$todayDate);
	}
	$path					=	$path.$todayDate.'/';
	foreach($_FILES['file']['tmp_name'] as $i => $tmp_name ){
		$name 				=	$_FILES['file']['name'][$i];
		$size 				=	$_FILES['file']['size'][$i];
		if(strlen($name))
		{
			$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
			if(in_array($ext,$valid_formats))
			{
				if($size<(3072*10240))
				{
					$randomDigit					=	$objCommon->randStrGenDigit(4);
					$time							=	time();
					$actual_image_name_ext_no		=	$time.'_'.$randomDigit;
					$actual_image_name				= 	$time.'_'.$randomDigit.".".$ext;

					if(!file_exists($path."original")){
						@mkdir($path."original");
					}
					if(!file_exists($path."thumb")){
						@mkdir($path."thumb");
					}
					$tmpName = $_FILES['file']['tmp_name'][$i];
					list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName);
					if(move_uploaded_file($_FILES['file']['tmp_name'][$i], $path.'original/'.$actual_image_name)){

						$resizeObj = new resize($path.'original/'.$actual_image_name);
						if($heighut >$widthu){
							if ($heighut > 700){
								$resizeObj -> resizeImage('', 700, 'auto');
								$resizeObj -> saveImage($path.$actual_image_name, 100);
							}else{
								$resizeObj -> resizeImage('', $heighut, 'auto');
								$resizeObj -> saveImage($path.$actual_image_name, 100);
							}
						}else{
							if ($widthu > 900){
								$resizeObj -> resizeImage(900, '', 'auto');
								$resizeObj -> saveImage($path.$actual_image_name, 100);
							}else{
								$resizeObj -> resizeImage($widthu,'', 'auto');
								$resizeObj -> saveImage($path.$actual_image_name, 100);
							}
						}
						if($heighut < 300 || $widthu <480){
							$resizeObj -> resizeImage($widthu, $heighut, 'exact');
							$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
						}else{
							$resizeObj -> resizeImage(480, 300, 'exact');
							$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
						}
						$uploadStatus				=	1;
						$_POST['ch_id']				=	$albumId;
						$_POST['chi_image']			=	$todayDate.'/'.$actual_image_name;
						$objCommunityImages->insert($_POST);
						$lastAlbImgId				=	$objCommunityImages->insertId();
						$arrayResponse			   =	array("imgId"=>$lastAlbImgId);
						//$imgStr					 .=	'<tr><td><img src="'.SITE_ROOT.'uploads/albums/'.$todayDate.'/thumb/'.$actual_image_name.'" class="img-responsive" width="50" height="60"/></td><td><textarea class="form-control" rows="1" name="img_descr[]"></textarea><input type="hidden" name="hid_img_id[]" value="'.$lastAlbImgId.'"/></td></tr>';
					}
				}
			}
//else
//echo '<font color="#CC0000">Invalid file format..</font>'; 
		}
//else
//echo '<font color="#CC0000">Please select image..!</font>';
	}
	if($uploadStatus==1){
		echo $jsonResponse		=	json_encode($arrayResponse);
	}
	//exit;

}
$hiddenIdDz				=	$_POST['hiddenIdDz'];
if(count($hiddenIdDz)>0){
	foreach($hiddenIdDz as $allhid){
		$ai_caption	=	$objCommon->esc($_POST['dZdesc_'.$allhid]);
		$objCommunityImages->updateField(array('chi_title'=>$ai_caption),"chi_id=".$allhid);
	}
}
?>