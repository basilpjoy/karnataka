<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new taluk();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['t_name'])&&$_POST['d_id']!=""){
	$_POST['t_code']		=	$objCommon->esc($_POST['t_code']);
	$_POST['t_name']		=	$objCommon->esc($_POST['t_name']);
	$_POST['d_id']			=	$objCommon->esc($_POST['d_id']);
	if($editId){
		$objMain->update($_POST,"t_id=".$editId);
		$objCommon->addMsg("Taluk  updated successfully",1);
	}else{
		$_POST['t_alias']	=	$objCommon->getAlias($_POST['t_name']);
		$objMain->insert($_POST);
		$objCommon->addMsg("Taluk  added successfully",1);
	}
	header("location:../index.php?page=add-taluk");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>