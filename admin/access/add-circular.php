<?php
@session_start();
include_once("../../config/site_root.php");
$objRTI		  =	new circulars();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['circular_title'],$_POST['circular_content'])&&$_POST['circular_title']!=""&&$_POST['circular_content']!=""){
	$_POST['circular_date']	=	$objCommon->backEndDate($_POST['circular_date']);
	if($_FILES['circularFile']['name']){
		if(pathinfo($_FILES['circularFile']['name'], PATHINFO_EXTENSION)=='pdf'){
			$filename	=	$_POST['circular_file']	=	$objCommon->getAlias($_POST['circular_title']).time().'.'.pathinfo($_FILES['circularFile']['name'], PATHINFO_EXTENSION);
			move_uploaded_file($_FILES['circularFile']['tmp_name'], DIR_ROOT . 'assets/uploads/circulars/' . $filename);
			if($editId) {
				$currRowDetails = $objRTI->getRow("circular_id=$editId");
				if(file_exists(DIR_ROOT . 'assets/uploads/circulars/' . $currRowDetails['circular_file'])){
					unlink(DIR_ROOT . 'assets/uploads/circulars/' . $currRowDetails['circular_file']);
				}
			}
		}else{
			$objCommon->addMsg("Only PDF files will support",0);
			header("location:".$_SERVER['HTTP_REFERER']);
			exit();
		}
	}
	if($editId){
		$objRTI->update($_POST,"circular_id=".$editId);
		$objCommon->addMsg("Circular details updated successfully",1);
	}else{
		$_POST['circular_alias']			=	$objCommon->getAlias($_POST['circular_title']);
		$objRTI->insert($_POST);
		$objCommon->addMsg("Circular details added successfully",1);
	}
	header("location:../index.php?page=circulars");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);