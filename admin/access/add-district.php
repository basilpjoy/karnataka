<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					=	new districts();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['district_name'])&&$_POST['district_name']!=""){
	$_POST['d_code']		=	$objCommon->esc($_POST['district_code']);
	$_POST['d_name']		=	$objCommon->esc($_POST['district_name']);
	if($editId){
		$objMain->update($_POST,"d_id=".$editId);
		$objCommon->addMsg("District  updated successfully",1);
	}else{
		$_POST['d_alias']	=	$objCommon->getAlias($_POST['district_name']);
		$objMain->insert($_POST);
		$objCommon->addMsg("District  added successfully",1);
	}
	header("location:../index.php?page=add-district");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);