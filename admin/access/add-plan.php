<?php
@session_start();
include_once("../../config/site_root.php");
$objPlans		  =	new component_plans();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['plan_title'],$_POST['plan_content'])&&$_POST['plan_title']!=""&&$_POST['plan_content']!=""){
	$_POST['plan_title']	=	$objCommon->esc($_POST['plan_title']);
	$_POST['plan_alias']	=	$objCommon->getAlias($_POST['plan_title']);
	$_POST['plan_created_on']	=	date("Y-m-d H:i:s");
	$_POST['plan_status']   =	1;
	if($editId){
		$objPlans->update($_POST,"plan_id=".$editId);
		$objCommon->addMsg("Plan  updated successfully",1);
	}else{
		$_POST['plan_alias']	=	$objCommon->getAlias($_POST['plan_title']);
		$objPlans->insert($_POST);
		$objCommon->addMsg("Plan  added successfully",1);
	}
	header("location:../index.php?page=component-plans");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>