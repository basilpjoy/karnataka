<?php
@session_start();
include_once("../../config/site_root.php");
$objColonies                 =	new colonies();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['gcat_id'],$_POST['community_name'])&&$_POST['gcat_id']!=""&&$_POST['community_name']!=""){
	$_POST['latitude']    =   $objCommon->map2Degree($_POST['latitude']);
	$_POST['longitude']    =   $objCommon->map2Degree($_POST['longitude']);
	$_POST['basic_livings']	=	serialize($_POST['basicLivings']);
	if($editId){
		$objColonies->update($_POST,"colony_id=".$editId);
		$objCommon->addMsg("Colony details updated successfully",1);
	}else{
		//$_POST['colony_alias']			=	$objCommon->getAlias($_POST['colony_name']);
		$objColonies->insert($_POST);
		$objCommon->addMsg("Colony details added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>