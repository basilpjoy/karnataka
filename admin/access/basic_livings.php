<?php
@session_start();
include_once("../../config/site_root.php");
$objMain					  =	new basic_living();
$objCommon		 			=	new common();
$objCommon->adminCheck();
$editId						=	$objCommon->esc($_POST['editId']);
if(isset($_POST['basic_name'])&&$_POST['basic_name']!=""){
	$_POST['basic_name']		=	$objCommon->esc($_POST['basic_name']);
	if($editId){
		$objMain->update($_POST,"basic_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objMain->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=basic_livings");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);