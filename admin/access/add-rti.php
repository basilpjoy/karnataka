<?php
@session_start();
include_once("../../config/site_root.php");
$objRTI		  =	new rti();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['rtmain_id'],$_POST['rti_number'])&&$_POST['rtmain_id']!=""&&$_POST['rti_number']!=""){
	if($_FILES['RTIFile']['name']){
		if(pathinfo($_FILES['RTIFile']['name'], PATHINFO_EXTENSION)=='pdf'){
			$filename	=	$_POST['rti_file']	=	$objCommon->getAlias($_POST['rti_number']).time().'.'.pathinfo($_FILES['RTIFile']['name'], PATHINFO_EXTENSION);
			move_uploaded_file($_FILES['RTIFile']['tmp_name'], DIR_ROOT . 'assets/uploads/pdf/' . $filename);
			if($editId) {
				$currRowDetails = $objRTI->getRow("rti_id=$editId");
				if(file_exists(DIR_ROOT . 'assets/uploads/pdf/' . $currRowDetails['rti_file'])){
					unlink(DIR_ROOT . 'assets/uploads/pdf/' . $currRowDetails['rti_file']);
				}
			}
		}else{
			$objCommon->addMsg("Only PDF files will support",0);
			header("location:".$_SERVER['HTTP_REFERER']);
			exit();
		}
	}
	if($editId){
		$objRTI->update($_POST,"rti_id=".$editId);
		$objCommon->addMsg("RTI details updated successfully",1);
	}else{
		if($_FILES['RTIFile']['name']) {
			$objRTI->insert($_POST);
			$objCommon->addMsg("RTI details added successfully", 1);
		}else{
			$objCommon->addMsg("Please upload RTI pdf",0);
		}
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>
