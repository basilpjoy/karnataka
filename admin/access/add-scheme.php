<?php
@session_start();
include_once("../../config/site_root.php");
$objSchemes		  =	new schemes();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['scheme_title'],$_POST['scheme_content'])&&$_POST['scheme_title']!=""&&$_POST['scheme_content']!=""){
	if($editId){
		$objSchemes->update($_POST,"scheme_id=".$editId);
		$objCommon->addMsg("Scheme details updated successfully",1);
	}else{
		$_POST['scheme_alias']			=	$objCommon->getAlias($_POST['scheme_title']);
		$objSchemes->insert($_POST);
		$objCommon->addMsg("Scheme details added successfully",1);
	}
	header("location:../index.php?page=schemes");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>