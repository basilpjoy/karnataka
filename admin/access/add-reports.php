<?php
@session_start();
include_once("../../config/site_root.php");
$objReports		  =	new reports();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['report_name'],$_POST['rcat_id'])&&$_POST['report_name']!=""&&$_POST['rcat_id']!=""){
	$_POST['report_date']	=	$objCommon->backEndDate($_POST['report_date']);
	if($_FILES['reportFile']['name']){
		if(pathinfo($_FILES['reportFile']['name'], PATHINFO_EXTENSION)=='pdf'){
			$filename	=	$_POST['report_file']	=	$objCommon->getAlias($_POST['report_name']).time().'.'.pathinfo($_FILES['reportFile']['name'], PATHINFO_EXTENSION);
			move_uploaded_file($_FILES['reportFile']['tmp_name'], DIR_ROOT . 'assets/uploads/reports/' . $filename);
			if($editId) {
				$currRowDetails = $objReports->getRow("report_id=$editId");
				if(file_exists(DIR_ROOT . 'assets/uploads/reports/' . $currRowDetails['report_file'])){
					unlink(DIR_ROOT . 'assets/uploads/reports/' . $currRowDetails['report_file']);
				}
			}
		}else{
			$objCommon->addMsg("Only PDF files will support",0);
			header("location:".$_SERVER['HTTP_REFERER']);
			exit();
		}
	}
	if($editId){
		$objReports->update($_POST,"report_id=".$editId);
		$objCommon->addMsg("Report updated successfully",1);
	}else{
		$_POST['report_alias']			=	$objCommon->getAlias($_POST['report_name']);
		$objReports->insert($_POST);
		$objCommon->addMsg("Report added successfully",1);
	}
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);