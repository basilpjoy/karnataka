<?php
@session_start();
ob_start();
include_once("../config/site_root.php");
include_once(DIR_ROOT."config/class/pagination-class.php");
$objCommon	=	new common();
if(!$objCommon->adminLogin()){
  header("location:login.php");
  exit();
}
$page	=	"pages";
if(isset($_GET['page'])&&$_GET['page']!=""){
  $page	=	$_GET['page'];
}
$pageName	=	"pages/".$page.".php";
if(!file_exists($pageName)){
  $pageName	=	"pages/pages.php";
}
include_once(DIR_ROOT.'admin/includes/header.php');
?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>DK</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Social welfare DK</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">Karnataka <i class="fa fa-caret-down"></i></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <p>
                  Social welfare DK
                  <small>Commissionarate of Social Welfare - Dakshina Kannada, Mangalore</small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="?page=change-password" class="btn btn-default btn-flat">Change Password</a>
                </div>
                <div class="pull-right">
                  <a href="access/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <?php include_once('includes/sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <?php include_once($pageName); ?>

    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      corecipher
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date("Y") ?> <a href="#">Social welfare DK</a>.</strong> All rights reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
