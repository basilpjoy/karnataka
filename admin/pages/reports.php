<?php
$objReports                 =	new reports();
$objReportCats              =	new report_categories();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
if($nId){
    $getRowDetails          =	$objReports->getRow("report_id=".$nId);
}
if($dId){
    $deleteRow  =   $objReports->getRow("report_id=".$dId);
    if(file_exists(DIR_ROOT . 'assets/uploads/reports/' . $deleteRow['report_file'])){
        unlink(DIR_ROOT . 'assets/uploads/reports/' . $deleteRow['report_file']);
    }
    $objReports->delete("report_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:index.php?page=pdf-files");
    exit;
}
$sql						 .= "SELECT r.*,c.rcat_name FROM reports AS r LEFT JOIN report_categories AS c ON r.rcat_id=c.rcat_id WHERE 1";
if($search){
    $sql					.= " AND (	r.report_name LIKE '%".$search."%' OR 	c.rcat_name LIKE '%".$search."%' OR 	r.report_id LIKE '%".$search."%')";
}
$sql						 .= " ORDER by 	r.report_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objReports->listQuery($paginationQuery);
$catList                    =   $objReportCats->getAll("rcat_status=1");
?>
<div class="page-heading">
    <h3>Reports</h3>
    <ul class="breadcrumb">
        <li><a href="#">Reports</a></li>
        <li class="active"> Reports </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add Reports</header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-reports.php" enctype="multipart/form-data">
                    <input type="hidden" name="report_category" value="2">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Report Name</label>
                        <input type="text" name="report_name" id="report_name" class="form-control" value="<?php echo ($getRowDetails['report_name'])?$objCommon->html2text($getRowDetails['report_name']):''?>" placeholder="Enter Name" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Report Category</label>
                        <select class="form-control" name="rcat_id">
                            <option value="">Select Category</option>
                            <?php
                            foreach ($catList as $cat) {
                                $selDist = ($cat['rcat_id'] == $getRowDetails['rcat_id']) ? 'selected="selected"' : '';
                                ?>
                                <option value="<?php echo $objCommon->html2text($cat['rcat_id']) ?>" <?php echo $selDist ?>><?php echo $objCommon->html2text($cat['rcat_name']) ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Reports File</label>
                        <input name="reportFile" type="file" id="exampleInputFile">
                        <p class="help-block">Please Upload PDF only.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Report Date</label>
                        <input type="text" name="report_date" id="report_date" class="form-control datepicker" value="<?php echo ($getRowDetails['report_date'])?$objCommon->html2text($getRowDetails['report_date']):''?>" placeholder="Enter Name" >
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">Reports List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="8%">No</th>
                            <th width="30%">Report Name</th>
                            <th width="30%">Report Category</th>
                            <th width="20%">Report Date</th>
                            <th width="10%">Reports</th>
                            <th width="10%">ID</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['report_name']); ?></td>
                                    <td><?php echo $objCommon->html2text($list['rcat_name']); ?></td>
                                    <td><?php echo $objCommon->frontEndDate($list['report_date']); ?></td>
                                    <td><a target="_blank" href="<?php echo SITE_ROOT . 'assets/uploads/reports/' .$objCommon->html2text($list['report_file']); ?>">View PDF</a></td>
                                    <td><?php echo $list['report_id']; ?></td></td>
                                    <td>
                                        <a href="?page=reports&nId=<?php echo $list['report_id']?> "
                                           class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=reports&dId=<?php echo $list['report_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="5">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    $(function() {
        $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy'});
    });
</script>