<?php
$objSubCategories               =	new geo_sub_categories();
$objCategories               =	new geo_categories();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
$gcatID                     =	$objCommon->esc($_GET['gcatID']);
$catTalukList                =   $objCategories->getAll("gcat_type=1");
if($nId){
    $getRowDetails          =	$objSubCategories->getRow("gscat_id=".$nId);
}
if($dId){
    $objSubCategories->delete("gscat_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
$sql						 .= "SELECT gs.*,gc.gcat_name FROM geo_sub_categories as gs LEFT JOIN geo_categories AS gc ON gs.gcat_id=gc.gcat_id WHERE 1 ";
if($search){
    $sql					.= " AND (gscat_name LIKE '%".$search."%' OR gscat_id LIKE '%".$search."%')";
}
if($gcatID){
    $sql					.= " AND gcat_ide=$gcatID";
}
$sql						 .= " ORDER by gcat_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objCategories->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Grama Panchayath</h3>
    <ul class="breadcrumb">
        <li><a href="#">Colonies</a></li>
        <li class="active"> Grama Panchayath </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add Grama Panchayath</header>
            <div class="panel-body">
                <form role="form" id="add_category" method="post" action="access/add-geo-sub-category.php">
                    <!--<div class="form-group">
                        <label for="exampleInputEmail1">Category Type</label>
                        <select class="form-control" name="gcat_type" onchange="selectCats(this);">
                            <option value="0">Select Type</option>
                            <option value="1"<?php /*echo ($getRowDetails['gcat_type']==1)?' selected':''; */?>>Taluk</option>
                            <option value="2"<?php /*echo ($getRowDetails['gcat_type']==2)?' selected':''; */?>>ULB</option>
                            <option value="3"<?php /*echo ($getRowDetails['gcat_type']==3)?' selected':''; */?>>City Corporation</option>
                        </select>
                    </div>-->
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Taluk</label>
                        <select class="form-control" name="gcat_id" id="gcat_id">
                            <option value="0">Select</option>
                            <?php
                                $catList    =   $objCategories->getAll("gcat_type=1");
                            foreach($catList as $cat){ ?>
                            <option value="<?php echo $cat['gcat_id']; ?>"<?php echo ($getRowDetails['gcat_id']==$cat['gcat_id'])?' selected':''; ?>><?php echo $cat['gcat_name']; ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Grama Panchayath Name</label>
                        <input type="text" name="gscat_name" id="gscat_name" class="form-control" value="<?php echo ($getRowDetails['gscat_name'])?$objCommon->html2text($getRowDetails['gscat_name']):''?>" placeholder="Enter Name" required >
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">Grama Panchayath List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <select name="gcatID">
                                <option value="0">Select Taluk</option>
                                <?php foreach($catTalukList as $talk){ ?>
                                <option value="<?php echo $talk['gcat_id']; ?>"<?php echo ($_GET['gcatID']==$talk['gcat_id'])?' selected':''; ?>><?php echo $talk['gcat_name']; ?></option>
                                <?php } ?>
                            </select>
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="35%">Category Name</th>
                            <th width="35%">Category Type</th>
                            <th width="10%">ID</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['gscat_name']); ?></td>
                                    <td><?php echo $list['gcat_name']; ?></td>
                                    <td><?php echo $list['gcat_id']; ?></td>
                                    <td>
                                        <a href="?page=geo_sub_categories&nId=<?php echo $list['gscat_id']?>" class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=geo_sub_categories&dId=<?php echo $list['gscat_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="4">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script type="text/javascript" >
/*function selectCats(a){
    var catVal     =    $(a).val();
    $.get("ajax/getCategories.php",{typeID:catVal},
    function(data){
        $("#gcat_id").html(data);
    });
}*/
</script>