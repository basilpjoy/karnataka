<?php
$objMain                    =	new home_slider();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
if($nId){
    $getRowDetails          =	$objMain->getRow("hs_id=".$nId);
}
if($dId){
    $objMain->delete("hs_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
$sql						 .= "SELECT * FROM home_slider WHERE 1 ";
if($search){
    $sql					.= " AND (hs_title LIKE '%".$search."%' OR hs_id LIKE '%".$search."%')";
}
$sql						 .= " ORDER by hs_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objMain->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Gallery</h3>
    <ul class="breadcrumb">
        <li><a href="#">Gallery</a></li>
        <li class="active"> Home Slider </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add Home Slider</header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-home-slider.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" name="hs_title" id="hs_title" class="form-control" value="<?php echo ($getRowDetails['hs_title'])?$objCommon->html2text($getRowDetails['hs_title']):''?>" placeholder="Enter Title" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Order</label>
                        <input type="text" name="hs_order" id="hs_order" class="form-control" value="<?php echo ($getRowDetails['hs_order'])?$objCommon->html2text($getRowDetails['hs_order']):''?>" placeholder="Enter Order"  >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Image</label>
                        <input type="file" class="form-control" name="hs_img">
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">Slider List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="30%">Image</th>
                            <th width="30%">Title</th>
                            <th width="10%">Order</th>
                            <th width="10%">ID</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><img src="<?php echo SITE_ROOT.'assets/uploads/home-slider/'.$objCommon->html2text($list['hs_img'])?>" width="50" height="50"></td></td>
                                    <td><?php echo $objCommon->html2text($list['hs_title']); ?></td></td>
                                    <td><?php echo $objCommon->html2text($list['hs_order']); ?></td></td>
                                    <td><?php echo $list['hs_id']; ?></td></td>
                                    <td>
                                        <a href="?page=home-slider&nId=<?php echo $list['hs_id']?> "class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=home-slider&dId=<?php echo $list['hs_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="4">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    var Script = function () {
        $.validator.setDefaults({
            submitHandler: function() { alert("submitted!"); }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#add_district").validate();
        });
    }();
</script>