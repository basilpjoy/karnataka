<?php
include_once(DIR_ROOT."config/class/taluk.php");
include_once(DIR_ROOT."config/class/districts.php");
$objMain                    =	new taluk();
$objColonies               =   new districts();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
$districtList               =   $objColonies->getAll("","d_name");
if($nId){
    $getRowDetails          =	$objMain->getRow("t_id=".$nId);
}
if($dId){
    $objMain->delete("t_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
$sql						    .= "SELECT t.*,d.d_name FROM taluk AS t LEFT JOIN districts AS d ON t.d_id = d.d_id WHERE 1 ";
if($search){
    $sql					    .= " AND (t_name LIKE '%".$search."%' OR t_id LIKE '%".$search."%' OR d_name LIKE '%".$search."%')";
}
$sql                            .= " ORDER by t_id DESC";
$num_results_per_page           = 20;
$num_page_links_per_page        = 5;
$pg_param 					    = "";
$pagesection				    = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				    =	$objMain->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Settings</h3>
    <ul class="breadcrumb">
        <li><a href="#">Settings</a></li>
        <li class="active"> Taluks </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add Taluks</header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-taluk.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Taluk Code</label>
                        <input type="text" name="t_code" id="t_code" class="form-control" value="<?php echo
                        ($getRowDetails['t_code'])?$objCommon->html2text($getRowDetails['t_code']):''?>" placeholder="Enter Code" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Taluk Name</label>
                        <input type="text" name="t_name" id="t_name" class="form-control" value="<?php echo ($getRowDetails['t_name'])?$objCommon->html2text($getRowDetails['t_name']):''?>" placeholder="Enter Name" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">District Name</label>
                        <select class="form-control" name="d_id">
                            <option value="">Select District</option>
                            <?php
                            foreach($districtList as $allDist){
                                $selDist        =   ($allDist['d_id']==$getRowDetails['d_id'])?'selected="selected"':'';
                                ?>
                                <option value="<?php echo $objCommon->html2text($allDist['d_id'])?>" <?php echo $selDist?>><?php
                                echo $objCommon->html2text($allDist['d_name'])?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">Taluks List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="35%">Name</th>
                            <th width="35%">District</th>
                            <th width="10%">Code</th>
                            <th width="10%">ID</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['t_name']); ?></td></td>
                                    <td><?php echo $objCommon->html2text($list['d_name']); ?></td></td>
                                    <td><?php echo $objCommon->html2text($list['t_code']); ?></td></td>
                                    <td><?php echo $list['t_id']; ?></td></td>
                                    <td>
                                        <a href="?page=add-taluk&nId=<?php echo $list['t_id']?> "
                                           class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=add-taluk&dId=<?php echo $list['t_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="4">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    var Script = function () {
        $.validator.setDefaults({
            submitHandler: function() { alert("submitted!"); }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#add_district").validate();
        });
    }();
</script>