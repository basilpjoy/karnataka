<?php
$objHostels			  		=	new hostels();
$nId                        =	$objCommon->esc($_GET['nId']);
if($nId){
    $getRowDetails          =	$objHostels->getRow("hostel_id=".$nId);
}
?>
<div class="page-heading">
    <h3>Hostels</h3>
    <ul class="breadcrumb">
        <li><a href="#">Hostels</a></li>
        <li class="active"> Add Hostel </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Add Hostel
            </header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-hostel.php">
                    <?php
                    if(isset($_GET['nId'])) { ?>
                        <input type="hidden" name="editId" value="<?php echo $nId; ?>">
                        <?php
                    }
                    ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Name</label>
                        <input required type="text" name="hostel_name" id="district_code" class="form-control" value="<?php echo ($getRowDetails['hostel_name'])?$objCommon->html2text($getRowDetails['hostel_name']):''?>" placeholder="Enter Name" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Content</label>
                        <textarea class="form-control ckeditor" name="hostel_content" rows="6"><?php echo $objCommon->displayEditor($getRowDetails['hostel_content']); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Order</label>
                        <input type="text" name="hostel_order" id="hostel_order" class="form-control" value="<?php echo ($getRowDetails['hostel_order'])?$objCommon->html2text($getRowDetails['hostel_order']):''?>" placeholder="Enter Order" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Status</label>
                        <select class="form-control" name="hostel_status">
                            <option value="1"<?php echo ($getRowDetails['hostel_status']==1)?' selected':''; ?>>Enable</option>
                            <option value="0"<?php echo (isset($getRowDetails['hostel_status'])&&$getRowDetails['hostel_status']==0)?' selected':''; ?>>Disable</option>
                        </select>
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckfinder.js"></script>