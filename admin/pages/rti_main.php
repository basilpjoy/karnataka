<?php
$objRtiMain                   =	new rti_main();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
if($nId){
    $getRowDetails          =	$objRtiMain->getRow("rtmain_id=".$nId);
}
if($dId){
    $deleteRow  =   $objRtiMain->getRow("rtmain_id=".$dId);
    $objRtiMain->delete("rtmain_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:index.php?page=rti_main");
    exit;
}
$sql						 .= "SELECT * FROM rti_main WHERE 1 ";
if($search){
    $sql					.= " AND (	rtmain_title LIKE '%".$search."%' OR rtmain_id LIKE '%".$search."%')";
}
$sql						 .= " ORDER by rtmain_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objRtiMain->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>RTI Files</h3>
    <ul class="breadcrumb">
        <li><a href="#">Usefull Links</a></li>
        <li class="active"> RTI Files </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add RTI File</header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-rti-main.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">RTI Title</label>
                        <input type="text" name="rtmain_title" id="rtmain_title" class="form-control" value="<?php echo ($getRowDetails['rtmain_title'])?$objCommon->html2text($getRowDetails['rtmain_title']):''?>" placeholder="Enter Title" >
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">RTI File List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="8%">No</th>
                            <th width="50%">RTI Name</th>
                            <th width="10%">Order</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['rtmain_title']); ?></td>
                                    <td><?php echo $list['rtmain_id']; ?></td></td>
                                    <td>
                                        <a href="?page=rti_main&nId=<?php echo $list['rtmain_id']?>" class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=rti_main&dId=<?php echo $list['rtmain_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="6">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    var Script = function () {
        $.validator.setDefaults({
            submitHandler: function() { alert("submitted!"); }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#add_district").validate();
        });
    }();
</script>
