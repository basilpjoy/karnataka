<?php
$objSchemeFiles             =	new scheme_files();
$objSchemes                 =	new schemes();
$schemeId                   =	$objCommon->esc($_GET['schemeId']);
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
if($schemeId){
    $getSchemeDetails          =	$objSchemes->getRow("scheme_id=".$schemeId);
    if(count($getSchemeDetails)==1){
        header("location:?page=schemes");
        exit;
    }
}else{
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
if($nId){
    $getRowDetails          =	$objSchemeFiles->getRow("sp_id=".$nId);
}
if($dId){
    $deleteRow  =   $objSchemeFiles->getRow("sp_id=".$dId);
    if(file_exists(DIR_ROOT . 'assets/uploads/pdf/' . $deleteRow['sp_file'])){
        unlink(DIR_ROOT . 'assets/uploads/pdf/' . $deleteRow['sp_file']);
    }
    $objSchemeFiles->delete("sp_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:index.php?page=scheme-files");
    exit;
}
$sql						 .= "SELECT * FROM scheme_files WHERE scheme_id=$schemeId ";
if($search){
    $sql					.= " AND (	sp_title LIKE '%".$search."%' OR 	sp_id LIKE '%".$search."%')";
}
$sql						 .= " ORDER by 	sp_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objSchemeFiles->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Scheme PDF Files</h3>
    <ul class="breadcrumb">
        <li><a href="?page=schemes">Schemes</a></li>
        <li><a href="#"><?php echo $getSchemeDetails['scheme_title']; ?></a></li>
        <li class="active"> PDF Files </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add PDF File</header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-schemeFiles.php" enctype="multipart/form-data">
                    <input type="hidden" name="scheme_id" value="<?php echo $schemeId; ?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1">File Name</label>
                        <input type="text" name="sp_title" id="sp_title" class="form-control" value="<?php echo ($getRowDetails['sp_title'])?$objCommon->html2text($getRowDetails['sp_title']):''?>" placeholder="Enter Name" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">PDF File</label>
                        <input name="pdfFile" type="file" id="exampleInputFile">
                        <p class="help-block">Please Upload PDF only.</p>
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">PDF File List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="8%">No</th>
                            <th width="60%">PDF Name</th>
                            <th width="10%">PDF File</th>
                            <th width="10%">ID</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['sp_title']); ?></td>
                                    <td><a target="_blank" href="<?php echo SITE_ROOT . 'assets/uploads/pdf/' .$objCommon->html2text($list['sp_file']); ?>">View PDF</a></td>
                                    <td><?php echo $list['link_id']; ?></td></td>
                                    <td>
                                        <a href="?page=scheme-files&schemeId=<?php echo $schemeId; ?>&nId=<?php echo $list['sp_id']?> "
                                           class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=scheme-files&schemeId=<?php echo $schemeId; ?>&dId=<?php echo $list['sp_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="5">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    var Script = function () {
        $.validator.setDefaults({
            submitHandler: function() { alert("submitted!"); }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#add_district").validate();
        });
    }();
</script>