<?php
$objOfficers                 =	new contact_offices();
$objContactCats              =	new contact_category();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
if($nId){
    $getRowDetails          =	$objOfficers->getRow("officer_id=".$nId);
}
if($dId){
    $deleteRow  =   $objOfficers->getRow("officer_id=".$dId);
    if(file_exists(DIR_ROOT . 'assets/uploads/officers/' . $deleteRow['officer_image'])){
        unlink(DIR_ROOT . 'assets/uploads/officers/' . $deleteRow['officer_image']);
    }
    $objOfficers->delete("officer_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:index.php?page=contact-officers");
    exit;
}
$sql						 .= "SELECT r.*,c.ccat_name FROM contact_offices AS r LEFT JOIN contact_category AS c ON r.ccat_id=c.ccat_id WHERE 1";
if($search){
    $sql					.= " AND (	r.officer_name LIKE '%".$search."%' OR 	c.ccat_name LIKE '%".$search."%' OR 	r.officer_id LIKE '%".$search."%')";
}
$sql						.= " ORDER by 	r.officer_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	= 5;
$pg_param 					= "";
$pagesection				= '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objOfficers->listQuery($paginationQuery);
$catList                    =   $objContactCats->getAll("ccat_status=1");
?>
<div class="page-heading">
    <h3>Officers</h3>
    <ul class="breadcrumb">
        <li><a href="#">Officers</a></li>
        <li class="active"> Officer </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-5">
        <section class="panel">
            <header class="panel-heading">Add Officer</header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-officer.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Officer Category</label>
                        <select class="form-control" name="ccat_id">
                            <option value="">Select Category</option>
                            <?php
                            foreach ($catList as $cat) {
                                $selDist = ($cat['ccat_id'] == $getRowDetails['ccat_id']) ? 'selected="selected"' : '';
                                ?>
                                <option value="<?php echo $objCommon->html2text($cat['ccat_id']) ?>" <?php echo $selDist ?>><?php echo $objCommon->html2text($cat['ccat_name']) ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Officer Info</label>
                        <textarea class="form-control ckeditor" name="officer_info" rows="4"><?php echo $objCommon->displayEditor($getRowDetails['officer_info']); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Contact Info</label>
                        <textarea class="form-control ckeditor" name="contact_info" rows="4"><?php echo $objCommon->displayEditor($getRowDetails['contact_info']); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Officer Image</label>
                        <input name="officerFile" type="file" id="exampleInputFile">
                        <p class="help-block">Please Upload images (jpg, jpeg, png, gif) only.</p>
                    </div>
					 <div class="form-group">
					 <label><input type="checkbox"  name="cont_sidebar" value="1" <?php echo ($getRowDetails['cont_sidebar']==1)?'checked':'';?>/>
                     &nbsp;&nbsp;Show Contact Info in sidebar</label>
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-7">
        <section class="panel">
            <header class="panel-heading">Reports List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="8%">No</th>
                            <th width="20%">Officer Image</th>
                            <th width="30%">Officer Info</th>
                            <th width="30%">Contact Info</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><img width="150" src="<?php echo SITE_ROOT . 'assets/uploads/officers/' .$objCommon->html2text($list['officer_image']); ?>"></td>
                                    <td><?php echo $objCommon->displayEditor($list['officer_info']); ?></td>
                                    <td><?php echo $objCommon->displayEditor($list['contact_info']); ?></td>
                                    <td>
                                        <a href="?page=contact-offices&nId=<?php echo $list['officer_id']?> "
                                           class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=contact-offices&dId=<?php echo $list['officer_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="5">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    $(function() {
        $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy'});
    });
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckfinder.js"></script>