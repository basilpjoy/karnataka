<?php
$objNotifications			  		=	new notifications();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
if($dId){
    $objNotifications->delete("note_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
$sql						.= "SELECT * FROM notifications WHERE 1 ";
if($search){
    $sql					.= " AND (notification_title LIKE '%".$search."%' OR notification_alias LIKE '%".$search."%')";
}
$sql						.= " ORDER by note_id DESC";
$num_results_per_page        = 20;
$num_page_links_per_page 	 = 5;
$pg_param 					 = "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objNotifications->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Notifications</h3>
    <ul class="breadcrumb">
        <li><a href="#">Notifications</a></li>
        <li class="active"> Notification List </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">

    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="page-list" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="30%">Notification Title</th>
                            <th width="30%">Notification Alias</th>
                            <th width="20%">Notification Status</th>
                            <th width="20%">Notification File</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['notification_title']); ?></td>
                                    <td><?php echo $objCommon->html2text($list['notification_alias']); ?></td>
                                    <td><?php echo ($list['notification_status']==1)?'New':'Old'; ?></td>
                                    <td><?php echo ($list['notification_file']!="")?'<a target="_blank" href="'.SITE_ROOT . 'assets/uploads/reports/' .$objCommon->html2text($list['notification_file']).'">View File</a>':''; ?></td>
                                    <td>
                                        <a href="?page=add-notification&nId=<?php echo $list['note_id']?> " class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=notifications&dId=<?php echo $list['note_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="5">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    var Script = function () {
        $.validator.setDefaults({
            submitHandler: function() { alert("submitted!"); }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#add_collar").validate();
        });
    }();
</script>