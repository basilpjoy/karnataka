<?php
//$objMain                    =	new colony_images();
$objMain                    =	new community_images();
if ($_GET['chId']){
    $getRowDetails          =	$objMain->getRow("colony_id=".$_GET['chId']);
}else{
    header("location:../index.php?page=colonies");
}
?>
<link href="<?php echo SITE_ROOT?>admin/plugins/dropzone/css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?php echo SITE_ROOT?>admin/plugins/dropzone/dropzone.js"></script>
<script src="<?php echo SITE_ROOT?>admin/plugins/jquery.form.js"></script>
<div class="page-heading">
    <h3>Community Hall Images</h3>
    <ul class="breadcrumb">
        <li><a href="#">Community Hall Images</a></li>
        <li class="active"> Upload New </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Upload Image :<?php echo $objCommon->html2text($getRowDetails['colony_name'])?>
                <a href="?page=colonies" class="pull-right">Back</a>
            </header>
            <div class="panel-body">
                <div class="dropZoneSection">
                    <div class="my_prev">
                        <form action="access/communityHallImages.php" class="dropzone" id="newUplaod" method="post"><input type="hidden" value="<?php echo $objCommon->esc($_GET['chId'])?>" name="chId"></form>
                        <div id="drop_message">Drop Your File Here</div>
                    </div>
                    <div class="form_submit">
                        <button class="btn btn-default pull-right removeAttr media_form_submit" disabled="disabled">Submit</button>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div id="preview" class="text-center"></div>
                <?php
                $getAlbumImages     =  $objMain->listQuery("SELECT * FROM community_images WHERE 1 AND ch_id=".$objCommon->esc($_GET['chId']));
                if(count($getAlbumImages)){
                    ?>
                    <form action="access/community_images_edit.php" method="post">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <?php
                        echo '<tr>';
                        foreach($getAlbumImages as $keyAll=>$allImages){
                            if($keyAll%2==0) {
                                echo '</tr><tr>';
                            }
                            ?>
                                <input type="hidden" name="imageId[]" value="<?php echo $objCommon->html2text($allImages['chi_id'])?>">
                                <td width="10%"><img src="<?php echo SITE_ROOT?>assets/uploads/colonyImages/<?php echo $allImages['chi_image']?>" width="50" height="50" ></td>
                                <td  width="35%"><textarea name="imageTitle[]" style="width:95%"><?php echo $objCommon->html2text($allImages['chi_title'])?></textarea></td>
                                <td width="5%" style="vertical-align: middle"><a href="access/delete_community_image.php?dId=<?php echo $objCommon->html2text($allImages['chi_id'])?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a></td>
                            <?php
                        }
                        echo '</tr>';
                        ?>
                    </table>
                        <div class="row"><div class="col-md-12 text-right"> <button class="btn btn-primary" type="submit">Update</button> </div></div>
                    </form>
                    <?php
                }
                ?>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    Dropzone.autoDiscover = false;
    $("#newUplaod").dropzone({
        paramName: "file[]",
        addRemoveLinks: false,
        thumbnailWidth: 182,
        thumbnailHeight: 184,
        success:function(file, response){
            $('#drop_message').show();
            $('.my_prev .title_upload_dz, .my_prev .desc_photo_dz').click(function(){
                insertId($(this));
            });
            $('.dz-remove').click(function(){
                var deletId	=	$(this).parent('.dz-preview').attr('id');
                $(this).attr('data-deleteId',deletId );

            });
        },
        queuecomplete: function(){
            $('.my_prev .title_upload_dz, .my_prev .desc_photo_dz, .removeAttr').removeAttr('disabled');
        }
    });

    function insertId(_this) {
        var primaryId	=	_this.parent('.form_section').parent('.dz-preview').attr('id');

        _this.parent('.form_section').children('.hiddenField').attr({'name': 'hiddenIdDz[]', 'value':primaryId});
        _this.parent('.form_section').children('.desc_photo_dz').attr('name', 'dZdesc_'+primaryId);

    }
    $(document).ready(function(e) {
        $('.media_form_submit').on('click', function(){
            $("#preview").html('<img src="<?php echo SITE_ROOT?>assets/images/1.gif" width="100"/>');
            $("#newUplaod").ajaxForm(
                {
                    target: '#preview',
                    success:successCall
                }).submit();
            $(".albListShow").hide();
        });
    });
    function successCall(){
        window.location.href="<?php echo SITE_ROOT?>admin/index.php?page=community_halls-images&chId=<?php echo $objCommon->esc($_GET['chId'])?>";
    }

</script>