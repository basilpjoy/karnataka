<?php
$objpages			  		=	new pages();
$parentPages            =   $objpages->getAll("page_parent=0");
if(isset($_GET['nId'])){
    $nId            =   $objCommon->esc($_GET['nId']);
    $notificationDetails    =   $objpages->getRow("page_id=$nId");
}
?>
<div class="page-heading">
    <h3>Pages</h3>
    <ul class="breadcrumb">
        <li><a href="#">Pages</a></li>
        <li class="active"> Add Page </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Add Pages
            </header>
            <div class="panel-body">
                <form role="form" method="post" action="access/add-page.php">
                    <?php
                    if(isset($_GET['nId'])) { ?>
                        <input type="hidden" name="editId" value="<?php echo $nId; ?>">
                    <?php
                    }
                    ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Page Name</label>
                        <input type="text" name="page_title" class="form-control" value="<?php echo $objCommon->html2text(($notificationDetails['page_title'])); ?>" id="exampleInputEmail1" placeholder="Enter Title">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Parent Page</label>
                        <select name="page_parent" id="parentPage" class="form-control">
                            <option value="">Select Parent page</option>
                            <?php
                                foreach($parentPages as $page) {
                                    ?>
                                    <option value="<?php echo $page['page_id']; ?>"<?php echo ($notificationDetails['page_parent']==$page['page_id']); ?>><?php echo $page['page_title']; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Page Content</label>
                        <textarea class="form-control ckeditor" name="page_content" rows="6"><?php echo $objCommon->displayEditor($notificationDetails['page_content']); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Include This on Footer</label>
                        <div><input <?php echo ($notificationDetails['footer_page'])?'checked':''; ?> type="checkbox" name="footer_page" id="footer_page"> Yes</div>                        
                        <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckfinder.js"></script>