<?php
$objHostels                 =	new hostels();
$objTaluk                   =	new taluk();
$objDistricts               =	new districts();
$objCats                    =	new hostel_categories();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
$tID                        =	$objCommon->esc($_GET['t_id']);
if($nId){
    $getRowDetails          =	$objHostels->getRow("hostel_id=".$nId);
}
if($dId){
    $objHostels->delete("hostel_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
$sql						 .= "SELECT h.*,t.t_name FROM hostels AS h LEFT JOIN taluk AS t ON h.t_id=t.t_id LEFT JOIN districts AS d ON h.d_id=d.d_id WHERE 1 ";
if($search){
    $sql					.= " AND (h.hostel_name LIKE '%".$search."%' OR t.t_name LIKE '%".$search."%' OR d.d_name LIKE '%".$search."%' OR hostel_id LIKE '%".$search."%')";
}
if($tID){
    $sql					.= " AND h.t_id=$tID";
}
$sql						 .= " ORDER by hostel_name ASC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objHostels->listQuery($paginationQuery);
$districtList                   =   $objDistricts->getAll();
$catList                    =   $objCats->getAll();
$talukSearchList            =   $objTaluk->getAll();
?>
<div class="page-heading">
    <h3>Hostels</h3>
    <ul class="breadcrumb">
        <li><a href="#">Hostels</a></li>
        <li class="active"> Hostels </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-5">
        <section class="panel">
            <header class="panel-heading">Add Hostel</header>
            <div class="panel-body">
                <form role="form" id="ad_hostel" method="post" action="access/add-hostel.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Name</label>
                        <input type="text" name="hostel_name" id="hostel_name" class="form-control" value="<?php echo ($getRowDetails['hostel_name'])?$objCommon->html2text($getRowDetails['hostel_name']):''?>" placeholder="Enter Name" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">District Name</label>
                        <select class="form-control" name="d_id" onchange="selectTaluk(this);">
                            <option value="">Select District</option>
                            <?php
                            foreach($districtList as $allDist){
                                $selDist        =   ($allDist['d_id']==$getRowDetails['d_id'])?'selected="selected"':'';
                                ?>
                                <option value="<?php echo $objCommon->html2text($allDist['d_id'])?>" <?php echo $selDist?>><?php echo $objCommon->html2text($allDist['d_name'])?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Taluk Name</label>
                        <select class="form-control" name="t_id" id="talukId">
                            <option value="">Select Taluk</option>
                            <?php
                            if($nId) {
                                $talukList                  =   $objTaluk->getAll("d_id=".$getRowDetails['d_id']);
                                foreach ($talukList as $taluk) {
                                    $selDist = ($taluk['t_id'] == $getRowDetails['t_id']) ? 'selected="selected"' : '';
                                    ?>
                                    <option value="<?php echo $objCommon->html2text($taluk['t_id']) ?>" <?php echo $selDist ?>><?php echo $objCommon->html2text($taluk['t_name']) ?></option>
                            <?php }
                            }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Category</label>
                        <select class="form-control" name="cat_id">
                            <option value="">Select Category</option>
                            <?php
                                foreach ($catList as $cat) {
                                    $selDist = ($cat['cat_id'] == $getRowDetails['cat_id']) ? 'selected="selected"' : '';
                                    ?>
                                    <option value="<?php echo $objCommon->html2text($cat['cat_id']) ?>" <?php echo $selDist ?>><?php echo $objCommon->html2text($cat['cat_name']) ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Type</label>
                        <select class="form-control" name="hostel_type">
                            <option value="0">Select Type</option>
                            <option value="1" <?php echo ($getRowDetails['hostel_type']==1) ? 'selected="selected"' : ''; ?>>Boys Hostel</option>
                            <option value="2" <?php echo ($getRowDetails['hostel_type']==2) ? 'selected="selected"' : ''; ?>>Girls Hostel</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Place</label>
                        <input type="text" name="hostel_place" id="hostel_place" class="form-control" value="<?php echo ($getRowDetails['hostel_place'])?$objCommon->html2text($getRowDetails['hostel_place']):''?>" placeholder="Place Name" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Latitude</label>
                        <input type="text" name="hostel_latitude" id="hostel_latitude" class="form-control" value="<?php echo ($getRowDetails['hostel_latitude'])?$objCommon->html2text($getRowDetails['hostel_latitude']):''?>" placeholder="Enter Latitude" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Longitude</label>
                        <input type="text" name="hostel_longitude" id="hostel_longitude" class="form-control" value="<?php echo ($getRowDetails['hostel_longitude'])?$objCommon->html2text($getRowDetails['hostel_longitude']):''?>" placeholder="Enter Longitude" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hostel Strength</label>
                        <input type="text" name="hostel_strength" id="hostel_strength" class="form-control" value="<?php echo ($getRowDetails['hostel_strength'])?$objCommon->html2text($getRowDetails['hostel_strength']):''?>" placeholder="Enter Count of Students" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">GO Dated</label>
                        <input type="text" name="hostel_godate" id="hostel_godate" class="form-control datepicker" value="<?php echo ($getRowDetails['hostel_godate'])?$objCommon->frontEndDate($getRowDetails['hostel_godate']):''?>" placeholder="Enter Count of Students" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Building Type</label>
                        <select class="form-control" name="building_type">
                            <option value="0">Select Type</option>
                            <option value="1" <?php echo ($getRowDetails['building_type']==1) ? 'selected="selected"' : ''; ?>>Own</option>
                            <option value="2" <?php echo ($getRowDetails['building_type']==2) ? 'selected="selected"' : ''; ?>>Rent</option>
                            <option value="3" <?php echo ($getRowDetails['building_type']==3) ? 'selected="selected"' : ''; ?>>Ren</option>
                        </select>
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-7">
        <section class="panel">
            <header class="panel-heading">Hostel List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <select name="t_id" id="tID">
                                <option value="">Select Taluk</option>
                                <?php foreach($talukSearchList as $tq){?>
                                <option value="<?php echo $tq['t_id']; ?>" <?php echo ($tq['t_id']==$_GET['t_id'])?'selected':''; ?>><?php echo $tq['t_name']; ?></option>
                                <?php }?>
                            </select>
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="20%">Taluk Name</th>
                            <th width="20%">Hostel Place</th>
                            <th width="20%">Hostel Type</th>
                            <th width="20%">Hostel Strength</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['t_name']); ?></td>
                                    <td><?php echo $objCommon->html2text($list['hostel_place']); ?></td>
                                    <td><?php echo ($list['hostel_type']==1)?'Boys':'Girls'; ?></td>
                                    <td><?php echo $objCommon->html2text($list['hostel_strength']); ?></td>
                                    <td>
                                        <a href="?page=hostel-images&hostelId=<?php echo $list['hostel_id']?> "class="actionLink" title="Upload Images"><i class="fa fa-upload"></i></a>&nbsp;&nbsp;
                                        <a href="?page=hostels&nId=<?php echo $list['hostel_id']?> "
                                           class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=hostels&dId=<?php echo $list['hostel_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="7">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script type="text/javascript">
    function selectTaluk(a){
        var distId =   $(a).val();
        $("#talukId").html('<option value="">Select Taluk</option>');
        if(distId!=""){
            $.get("<?php echo SITE_ROOT;?>admin/access/taluk-list.php",{dID:distId},
            function(data){
                if(data!=""){
                    $("#talukId").html(data);
                }
            });
        }
    }
    $(function() {
        $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy'});
    });
</script>