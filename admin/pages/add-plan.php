<?php
$objPlans			  		=	new component_plans();
if(isset($_GET['nId'])){
    $nId            =   $objCommon->esc($_GET['nId']);
    $notificationDetails    =   $objPlans->getRow("plan_id=$nId");
}
?>
<div class="page-heading">
    <h3>Component Plan</h3>
    <ul class="breadcrumb">
        <li><a href="#">Component Plan</a></li>
        <li class="active"> Add Plan </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Add Plan
            </header>
            <div class="panel-body">
                <form role="form" method="post" action="access/add-plan.php">
                    <?php
                    if(isset($_GET['nId'])) { ?>
                        <input type="hidden" name="editId" value="<?php echo $nId; ?>">
                    <?php
                    }
                    ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Plan Name</label>
                        <input type="text" name="plan_title" class="form-control" value="<?php echo $objCommon->html2text(($notificationDetails['plan_title'])); ?>" id="exampleInputEmail1" placeholder="Enter Title">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Plan Content</label>
                        <textarea class="form-control ckeditor" name="plan_content" rows="6"><?php echo $objCommon->displayEditor($notificationDetails['plan_content']); ?></textarea>
                    </div>
                    <!--<div class="form-group">
                        <label for="exampleInputFile">Plan Banner</label>
                        <input type="file" id="exampleInputFile">
                        <p class="help-block">Example block-level help text here.</p>
                    </div>-->
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckfinder.js"></script>