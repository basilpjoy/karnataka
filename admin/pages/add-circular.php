<?php
$objCirculars			  	=	new circulars();
$nId                        =	$objCommon->esc($_GET['nId']);
if($nId){
    $getRowDetails          =	$objCirculars->getRow("circular_id=".$nId);
}
?>
<div class="page-heading">
    <h3>Circulars</h3>
    <ul class="breadcrumb">
        <li><a href="#">Circulars</a></li>
        <li class="active"> Add Circular </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Add Circular
            </header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-circular.php" enctype="multipart/form-data">
                    <?php
                    if(isset($_GET['nId'])) { ?>
                        <input type="hidden" name="editId" value="<?php echo $nId; ?>">
                        <?php
                    }
                    ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Circular Name</label>
                        <input required type="text" name="circular_title" id="district_code" class="form-control" value="<?php echo ($getRowDetails['circular_title'])?$objCommon->html2text($getRowDetails['circular_title']):''?>" placeholder="Enter Name" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Circular Content</label>
                        <textarea class="form-control ckeditor" name="circular_content" rows="6"><?php echo $objCommon->displayEditor($getRowDetails['circular_content']); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Circular Date</label>
                        <input required type="text" name="circular_date" id="circular_date" class="form-control datepicker" value="<?php echo ($getRowDetails['circular_date'])?$objCommon->frontEndDate($getRowDetails['circular_date']):''?>" placeholder="Enter Date" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Circular File</label>
                        <input name="circularFile" type="file" id="exampleInputFile">
                        <p class="help-block">Please Upload PDF only.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Circular Order</label>
                        <input type="text" name="circular_order" id="circular_order" class="form-control" value="<?php echo ($getRowDetails['circular_order'])?$objCommon->html2text($getRowDetails['circular_order']):''?>" placeholder="Enter Order" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Circular Status</label>
                        <select class="form-control" name="circular_status">
                            <option value="1"<?php echo ($getRowDetails['circular_status']==1)?' selected':''; ?>>Enable</option>
                            <option value="0"<?php echo (isset($getRowDetails['circular_status'])&&$getRowDetails['circular_status']==0)?' selected':''; ?>>Disable</option>
                        </select>
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
</div>
<script>
    $(function() {
        $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy'});
    });
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckfinder.js"></script>