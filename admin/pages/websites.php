<?php
$objReports                   =	new useful_links();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
if($nId){
    $getRowDetails          =	$objReports->getRow("link_id=".$nId);
}
if($dId){
    $objReports->delete("link_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:index.php?page=websites");
    exit;
}
$sql						 .= "SELECT * FROM useful_links WHERE  link_category=1 ";
if($search){
    $sql					.= " AND (	link_title LIKE '%".$search."%' OR 	link_id LIKE '%".$search."%')";
}
$sql						 .= " ORDER by 	link_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objReports->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Websites</h3>
    <ul class="breadcrumb">
        <li><a href="#">Usefull Links</a></li>
        <li class="active"> Websites </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add Websites</header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-usefullinks.php">
                    <input type="hidden" name="link_category" value="1">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Website Name</label>
                        <input type="text" name="link_title" id="link_title" class="form-control" value="<?php echo ($getRowDetails['link_title'])?$objCommon->html2text($getRowDetails['link_title']):''?>" placeholder="Enter Name" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Website Link</label>
                        <input type="text" name="link_url" id="link_url" class="form-control" value="<?php echo ($getRowDetails['link_url'])?$objCommon->html2text($getRowDetails['link_url']):''?>" placeholder="Enter Link" required >
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">Website List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="8%">No</th>
                            <th width="60%">Website Name</th>
                            <th width="10%">Website Link</th>
                            <th width="10%">ID</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['link_title']); ?></td>
                                    <td><a href="<?php echo $objCommon->html2text($list['link_url']); ?>">View Link</a></td>
                                    <td><?php echo $list['link_id']; ?></td></td>
                                    <td>
                                        <a href="?page=websites&nId=<?php echo $list['link_id']?> "
                                           class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=websites&dId=<?php echo $list['link_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="5">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    var Script = function () {
        $.validator.setDefaults({
            submitHandler: function() { alert("submitted!"); }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#add_district").validate();
        });
    }();
</script>