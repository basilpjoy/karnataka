<?php
$objColonies                 =	new colonies();
$objDistricts               =	new districts();
$objGeoCat                   =	new geo_categories();
$objGeoSubCats               =	new geo_sub_categories();
$objBasicLivings             =	new basic_living();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
$tID                        =	$objCommon->esc($_GET['t_id']);
if($nId){
    $getRowDetails          =	$objColonies->getRow("colony_id=".$nId);
    $typeCat   =   $objGeoCat->getRow("gcat_id=".$getRowDetails['gcat_id']);
}
if($dId){
    $objColonies->delete("colony_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:?page=colonies");
    exit;
}
$sql						 .= "SELECT c.*,gc.gcat_name,gsc.gscat_name FROM colonies AS c LEFT JOIN geo_categories AS gc ON c.gcat_id=gc.gcat_id LEFT JOIN geo_sub_categories AS gsc ON c.gscat_id=gsc.gscat_id WHERE 1 ";
if($search){
    $sql					.= " AND (c.colony_name LIKE '%".$search."%' OR gc.gcat_name LIKE '%".$search."%' OR c.community_name LIKE '%".$search."%' OR colony_id LIKE '%".$search."%')";
}
/*if($tID){
    $sql					.= " AND h.t_id=$tID";
}*/
$sql						 .= " ORDER by colony_name ASC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objColonies->listQuery($paginationQuery);
$districtList                   =   $objDistricts->getAll();
$catList                     =   $objGeoCat->getAll();
?>
<div class="page-heading">
    <h3>Colonies</h3>
    <ul class="breadcrumb">
        <li><a href="#">Colonies</a></li>
        <li class="active"> Colonies </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add Colony</header>
            <div class="panel-body">
                <form role="form" id="ad_hostel" method="post" action="access/add-colony.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">District Name</label>
                        <select class="form-control" name="d_id" onchange="selectTaluk(this);" required>
                            <option value="">Select District</option>
                            <?php
                            foreach($districtList as $allDist){
                                $selDist        =   ($allDist['d_id']==$getRowDetails['d_id'])?'selected="selected"':'';
                                ?>
                                <option value="<?php echo $objCommon->html2text($allDist['d_id'])?>" <?php echo $selDist?>><?php echo $objCommon->html2text($allDist['d_name'])?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category Type</label>
                        <select class="form-control" id="gCatType" name="gcat_type" onchange="selectCats(this);" required>
                            <option value="0">Select Type</option>
                            <option value="1"<?php echo ($typeCat['gcat_type']==1)?' selected':''; ?>>Taluk</option>
                            <option value="2"<?php echo ($typeCat['gcat_type']==2)?' selected':''; ?>>ULB</option>
                            <option value="3"<?php echo ($typeCat['gcat_type']==3)?' selected':''; ?>>City Corporation</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Taluk/ULB/Corporation</label>
                        <select class="form-control" name="gcat_id" id="gcat_id" onchange="selectGrama(this);" required>
                            <option value="0">Select</option>
                            <?php
                            if($nId){
                            $catList    =   $objGeoCat->getAll("gcat_type=".$typeCat['gcat_type']);
                            foreach($catList as $cat){ ?>
                                <option value="<?php echo $cat['gcat_id']; ?>"<?php echo ($getRowDetails['gcat_id']==$cat['gcat_id'])?' selected':''; ?>><?php echo $cat['gcat_name']; ?></option>
                            <?php }}?>
                        </select>
                    </div>
                    <div class="gamaPanchayath">
                        <?php if($typeCat['gcat_type']==1){
                            $catList        =   $objGeoSubCats->getAll("gcat_id=".$typeCat['gcat_id']);
                            ?>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Grama Panchayath</label>
                                <select class="form-control" name="gscat_id">
                                    <option value="">Select Grama</option>
                                    <?php
                                    foreach($catList as $cat){
                                        $selected   =   ($cat['gscat_id']==$getRowDetails['gscat_id'])?'selected':'';
                                        echo '<option value="'.$cat['gscat_id'].'" '.$selected.'>'.$cat['gscat_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Type of Community</label>
                        <select class="form-control" name="community_type">
                            <option value="0">Select Type</option>
                            <option value="1" <?php echo ($getRowDetails['community_type']==1) ? 'selected="selected"' : ''; ?>>SC</option>
                            <option value="2" <?php echo ($getRowDetails['community_type']==2) ? 'selected="selected"' : ''; ?>>ST</option>
                            <option value="3" <?php echo ($getRowDetails['community_type']==3) ? 'selected="selected"' : ''; ?>>Mix</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Community Name</label>
                        <input type="text" name="community_name" id="community_name" class="form-control" value="<?php echo ($getRowDetails['community_name'])?$objCommon->html2text($getRowDetails['community_name']):''?>" placeholder="Colony Name" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Latitude</label>
                        <input type="text" name="latitude" id="latitude" class="form-control" value="<?php echo ($getRowDetails['latitude'])?$objCommon->html2text($getRowDetails['latitude']):''?>" placeholder="Enter Latitude" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Longitude</label>
                        <input type="text" name="longitude" id="longitude" class="form-control" value="<?php echo ($getRowDetails['longitude'])?$objCommon->html2text($getRowDetails['longitude']):''?>" placeholder="Enter Longitude" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Colony Name</label>
                        <input type="text" name="colony_name" id="colony_name" class="form-control" value="<?php echo ($getRowDetails['colony_name'])?$objCommon->html2text($getRowDetails['colony_name']):''?>" placeholder="Enter Name" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">No.of Houses</label>
                        <input type="text" name="no_houses" id="no_houses" class="form-control" value="<?php echo ($getRowDetails['no_houses'])?$objCommon->html2text($getRowDetails['no_houses']):''?>" placeholder="No.of Houses" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">No.of Houses Hold</label>
                        <input type="text" name="house_hold" id="house_hold" class="form-control" value="<?php echo ($getRowDetails['house_hold'])?$objCommon->html2text($getRowDetails['house_hold']):''?>" placeholder="No.of Houses Hold" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">No.of Houses Less</label>
                        <input type="text" name="house_less" id="house_less" class="form-control" value="<?php echo ($getRowDetails['house_less'])?$objCommon->html2text($getRowDetails['house_less']):''?>" placeholder="No.of Houses Less" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Population</label>
                        <input type="text" name="population" id="population" class="form-control" value="<?php echo ($getRowDetails['population'])?$objCommon->html2text($getRowDetails['population']):''?>" placeholder="Population" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Men</label>
                        <input type="text" name="men" id="men" class="form-control" value="<?php echo ($getRowDetails['men'])?$objCommon->html2text($getRowDetails['men']):''?>" placeholder="No. of Men" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Women</label>
                        <input type="text" name="women" id="women" class="form-control" value="<?php echo ($getRowDetails['women'])?$objCommon->html2text($getRowDetails['women']):''?>" placeholder="No. of Women" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Children</label>
                        <input type="text" name="children" id="children" class="form-control" value="<?php echo ($getRowDetails['children'])?$objCommon->html2text($getRowDetails['children']):''?>" placeholder="No. of Children" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Basic Living:</label>
                        <div>
                        <?php
                        $basicLivings   =   $objBasicLivings->getAll("basic_status=1");
                        $livingsArray   =   array();
                        if($nId){
                            $livingsArray   =   unserialize(html_entity_decode($getRowDetails['basic_livings']));
                        }
                        foreach($basicLivings as $basics){
                        ?>
                            <label style="font-weight: 400;"><input value="<?php echo $basics['basic_id']; ?>" <?php echo (in_array($basics['basic_id'],$livingsArray))?' checked':''; ?> type="checkbox" name="basicLivings[]"> &nbsp;<?php echo $basics['basic_name']; ?></label> &nbsp; &nbsp;
                        <?php }?>
                        </div>
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">Colony List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <select name="t_id" id="tID">
                                <option value="">Select Taluk</option>
                                <?php foreach($talukSearchList as $tq){?>
                                <option value="<?php echo $tq['t_id']; ?>" <?php echo ($tq['t_id']==$_GET['t_id'])?'selected':''; ?>><?php echo $tq['t_name']; ?></option>
                                <?php }?>
                            </select>
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Colony Name</th>
                            <th width="20%">Taluk/ULB/Corporation</th>
                            <th width="15%">CommunityType</th>
                            <th width="10%">Population</th>
                            <th width="10%">Men</th>
                            <th width="5%">Women</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            $community  =   array(1=>"SC",2=>"ST",3=>"MIX");
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['colony_name']); ?></td>
                                    <td><?php echo $objCommon->html2text($list['gcat_name']); ?></td>
                                    <td><?php echo $community[$list['community_type']]; ?></td>
                                    <td><?php echo $objCommon->html2text($list['population']); ?></td>
                                    <td><?php echo $objCommon->html2text($list['men']); ?></td>
                                    <td><?php echo $objCommon->html2text($list['women']); ?></td>
                                    <td>
                                        <a href="?page=colony-images&colonyId=<?php echo $list['colony_id']?> "class="actionLink" title="Upload Images"><i class="fa fa-upload"></i></a>&nbsp;&nbsp;
                                        <a href="?page=colonies&nId=<?php echo $list['colony_id']?> " class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=colonies&dId=<?php echo $list['colony_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="7">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script type="text/javascript">
    function selectTaluk(a){
        var distId =   $(a).val();
        $("#talukId").html('<option value="">Select Taluk</option>');
        if(distId!=""){
            $.get("<?php echo SITE_ROOT;?>admin/access/taluk-list.php",{dID:distId},
            function(data){
                if(data!=""){
                    $("#talukId").html(data);
                }
            });
        }
    }
    $(function() {
        $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy'});
    });
    function selectCats(a){
         var catVal     =    $(a).val();
         $.get("ajax/getCategories.php",{typeID:catVal},
         function(data){
            $("#gcat_id").html(data);
         });
     }
    function selectGrama(tlk){
        if($("#gCatType").val()==1) {
            var tlkVal = $(tlk).val();
            if (tlkVal != 0) {
                $.get("ajax/getGramas.php", {talukId: tlkVal}, function (data) {
                    $(".gamaPanchayath").html(data);
                });
            }else{
                $(".gamaPanchayath").html('');
            }
        }else{
            $(".gamaPanchayath").html('');
        }
    }
</script>
