<?php
include_once(DIR_ROOT."config/class/notifications.php");
$objNotifications           =	new notifications();
if(isset($_GET['nId'])){
    $nId                    =   $objCommon->esc($_GET['nId']);
    $notificationDetails    =   $objNotifications->getRow("note_id=$nId");
}
?>
<div class="page-heading">
    <h3>Notification</h3>
    <ul class="breadcrumb">
        <li><a href="#">Notifications</a></li>
        <li class="active"> Add Notification </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Add Notification
            </header>
            <div class="panel-body">
                <form role="form" method="post" action="access/add-notification.php" enctype="multipart/form-data">
                    <?php
                    if(isset($_GET['nId'])) { ?>
                        <input type="hidden" name="editId" value="<?php echo $nId; ?>">
                    <?php
                    }
                    ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Notification Title</label>
                        <input type="text" name="notification_title" class="form-control" value="<?php echo $objCommon->html2text(($notificationDetails['notification_title'])); ?>" id="exampleInputEmail1" placeholder="Enter Title">
                    </div>
                    <div class="form-group">
                        <label>Notification Date</label>
                        <input type="text" name="notification_date"  value="<?php echo $objCommon->frontEndDate($notificationDetails['notification_date']); ?>" class="form-control datepicker" placeholder="Notification Date"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Notification Status</label> &nbsp; &nbsp; &nbsp;
                        <label style="font-weight: normal;"><input type="checkbox" name="notification_status" <?php echo ($notificationDetails['notification_status']==1)?' checked':''; ?> value="1"> New</label>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Notification Content</label>
                        <textarea class="form-control ckeditor" name="notification_content" rows="6"><?php echo $objCommon->displayEditor($notificationDetails['notification_content']); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Notification File</label>
                        <input name="notificationFile" type="file" id="exampleInputFile">
                        <p class="help-block">Please Upload PDF only.</p>
                    </div>
                    <!--<div class="form-group">
                        <label for="exampleInputFile">Page Banner</label>
                        <input type="file" id="exampleInputFile">
                        <p class="help-block">Example block-level help text here.</p>
                    </div>-->
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
</div>
<script>
    $(function() {
        $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy'});
    });
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckfinder.js"></script>