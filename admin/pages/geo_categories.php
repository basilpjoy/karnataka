<?php
$objCategories               =	new geo_categories();
$objDistricts               =	new districts();
$nId                        =	$objCommon->esc($_GET['nId']);
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
$gcatType                   =	$objCommon->esc($_GET['gcatType']);
$districtList               =   $objDistricts->getAll();
if($nId){
    $getRowDetails          =	$objCategories->getRow("gcat_id=".$nId);
}
if($dId){
    $objCategories->delete("gcat_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
$sql						 .= "SELECT * FROM geo_categories WHERE 1 ";
if($search){
    $sql					.= " AND (gcat_name LIKE '%".$search."%' OR gcat_id LIKE '%".$search."%')";
}
if($gcatType){
    $sql					.= " AND gcat_type=$gcatType";
}
$sql						 .= " ORDER by gcat_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objCategories->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Geo Categories</h3>
    <ul class="breadcrumb">
        <li><a href="#">Colonies</a></li>
        <li class="active"> Geo Categories </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Add Category</header>
            <div class="panel-body">
                <form role="form" id="add_category" method="post" action="access/add-geo-category.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">District</label>
                        <select class="form-control" name="d_id">
                            <option value="0">Select District</option>
                            <?php foreach($districtList as $district){ ?>
                            <option value="<?php echo $district['d_id']; ?>"<?php echo ($getRowDetails['d_id']==1)?' selected':''; ?>><?php echo $district['d_name']; ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category Type</label>
                        <select class="form-control" name="gcat_type">
                            <option value="0">Select Type</option>
                            <option value="1"<?php echo ($getRowDetails['gcat_type']==1)?' selected':''; ?>>Taluk</option>
                            <option value="2"<?php echo ($getRowDetails['gcat_type']==2)?' selected':''; ?>>ULB</option>
                            <option value="3"<?php echo ($getRowDetails['gcat_type']==3)?' selected':''; ?>>City Corporation</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category Name</label>
                        <input type="text" name="gcat_name" id="gcat_name" class="form-control" value="<?php echo ($getRowDetails['gcat_name'])?$objCommon->html2text($getRowDetails['gcat_name']):''?>" placeholder="Enter Name" required >
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">Category List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <select name="gcatType">
                                <option value="0">Select Type</option>
                                <option value="1"<?php echo ($_GET['gcatType']==1)?' selected':''; ?>>Taluk</option>
                                <option value="2"<?php echo ($_GET['gcatType']==2)?' selected':''; ?>>ULB</option>
                                <option value="3"<?php echo ($_GET['gcatType']==3)?' selected':''; ?>>City Corporation</option>
                            </select>
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="35%">Category Name</th>
                            <th width="35%">Category Type</th>
                            <th width="10%">ID</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){
                                $typeArray  =   array(1=>"Taluk",2=>"ULB",3=>"City Corporation");
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['gcat_name']); ?></td></td>
                                    <td><?php echo $typeArray[$list['gcat_type']]; ?></td></td>
                                    <td><?php echo $list['gcat_id']; ?></td></td>
                                    <td>
                                        <a href="?page=geo_categories&nId=<?php echo $list['gcat_id']?> "
                                           class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=geo_categories&dId=<?php echo $list['gcat_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="4">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>