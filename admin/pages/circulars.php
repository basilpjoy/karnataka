<?php
include_once(DIR_ROOT."config/class/circulars.php");
$objCirculars               =	new circulars();
$dId                        =	$objCommon->esc($_GET['dId']);
$search                     =	$objCommon->esc($_GET['search']);
if($dId){
    $deleteRow  =   $objCirculars->getRow("circular_id=$dId");
    if(file_exists(DIR_ROOT . 'assets/uploads/circulars/' . $deleteRow['circular_file'])){
        unlink(DIR_ROOT . 'assets/uploads/circulars/' . $deleteRow['circular_file']);
    }
    $objCirculars->delete("circular_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
$sql						 .= "SELECT * FROM circulars WHERE 1 ";
if($search){
    $sql					.= " AND (circular_name LIKE '%".$search."%' OR circular_id LIKE '%".$search."%')";
}
$sql						 .= " ORDER by circular_order DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objCirculars->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Circulars</h3>
    <ul class="breadcrumb">
        <li><a href="#">Circulars</a></li>
        <li class="active"> Circular List </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">Scheme List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                            <button class="btn btn-primary search_submit" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="50%">Circular Title</th>
                            <th width="10%">View PDF</th>
                            <th width="10%">Order</th>
                            <th width="10%">Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['circular_title']); ?></td>
                                    <td><a target="_blank" href="<?php echo SITE_ROOT . 'assets/uploads/circulars/'.$objCommon->html2text($list['circular_file']); ?>" class="viewButton">View PDF</a></td></td>
                                    <td><?php echo $objCommon->html2text($list['circular_order']); ?></td>
                                    <td><?php echo ($list['circular_status']==1)?'Enabled':'Disbled'; ?></td>
                                    <td>
                                        <a href="?page=add-circular&nId=<?php echo $list['circular_id']?> "
                                           class="actionLink" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="?page=circulars&dId=<?php echo $list['circular_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="5">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckfinder.js"></script>