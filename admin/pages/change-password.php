<?php
?>
<div class="page-heading">
    <h3>Change Password</h3>
    <ul class="breadcrumb">
        <li><a href="#">Settings</a></li>
        <li class="active"> Change Password </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">Add District</header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/change-password.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Current Password</label>
                        <input type="password" name="current_password" id="current_password" class="form-control" placeholder="Enter Code" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">New Password</label>
                        <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter Name" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Confirm Pasword</label>
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Enter Name" required >
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
</div>
<script language="javascript" type="application/javascript">
    var Script = function () {
        $.validator.setDefaults({
            submitHandler: function() { alert("submitted!"); }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#add_district").validate();
        });
    }();
</script>