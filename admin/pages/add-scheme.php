<?php
include_once(DIR_ROOT."config/class/schemes.php");
$objSchemes			  		=	new schemes();
$nId                        =	$objCommon->esc($_GET['nId']);
if($nId){
    $getRowDetails          =	$objSchemes->getRow("scheme_id=".$nId);
}
?>
<div class="page-heading">
    <h3>Schemes</h3>
    <ul class="breadcrumb">
        <li><a href="#">Schemes</a></li>
        <li class="active"> Add Scheme </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Add Scheme
            </header>
            <div class="panel-body">
                <form role="form" id="add_district" method="post" action="access/add-scheme.php">
                    <?php
                    if(isset($_GET['nId'])) { ?>
                        <input type="hidden" name="editId" value="<?php echo $nId; ?>">
                        <?php
                    }
                    ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Scheme Name</label>
                        <input required type="text" name="scheme_title" id="district_code" class="form-control" value="<?php echo ($getRowDetails['scheme_title'])?$objCommon->html2text($getRowDetails['scheme_title']):''?>" placeholder="Enter Name" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Scheme Content</label>
                        <textarea class="form-control ckeditor" name="scheme_content" rows="6"><?php echo $objCommon->displayEditor($getRowDetails['scheme_content']); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Scheme Order</label>
                        <input type="text" name="scheme_order" id="scheme_order" class="form-control" value="<?php echo ($getRowDetails['scheme_order'])?$objCommon->html2text($getRowDetails['scheme_order']):''?>" placeholder="Enter Name" required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Scheme Status</label>
                        <select class="form-control" name="scheme_status">
                            <option value="1"<?php echo ($getRowDetails['scheme_status']==1)?' selected':''; ?>>Enable</option>
                            <option value="0"<?php echo (isset($getRowDetails['scheme_status'])&&$getRowDetails['scheme_status']==0)?' selected':''; ?>>Disable</option>
                        </select>
                    </div>
                    <input type="hidden" name="editId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </section>
    </div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT;?>admin/ckeditor/ckfinder.js"></script>