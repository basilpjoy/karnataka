<?php
include_once("../../config/site_root.php");
$objGeoSubCat      =   new geo_sub_categories();
$objCommon      =   new common();
if(isset($_GET['talukId'])){
    $talukId         =   $objCommon->esc($_GET['talukId']);
    $catList        =   $objGeoSubCat->getAll("gcat_id=$talukId");
    $result        =   '<div class="form-group"><label for="exampleInputEmail1">Grama Panchayath</label><select class="form-control" name="gscat_id"><option value="">Select Grama</option>';
    foreach($catList as $cat){
        $result        .=   '<option value="'.$cat['gscat_id'].'">'.$cat['gscat_name'].'</option>';
    }
    $result        .=   '</select></div>';
    echo $result;
}