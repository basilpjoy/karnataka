<?php
ob_start();
include_once("config/site_root.php");
$objCommon	=	new common();
$page	=	"home";
if(isset($_GET['page'])&&$_GET['page']!=""){
    $page	=	$_GET['page'];
}
$pageName	=	"app/".$page.".php";
if(!file_exists($pageName)){
    $pageName	=	"pages/home.php";
} 
$objPages                           = 	new pages();
include_once(DIR_ROOT."app/includes/header.php");
include_once($pageName); 
include_once(DIR_ROOT."app/includes/footer.php");
?>