var $	=	require("jquery");
require("bootstrap-webpack");
require("style!../css/owl.carousel.css");
//require("style!../css/style.css");
require("./lib/script");
require("./lib/owl.carousel.min");
require("./lib/html5lightbox");
require("./lib/jquery.newsTicker");
require("./lib/jquery.matchHeight");
// JavaScript Document
$(document).ready(function(e) {
	 $('#shemes-carsouel li').matchHeight(); 
	 $('.alt-middle-sec').matchHeight();
	// new notification vertical scroll
	var nt_example1 = $('#nt-example1').newsTicker({
		row_height: 103,
		max_rows: 3,
		duration: 4000,
		prevButton: $('#news-left'),
		nextButton: $('#news-right')
	});
	//owl carsoule schems
    $('#shemes-carsouel').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:3000,
        navText:['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });
	$('#searchCircular').submit(function (event) { 
		var searchval = $('#searchval').val();
		window.location.href='http://localhost/karnataka/circular/?search='+searchval;
		return false;
	});
});