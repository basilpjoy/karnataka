var $	=	require("jquery");

require("./lib/bootstrap.min");
require("style!../css/bootstrap.min.css");


//require("bootstrap-webpack");
require("./lib/jquery.matchHeight");
require("style!../css/owl.carousel.css");
//require("style!../css/style.css");
require("./lib/owl.carousel.min");
require("./lib/script");
require("./lib/jquery.newsTicker");
require("./lib/jquery.matchHeight");

// JavaScript Document
$(document).ready(function(e) {
	 $('#shemes-carsouel li').matchHeight(); 
	 $('.alt-middle-sec').matchHeight();
	// new notification vertical scroll
	var nt_example1 = $('#nt-example1').newsTicker({
		row_height: 103,
		max_rows: 3,
		duration: 4000,
		prevButton: $('#news-left'),
		nextButton: $('#news-right')
	});
	//owl carsoule schems
    $('#shemes-carsouel').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:3000,
        navText:['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });
});