<?php
include_once("db_functions.php");
class hostel_categories extends db_functions
{
	var $tablename = "hostel_categories";
	var $primaryKey = "cat_id";
	var $table_fields = array("cat_id"=>"","cat_name"=>"","cat_alias"=>"","created_on"=>"","cat_status"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}