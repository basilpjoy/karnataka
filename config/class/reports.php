<?php
include_once("db_functions.php");
class reports extends db_functions
{
	var $tablename = "reports";
	var $primaryKey = "report_id";
	var $table_fields = array("report_id"=>"","rcat_id"=>"","report_name"=>"","report_alias"=>"","report_file"=>"","report_date"=>"","report_status"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}