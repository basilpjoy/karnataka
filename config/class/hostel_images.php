<?php
include_once("db_functions.php");
class hostel_images extends db_functions
{
    var $tablename = "hostel_images";
    var $primaryKey = "hi_id";
    var $table_fields = array("hi_id"=>"","hostel_id"=>"","hi_image"=>"","hi_title"=>"");

    function __construct()
    {
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}