<?php
include_once("db_functions.php");
class colonies extends db_functions
{
     var $tablename = "colonies";
     var $primaryKey = "colony_id";
     var $table_fields = array("colony_id"=>"","d_id"=>"","gcat_id"=>"","gscat_id"=>"","colony_name"=>"","colony_desc"=>"","latitude"=>"","longitude"=>"","community_type"=>"","community_name"=>"","no_houses"=>"","house_hold"=>"","house_less"=>"","population"=>"","men"=>"","women"=>"","children"=>"","basic_livings"=>"","colony_status"=>"");

     function __construct()
     {
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
