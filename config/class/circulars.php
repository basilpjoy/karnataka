<?php
include_once("db_functions.php");
class circulars extends db_functions
{
	var $tablename = "circulars";
	var $primaryKey = "circular_id";
	var $table_fields = array("circular_id"=>"","circular_title"=>"","circular_alias"=>"","circular_content"=>"","circular_date"=>"","circular_file"=>"","circular_order"=>"","created_on"=>"","circular_status"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}