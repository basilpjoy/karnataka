<?php
include_once("db_functions.php");
class gallery_category extends db_functions{
    var $tablename = "gallery_category";
    var $primaryKey = "gc_id";
    var $table_fields = array("gc_id"=>"","gc_name"=>"","gc_alias"=>"");

    function __construct(){
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}
?>