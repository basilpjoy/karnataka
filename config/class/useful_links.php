<?php
include_once("db_functions.php");
class useful_links extends db_functions
{
	var $tablename = "useful_links";
	var $primaryKey = "link_id";
	var $table_fields = array("link_id"=>"","link_category"=>"","link_title"=>"","link_url"=>"","link_staus"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}
?>