<?php
include_once("db_functions.php");
class pages extends db_functions
{
	var $tablename = "pages";
	var $primaryKey = "page_id";
	var $table_fields = array("page_id"=>"","page_title"=>"","page_alias"=>"","page_banner"=>"","page_content"=>"","created_on"=>"","page_parent"=>"","footer_page"=>"","page_status"=>"","meta_title"=>"","meta_keywords"=>"","meta_description"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}
?>