<?php
include_once("db_functions.php");
class gallery_images extends db_functions{
    var $tablename = "gallery_images";
    var $primaryKey = "gi_id";
    var $table_fields = array("gi_id"=>"","gc_id"=>"","gi_title"=>"","gi_url"=>"","gi_createdon"=>"","gi_status"=>"");

    function __construct(){
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}
?>