<?php
include_once("db_functions.php");
class geo_sub_categories extends db_functions
{
    var $tablename = "geo_sub_categories";
    var $primaryKey = "gscat_id";
    var $table_fields = array("gscat_id"=>"","gcat_id"=>"","gscat_name"=>"","gscat_status"=>"");

    function __construct()
    {
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}