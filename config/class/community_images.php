<?php
include_once("db_functions.php");
class community_images extends db_functions{
     var $tablename = "community_images";
     var $primaryKey = "chi_id";
     var $table_fields = array("chi_id"=>"","ch_id"=>"","chi_image"=>"","chi_title"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}