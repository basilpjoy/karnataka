<?php
include_once("db_functions.php");
class colony_images extends db_functions
{
    var $tablename = "colony_images";
    var $primaryKey = "ci_id";
    var $table_fields = array("ci_id"=>"","colony_id"=>"","ci_image"=>"","ci_title"=>"");

    function __construct()
    {
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}