<?php
include_once("db_functions.php");
class taluk extends db_functions{
    var $tablename = "taluk";
    var $primaryKey = "t_id";
    var $table_fields = array("t_id"=>"","d_id"=>"","t_name"=>"","t_alias"=>"","t_code"=>"");

    function __construct(){
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}
?>