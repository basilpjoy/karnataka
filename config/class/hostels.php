<?php
include_once("db_functions.php");
class hostels extends db_functions
{
	var $tablename = "hostels";
	var $primaryKey = "hostel_id";
	var $table_fields = array("hostel_id"=>"","d_id"=>"","t_id"=>"","cat_id"=>"","hostel_type"=>"","hostel_place"=>"","hostel_name"=>"","hostel_alias"=>"","hostel_content"=>"","hostel_latitude"=>"","hostel_longitude"=>"","hostel_strength"=>"","hostel_godate"=>"","building_type"=>"","hostel_status"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}