<?php
include_once("db_functions.php");
class rti_main extends db_functions
{
     var $tablename = "rti_main";
     var $primaryKey = "rtmain_id";
     var $table_fields = array("rtmain_id"=>"","rtmain_title"=>"","rtmain_alias"=>"","rtmain_order"=>"","rtmain_status"=>"");

     function __construct()
     {
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
