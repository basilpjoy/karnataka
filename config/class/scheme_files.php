<?php
include_once("db_functions.php");
class scheme_files extends db_functions
{
	var $tablename = "scheme_files";
	var $primaryKey = "sp_id";
	var $table_fields = array("sp_id"=>"","scheme_id"=>"","sp_title"=>"","sp_file"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}
?>