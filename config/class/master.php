<?php
include_once("db_functions.php");
class master extends db_functions 
{
	var $todays_date;
	var $tablename			= 	"master";
	var $primaryKey			= 	"master_id";
	var $table_fields	  	=   array('master_id' => "","username"=>'','password'=>"",'email' => "",'name' => "",'master_image' => "",'type' => "",'status' => "");

	function master()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>