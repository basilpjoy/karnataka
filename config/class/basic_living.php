<?php
include_once("db_functions.php");
class basic_living extends db_functions
{
    var $tablename = "basic_living";
    var $primaryKey = "basic_id";
    var $table_fields = array("basic_id"=>"","basic_name"=>"","basic_status"=>"");

    function __construct()
    {
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}