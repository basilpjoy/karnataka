<?php
include_once("db_functions.php");
class contact_category extends db_functions
{
	var $tablename = "contact_category";
	var $primaryKey = "ccat_id";
	var $table_fields = array("ccat_id"=>"","ccat_name"=>"","ccat_alias"=>"","ccat_status"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}