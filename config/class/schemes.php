<?php
include_once("db_functions.php");
class schemes extends db_functions
{
	var $tablename = "schemes";
	var $primaryKey = "scheme_id";
	var $table_fields = array("scheme_id"=>"","scheme_title"=>"","scheme_alias"=>"","scheme_content"=>"","scheme_order"=>"","scheme_status"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}
?>