<?php
include_once("db_functions.php");
class component_plans extends db_functions
{
	var $tablename = "component_plans";
	var $primaryKey = "plan_id";
	var $table_fields = array("plan_id"=>"","plan_title"=>"","plan_alias"=>"","plan_content"=>"","plan_created_on"=>"","plan_status"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}