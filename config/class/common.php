<?php
class common
{
	public function esc($s){ 
	   $s				= htmlentities($s);
	   $s				= str_replace("'","&#39;",$s);
	   $s				= mysql_real_escape_string($s);
	   return $s;
	}
	function adminLogin(){
		$admin	=	false;
		if(isset($_SESSION['master'])){
			$admin	=	true;
		}
		return $admin;
	}
	function employeeLogin(){
		$admin	=	false;
		if(isset($_SESSION['employee'])){
			$admin	=	true;
		}
		return $admin;
	}
	function adminCheck(){
		if(!isset($_SESSION['master'])){
			header("location:../login.php");
			exit();
		}
	}
	public function html2text($s){
		//htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
		
		$s				= html_entity_decode($s);
		$s				= strip_tags($s,'<h1><h2><h3><h4><h5><h6><table><tr><td><th><p><ul><li><b><strong><i><u><div><a><img><br>');
		$s				= stripslashes($s);
		$s				= str_replace("\r\n","<br/>",$s);
		$s				= str_replace("\\r\\n","<br/>",$s);
		$s				= str_replace("\\n","<br/>",$s);
		$s    			= str_replace("'","&#39;",$s);
		$s				= nl2br($s);
		//$s 				= filter_var($s, FILTER_SANITIZE_STRING);
		
		return $s;
	}
	function html2textarea($s){
		$s				= html_entity_decode($s);
		$s				= str_replace("\r\n","<br/>",$s);
		$s				= str_replace("\\r\\n","<br/>",$s);
		$s				= str_replace("<br/>","\n",$s);
		return $s;
	}
	function displayEditor($s){
		/*$s				= stripslashes($s);
		$s				= html_entity_decode($s);
		$s				= str_replace("rn","",$s);
		return $s;*/
		$s				= html_entity_decode($s);
		filter_var($s, FILTER_SANITIZE_STRING);
		$s				= str_replace("\\r\\n","",$s);
		$s				= str_replace("\r\n","",$s);
		return $s;
	}
	function addMsg($message,$type){
		$messageArray	=	array("msg"=>$message,"type"=>$type);
		$_SESSION["message"]=$messageArray;
	}
	/*function displayMsg(){
		if(isset($_SESSION['message'])){
			$message	=	$_SESSION['message']["msg"];
			$type		=	$_SESSION['message']["type"];
			$catImages	=	array("success"=>"images/success.png","failure"=>"images/failure.png");
			$catImage	=	($type==1)?$catImages['success']:$catImages['failure'];
			$selectClass=	($type==1)?"success":"danger";
			$messageBox	=	 '<div class="alert  alert-'.$selectClass.' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> '.$message.'</div>';
			$messageBox	=	'<div class="notification">
        <div class="container-notification">
        <i class="fa fa-close close-notify"></i>
            <div class="row">
                <div class="col-sm-2 less-padding-r">
                    <img class="img-responsive" src="'.$catImage.'" />
                </div>
                <div class="col-sm-8 less-padding-l">
                <div class="content-notofocation">
            <p>'.$message.'</p>
        </div>
                </div>
            </div>
        </div>
    </div>';
							unset($_SESSION['message']);
							if(isset($_SESSION['sect'])){
								unset($_SESSION['sect']);
							}
							return $messageBox;
		}
	}*/

	function displayMsg(){
		if(isset($_SESSION['message'])){
			$message	=	$_SESSION['message']["msg"];
			$type		=	$_SESSION['message']["type"];
			$selectClass=	($type==1)?"success":"danger";
			$messageBox	=	 '<div class="alert  alert-'.$selectClass.' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> '.$message.'</div>';
							unset($_SESSION['message']);
							if(isset($_SESSION['sect'])){
								unset($_SESSION['sect']);
							}
							return $messageBox;
		}
	}
	function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	function randStrGen($len){
		$result = "";
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}
	function randStrGenSpecial($len){
		$result = "";
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789@$*_";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}
	function randStrGenDigit($len){
		$result = "";
		$chars = "0123456789";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}
	function checkEmailverification(){
		global $getUserDetails;
		if($getUserDetails['email_validation'] !=1 ){
			echo '<div class="alert alert-warning" role="alert"><strong>Warning ! </strong> Your Account has not yet been verified..</div>';
		}
	}
	function getAlias($title){
		$alias	=	strtolower($title);		
		$alias	=	preg_replace("/[^a-z0-9]+/", "-", $alias);
		$alias	=	rtrim($alias, '-');
		$alias	=	ltrim($alias, '-');
		return $alias;
	}
	function createSlug($title){
		$alias	=	strtolower($title);		
		$alias	=	preg_replace("/[^a-z0-9]+/", "_", $alias);
		$alias	=	rtrim($alias, '_');
		$alias	=	ltrim($alias, '_');
		return $alias;
	}
	function getAge($birthDate){
		$age	=	floor((time()-strtotime($birthDate))/31556926);
		return $age;
	}
	function limitWords($pageContent,$limit){
		$resultWord		=	$this->displayEditor($pageContent);
		if(strlen($resultWord)>$limit){
			$resultWord	=	mb_substr($resultWord,0,$limit,'UTF-8')."...";
		}
		return $resultWord;
	}
	function smallWords($str,$count){
		if (strlen($str) > $count){
			$str = wordwrap($str, $count);
			$str = substr($str, 0, strpos($str, "\n"));
		}
		return $str;
	}
	function time_elapsed_string($ptime)
	{
		$etime = (strtotime($this->local_time(date("Y-m-d H:i:s")))) - $ptime;
		if ($etime < 1){
			return '0 seconds';
		}
		$a = array( 365 * 24 * 60 * 60  =>  'year',
					 30 * 24 * 60 * 60  =>  'month',
						  24 * 60 * 60  =>  'day',
							   60 * 60  =>  'hour',
									60  =>  'minute',
									 1  =>  'second'
					);
		$a_plural = array( 'year'   => 'years',
						   'month'  => 'months',
						   'day'    => 'days',
						   'hour'   => 'hours',
						   'minute' => 'minutes',
						   'second' => 'seconds'
					);
	
		foreach ($a as $secs => $str)
		{
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return '<span class="five_moths">'.$r.'</span>' . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
			}
		}
	}
	function addIMG($file,$folder,$name,$width,$height,$thump){
		$ret="";
		if(isset($file)){
			$ext=explode(".",$file['name']);
			$ext=(count($ext)!=0)?strtolower($ext[count($ext)-1]):"";
			$path="$folder$name.$ext";
			$name="$name.$ext";
			if(copy($file['tmp_name'],$path)){
				$proc=false;
				switch($ext){
					case "jpg": $proc=true; $im=imagecreatefromjpeg($path); break;
					case "jpeg": $proc=true; $im=imagecreatefromjpeg($path); break;
					case "gif": $proc=true; $im=imagecreatefromgif($path); break;
					case "png": $proc=true; $im=imagecreatefrompng($path); break;
				}
				if($proc){
					$ow=imagesx($im);
					$oh=imagesy($im);
					$bow=$ow;
					$boh=$oh;
					$posX=0;
					$posY=0;
					if($ow>$width||$oh>$height){
						if($thump){ 
							$cmp=1; 
							if($oh>$ow){ $cmp = $ow/$width; }
							if($ow>$oh){ $cmp = $oh/$height; }
							if($ow==$oh){ $cmp = $oh/$height; }
							$ow=round($ow/$cmp); 
							$oh=round($oh/$cmp);
							if($ow<$width){
								$cmp = $ow/$width;
								$ow=round($ow/$cmp); 
								$oh=round($oh/$cmp);
							}
							if($oh<$height){
								$cmp = $oh/$height;
								$ow=round($ow/$cmp); 
								$oh=round($oh/$cmp);
							}
						}else{
							$cmp=1; 
							if($ow>$oh){ $cmp = $ow/$width; }
							if($ow<$oh){ $cmp = $oh/$height; }
							if($ow==$oh){ $cmp = $oh/$height; }
							$ow=round($ow/$cmp); 
							$oh=round($oh/$cmp);
						}
					}
					if($thump){ 
						$posX=round(($width-$ow)/2); 
						$posY=round(($height-$oh)/2); 
					}
					$bow1=$ow; 
					$boh1=$oh; 
					if($thump){ 
						$ow=$width; 
						$oh=$height; 
					}
					$newImg = imagecreatetruecolor($ow,$oh);
					if($ext=="png"){
						imagealphablending($newImg, false);
						imagesavealpha($newImg,true);
						$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
						imagefilledrectangle($newImg, 0, 0, $ow, $oh, $transparent);
					}
					imagecopyresampled($newImg, $im, $posX, $posY, 0, 0 , $bow1, $boh1, $bow, $boh);
					if($ext=="png"){ 
						imagepng($newImg,$path,9); 
					}else if($ext=="jpg"){ 
						imagejpeg($newImg,$path,90); 
					}else if($ext=="gif"){ 
						imagegif($newImg,$path); 
					}
					$ret= $name;
				}
			}
		}
		return $ret;
	}
	function frontEndDate($date){
		$rtnDate	=	date("d-m-Y",strtotime($date));
		$date	=	date("Y-m-d",strtotime($date));
		if($date=="0000-00-00"||$date=="1970-01-01"||$date==""){
			$rtnDate	=	'';
		}
		return $rtnDate;
	}
	function backEndDate($date){
		$rtnDate	=	date("Y-m-d",strtotime($date));
		if($date==""||$date==" "||$date=="0000-00-00"){
			$rtnDate	=	'';
		}
		return $rtnDate;
	}
	
	function local_time($dateStr){
		$minutes_to_add	=	0;
		$minutes_to_add	=	$_SESSION['localtime'];//+240 if mysql time zone is Arabian Standard Time
		$time = new DateTime(date($dateStr));
		if($minutes_to_add >0){
			$minutes_to_add	=	abs($minutes_to_add);
			$time->sub(new DateInterval('PT' . $minutes_to_add . 'M'));
		}else{
			$minutes_to_add	=	abs($minutes_to_add);
			$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
		}
		
		return $time->format('Y-m-d H:i:s');
	}
	function wordFirst($string){
		$words = explode(" ", $string);
		$acronym = "";
		foreach ($words as $w) {
		  $acronym .= strtoupper($w[0]);
		}
		return $acronym;
	}
	public function getIdAlias($slug){
		$slugExpl							=	explode("-",$slug);
		$slugId							  =	end($slugExpl);
		$SlugAliasArr					    =	array_slice($slugExpl, 0, -1);
		$slugAlias						  =	implode("-",$SlugAliasArr);
		return array($slugId,$slugAlias);
	}
	public function map2Degree($dms){
		$dd	=	$dms;
		$dmsExpld	   =	explode(" ",$dms);
		if(count($dmsExpld)>2) {
			$dd = $dmsExpld[0] + $dmsExpld[1] / 60 + $dmsExpld[2] / 3600;
		}
		return $dd;
	}
}
?>