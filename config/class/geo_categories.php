<?php
include_once("db_functions.php");
class geo_categories extends db_functions
{
    var $tablename = "geo_categories";
    var $primaryKey = "gcat_id";
    var $table_fields = array("gcat_id"=>"","d_id"=>"","gcat_type"=>"","gcat_name"=>"","gcat_status"=>"");

    function __construct()
    {
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}