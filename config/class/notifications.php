<?php
include_once("db_functions.php");
class notifications extends db_functions
{
	var $tablename = "notifications";
	var $primaryKey = "note_id";
	var $table_fields = array("note_id"=>"","notification_title"=>"","notification_alias"=>"","notification_content"=>"","notification_date"=>"","notification_file"=>"","notification_status"=>"","status"=>"","meta_title"=>"","meta_keywords"=>"","meta_description"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}
?>