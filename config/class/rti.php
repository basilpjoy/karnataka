<?php
include_once("db_functions.php");
class rti extends db_functions
{
     var $tablename = "rti";
     var $primaryKey = "rti_id";
     var $table_fields = array("rti_id"=>"","rtmain_id"=>"","rti_number"=>"","rti_file"=>"","rti_order"=>"","rti_status"=>"");

     function __construct()
     {
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
