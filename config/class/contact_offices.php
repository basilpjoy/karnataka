<?php
include_once("db_functions.php");
class contact_offices extends db_functions
{
	var $tablename = "contact_offices";
	var $primaryKey = "officer_id";
	var $table_fields = array("officer_id"=>"","ccat_id"=>"","officer_image"=>"","officer_info"=>"","contact_info"=>"","cont_sidebar"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}