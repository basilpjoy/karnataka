<?php
include_once("db_functions.php");
class home_slider extends db_functions{
    var $tablename = "home_slider";
    var $primaryKey = "hs_id";
    var $table_fields = array("hs_id"=>"","hs_title"=>"","hs_img"=>"","hs_order"=>"");

    function __construct(){
        parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
    }
}
?>