<?php
include_once("db_functions.php");
class report_categories extends db_functions
{
	var $tablename = "report_categories";
	var $primaryKey = "rcat_id";
	var $table_fields = array("rcat_id"=>"","rcat_name"=>"","rcat_alias"=>"","created_on"=>"","rcat_status"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}