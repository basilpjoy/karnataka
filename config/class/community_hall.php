<?php
include_once("db_functions.php");
class community_hall extends db_functions{
     var $tablename = "community_hall";
     var $primaryKey = "ch_id";
     var $table_fields = array("ch_id"=>"","d_id"=>"","gcat_id"=>"","gscat_id"=>"","ch_name"=>"","ch_desc"=>"","latitude"=>"","longitude"=>"","community_type"=>"","community_name"=>"","ch_status"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}