<?php
$objColonies						   =	new colonies();
$id									=	$objCommon->esc($_GET['id']);
if($id){
	$idSlug							=	$objCommon->getIdAlias($id);
	$getColonyDetails				  =	$objColonies->getRowSql("SELECT coln.*,districts.d_name,geoCat.gcat_name,geoSubCat.gscat_name
												FROM colonies AS coln
												LEFT JOIN districts ON coln.d_id = districts.d_id
												LEFT JOIN geo_categories AS geoCat	ON coln.gcat_id = geoCat.gcat_id
												LEFT JOIN geo_sub_categories AS geoSubCat ON coln.gscat_id	=	geoSubCat.gscat_id
												WHERE coln.colony_status=1 AND coln.colony_id=".$idSlug[0]);
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
			<?php
			if($getColonyDetails['colony_id']){
			?>
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3><?php echo $objCommon->html2text($getColonyDetails['colony_name'])?></h3>
                    </div>
                </div>
				<?php
				$getColImages			=	$objColonies->listQuery("SELECT * FROM colony_images WHERE colony_id=".$getColonyDetails['colony_id']);
				if(count($getColImages)>0){
				?>
                <div class="img-pins-org">
                	<div class="img-s">
                    	<div class="new-carsouel">
                        	<?php foreach($getColImages as $allColImages){ ?>
                        	<div class="car-items">
                            	<div class="album-sec img-sec-inside" style="background-image:url('<?php echo SITE_ROOT ?>assets/uploads/colonyImages/<?php echo $objCommon->html2text($allColImages['ci_image'])?>')">
                                    <a href="<?php echo SITE_ROOT ?>assets/uploads/colonyImages/<?php echo $objCommon->html2text($allColImages['ci_image'])?>" data-group="mygroup" title="<?php echo $objCommon->html2text($allColImages['ci_title'])?>" class="overlay_album new-img"></a>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
				<?php
				}
				?>
                <div class="table-details pin-inside">
                	<table class="table table-responsive">
                        <tbody class="border-under">
                           <?php
						   echo ($getColonyDetails['colony_name'])?'<tr><td width="200">Colony Name</td><td>'.$objCommon->html2text($getColonyDetails['colony_name']).'</td></tr>':'';
						   echo ($getColonyDetails['d_name'])?'<tr><td width="200">District</td><td>'.$objCommon->html2text($getColonyDetails['d_name']).'</td></tr>':'';
						   echo ($getColonyDetails['gcat_name'])?'<tr><td width="200">Taluk</td><td>'.$objCommon->html2text($getColonyDetails['gcat_name']).'</td></tr>':'';
						   echo ($getColonyDetails['gscat_name'])?'<tr><td width="200">Grampanchayath</td><td>'.$objCommon->html2text($getColonyDetails['gscat_name']).'</td></tr>':'';
						   if($getColonyDetails['community_type']){
							   $commutype		=	'';
							   if($getColonyDetails['community_type']==1){
								   $commutype    =	'SC';
							   }else if($getColonyDetails['community_type']==2){
								   $commutype    =	'ST';
							   }if($getColonyDetails['community_type']==3){
								   $commutype    =	'Mixed';
							   }
						   ?>
						   <tr><td width="200">Type of Community</td><td><?php echo $commutype?></td></tr>
						   <?php
						   }
						   echo ($getColonyDetails['community_name'])?'<tr><td width="200">Community Name</td><td>'.$objCommon->html2text($getColonyDetails['community_name']).'</td></tr>':'';
						   echo ($getColonyDetails['no_houses'])?'<tr><td width="200">No.of Houses</td><td>'.$objCommon->html2text($getColonyDetails['no_houses']).'</td></tr>':'';
						   echo ($getColonyDetails['house_hold'])?'<tr><td width="200">No.of Houses Hold</td><td>'.$objCommon->html2text($getColonyDetails['house_hold']).'</td></tr>':'';
						   echo ($getColonyDetails['house_less'])?'<tr><td width="200">No.of Houses Less</td><td>'.$objCommon->html2text($getColonyDetails['house_less']).'</td></tr>':'';
						   echo ($getColonyDetails['population'])?'<tr><td width="200">Population</td><td>'.$objCommon->html2text($getColonyDetails['population']).'</td></tr>':'';
						   echo ($getColonyDetails['men'])?'<tr><td width="200">Men</td><td>'.$objCommon->html2text($getColonyDetails['men']).'</td></tr>':'';
						   echo ($getColonyDetails['women'])?'<tr><td width="200">Women</td><td>'.$objCommon->html2text($getColonyDetails['women']).'</td></tr>':'';
						   echo ($getColonyDetails['children'])?'<tr><td width="200">Children</td><td>'.$objCommon->html2text($getColonyDetails['children']).'</td></tr>':'';
						   ?>
                        </tbody>
                    </table>
                </div>
				<?php
				$colBasicLiv				=	unserialize(html_entity_decode($getColonyDetails['basic_livings']));
				if(count($colBasicLiv)>0){
					$basilLivString		 =	implode(",",$colBasicLiv);
					$getBasilLiv			=	$objColonies->listQuery("SELECT * FROM basic_living WHERE basic_id IN (".$basilLivString.") ORDER BY basic_name");
				?>
                <div class="facilities">
                	<div class="head-basic">
                    	<h4>Basic Livings</h4>
                    </div>
					<?php
					foreach($getBasilLiv as $allbasilLiv){
						echo '<span class="items-facilities"><i class="fa fa-tag"></i> '.$objCommon->html2text($allbasilLiv['basic_name']).'</span>';
					}
					?>
                </div>
			<?php
				}
			}else{
				echo '<p>No content found....</p>';
			}
			?>
          	 </div>
			<?php
				include_once(DIR_ROOT."app/widget/right_sidebar.php");
            ?>
        </div>
    </div>
</div>
<!---End Middle Section--->
