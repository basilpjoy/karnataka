<!---Slider--->
<?php
$objHomeSlider                      =   new home_slider();
$objUsefullLinks                    =   new useful_links();
$getHomeSlider                      =   $objHomeSlider->getAll('','hs_order');
?>
<div class="slider">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            foreach($getHomeSlider as $keyAllSlider=>$allHomeSlider){
            ?>
                <div class="item <?php echo ($keyAllSlider==0)?'active':''?>">
                    <img src="<?php echo SITE_ROOT .'assets/uploads/home-slider/'.$objCommon->html2Text($allHomeSlider['hs_img'])?>" alt="<?php echo $objCommon->html2Text($allHomeSlider['hs_title'])?>">
                </div>
            <?php
            }
            ?>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="fa fa-chevron-left fa-lg" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="fa fa-chevron-right fa-lg" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<!---End Slider--->
<?php
$objPages                           =   new pages();
$getPageId2                         =   $objPages->getRowSql("SELECT page_title,page_content FROM pages WHERE page_id=2");
?>
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <!---language Switcher---->
        <div class="language"> </div>
        <!-- End Language Switcher --->
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3><?php echo $objCommon->html2text($getPageId2['page_title']);?></h3>
                    </div>
                    <div class="parah">
                        <?php echo $objCommon->displayEditor($getPageId2['page_content']);?>
                        </div>
                </div>
				<?php
				$getHomeSchemes				=	$objPages->listQuery("SELECT * FROM schemes WHERE scheme_status=1 ORDER BY scheme_id desc LIMIT 10");
				if(count($getHomeSchemes)>0){
				?>
                <div class="Schemes">
                    <div class="head-message">
                        <h3>Schemes</h3>
                    </div>
                    <ul class="shemes-carsouel list-unstyled" id="shemes-carsouel">
                        <?php 
						foreach($getHomeSchemes as $allHomeSchemes){ 
						$getImgArrScheme		=	preg_match_all('/<img[^>]+>/i',html_entity_decode($allHomeSchemes['scheme_content']), $matches);
						if($matches[0][0]){
							$schemeImg		  =	$matches[0][0];
						}else{
							$schemeImg		  =	'<img src="'.SITE_ROOT.'assets/images/schemes/1.jpg">';
						}
						?>
                            <li class="item-carsouel">
                                <div class="img-carsouel"><?php echo $schemeImg?>
                                    <!--<div class="latest">
                                        <span>NEW</span>
                                    </div>-->
                                </div>
                                <div class="scheme-name">
                                    <h5><?php echo $objCommon->html2text($allHomeSchemes['scheme_title']);?></h5>
                                    <p><?php echo $objCommon->limitWords(strip_tags(html_entity_decode($allHomeSchemes['scheme_content'])),150);?></p>
                                    <a href="<?php echo SITE_ROOT.'single-scheme/'. $objCommon->html2text($allHomeSchemes['scheme_alias']).'-'.$objCommon->html2text($allHomeSchemes['scheme_id'])?>" class="read_more read-alt red-gradient" title="Read more">Read More</a>
                                </div>
                            </li>
                        <?php }?>
                    </ul>
                </div>
				<?php
				}
				?>
            </div>
            <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-6">
            <div class="middle_sec alt-middle-sec">
                <div class="head-message">
                    <h3>PDF Files</h3>
                </div>
                <div class="list-files">
                	<ul class="list-unstyled pdf_versions">
                        <?php
                        $pdfFiles   =   $objUsefullLinks->getAll("link_category=2");
                        if(count($pdfFiles)){
                            foreach($pdfFiles as $pdf){
                        ?>
                    	<li><a target="_blank" href="<?php echo SITE_ROOT.'assets/uploads/pdf/'.$pdf['link_url']; ?>"><?php echo $pdf['link_title']; ?></a></li>
                        <?php }}else{ ?>
                            <li>No PDF to display</li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="middle_sec alt-middle-sec">
                <div class="head-message">
                    <h3>Website Links</h3>
                </div>
                <div class="list-files">
                	<ul class="list-unstyled web_versions">
                        <?php
                        $usefullLinks   =   $objUsefullLinks->getAll("link_category=1");
                        if(count($usefullLinks)){
                            foreach($usefullLinks as $link){
                                ?>
                                <li><a target="_blank" href="<?php echo $link['link_url']; ?>"><?php echo $link['link_title']; ?></a></li>
                        <?php }}else{ ?>
                            <li>No Links to display</li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$homegalleryimages			=	$objHomeSlider->listQuery("SELECT * FROM gallery_images WHERE gi_status=1 ORDER BY RAND() LIMIT 4");
if(count($homegalleryimages)>0){
?>
<div class="gallery">
	<?php
	foreach($homegalleryimages as $allHomegallery){
	?>
    <div class="images-gallery" style="background-image:url('<?php echo SITE_ROOT.'assets/uploads/gallery/'.$objCommon->html2text($allHomegallery['gi_url'])?>')"></div>
	<?php
	}
	?>
    <div class="overlay_layer text-center">
        <a href="<?php echo SITE_ROOT.'gallery'?>" title="gallery">View Gallery</a>
    </div>
    <div class="clearfix"></div>
</div>
<?php
}
?>
<!---End Middle Section--->