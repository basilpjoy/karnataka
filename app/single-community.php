<?php
$objCommHalls						  =	new community_hall();
$id									=	$objCommon->esc($_GET['id']);
if($id){
	$idSlug							=	$objCommon->getIdAlias($id);
	$getCommuHalls				  =	$objCommHalls->getRowSql("SELECT hall.*,districts.d_name,geoCat.gcat_name,geoSubCat.gscat_name
												FROM community_hall AS hall 
												LEFT JOIN districts ON hall.d_id = districts.d_id 
												LEFT JOIN geo_categories AS geoCat	ON hall.gcat_id = geoCat.gcat_id
												LEFT JOIN geo_sub_categories AS geoSubCat ON hall.gscat_id	=	geoSubCat.gscat_id 
												WHERE hall.ch_status=1 AND hall.ch_id=".$idSlug[0]);
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
			<?php
			if($getCommuHalls['ch_id']){
			?>
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3><?php echo $objCommon->html2text($getCommuHalls['colony_name'])?></h3>
                    </div>
                </div>
				<?php
				$getColImages			=	$objCommHalls->listQuery("SELECT * FROM community_images WHERE ch_id=".$getCommuHalls['ch_id']);
				if(count($getColImages)>0){
				?>
                <div class="img-pins-org">
                	<div class="img-s">
                    	<div class="new-carsouel">
                        	<?php foreach($getColImages as $allColImages){ ?>
                        	<div class="car-items">
                            	<div class="album-sec img-sec-inside" style="background-image:url('<?php echo SITE_ROOT ?>assets/uploads/colonyImages/<?php echo $objCommon->html2text($allColImages['chi_image'])?>')">
                                    <a href="<?php echo SITE_ROOT ?>assets/uploads/colonyImages/<?php echo $objCommon->html2text($allColImages['chi_image'])?>" data-group="mygroup" title="<?php echo $objCommon->html2text($allColImages['chi_title'])?>" class="overlay_album new-img"></a>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
				<?php
				}
				?>
                <div class="table-details pin-inside">
                	<table class="table table-responsive">
                        <tbody class="border-under">
                           <?php 
						   echo ($getCommuHalls['ch_name'])?'<tr><td width="200">Name</td><td>'.$objCommon->html2text($getCommuHalls['ch_name']).'</td></tr>':'';
						   echo ($getCommuHalls['d_name'])?'<tr><td width="200">District</td><td>'.$objCommon->html2text($getCommuHalls['d_name']).'</td></tr>':'';
						   echo ($getCommuHalls['gcat_name'])?'<tr><td width="200">Taluk</td><td>'.$objCommon->html2text($getCommuHalls['gcat_name']).'</td></tr>':'';
						   echo ($getCommuHalls['gscat_name'])?'<tr><td width="200">Grampanchayath</td><td>'.$objCommon->html2text($getCommuHalls['gscat_name']).'</td></tr>':'';
						   if($getCommuHalls['community_type']){
							   $commutype		=	'';
							   if($getCommuHalls['community_type']==1){
								   $commutype    =	'SC';
							   }else if($getCommuHalls['community_type']==2){
								   $commutype    =	'ST';
							   }if($getCommuHalls['community_type']==3){
								   $commutype    =	'Mixed';
							   }
						   ?>
						   <tr><td width="200">Type of Community</td><td><?php echo $commutype?></td></tr>
						   <?php
						   }
						   echo ($getCommuHalls['no_houses'])?'<tr><td width="200">No.of Houses</td><td>'.$objCommon->html2text($getCommuHalls['no_houses']).'</td></tr>':'';
						   echo ($getCommuHalls['population'])?'<tr><td width="200">Population</td><td>'.$objCommon->html2text($getCommuHalls['population']).'</td></tr>':'';
						   echo ($getCommuHalls['men'])?'<tr><td width="200">Men</td><td>'.$objCommon->html2text($getCommuHalls['men']).'</td></tr>':'';
						   echo ($getCommuHalls['women'])?'<tr><td width="200">Women</td><td>'.$objCommon->html2text($getCommuHalls['women']).'</td></tr>':''; 
						   ?>
                        </tbody>
                    </table>
                </div>
				<?php
				
			}else{
				echo '<p>No content found....</p>';
			}
			?>
          	 </div>
			<?php
				include_once(DIR_ROOT."app/widget/right_sidebar.php");
            ?>
        </div>
    </div>
</div>
<!---End Middle Section--->