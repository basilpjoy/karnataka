<?php
$objHostels			   	   			=	new hostels();
$id									=	$objCommon->esc($_GET['id']);
if($id !=''){
	$idSlug							=	$objCommon->getIdAlias($id);
	$getHostelResidential		   	  =	$objHostels->listQuery("SELECT h.*,t.t_name,d.d_name FROM hostels AS h LEFT JOIN districts AS d ON h.d_id = d.d_id LEFT JOIN taluk AS t ON h.t_id = t.t_id WHERE h.t_id=".$idSlug[0].' AND h.cat_id=3  AND h.hostel_status=1 ORDER BY t.t_name');
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-12">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Hostels</h3>
                    </div>
                </div>
                <div>
                <div class="table-details">
                	<table class="table">
                        <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>District</th>
                                <th>Taluk</th>
                                <th>Place</th>
                                <th>Sanctioned Strength </th>
                                <th>GO Dated (Add):</th>
                                <th>Own/Rent/Ren</th>
                            </tr>
							</thead>
							<tbody class="border-under">
							<?php
							if(count($getHostelResidential)>0){
							foreach($getHostelResidential as $keyResid=>$allResidential){ 
								if($allResidential['building_type']==1){
									$hostelType				=	'Own';
								}else if($allResidential['building_type']==2){
									$hostelType				=	'Rent';
								}if($allResidential['building_type']==3){
									$hostelType				=	'Ren';
								}
								?>
								<tr>
									<td><?php echo ($keyResid+1) ?></td>
									<td><?php echo $objCommon->html2text($allResidential['d_name'])?></td>
									<td><?php echo $objCommon->html2text($allResidential['t_name'])?></td>
									<td><?php echo $objCommon->html2text($allResidential['hostel_place'])?></td>
									<td><?php echo $objCommon->html2text($allResidential['hostel_strength'])?></td>
									<td><?php echo date('d-m-Y',strtotime($allResidential['hostel_godate']));?></td>
									<td><?php echo $hostelType;?></td>
								</tr>
                            <?php 
							}
							}
							?>
                        </tbody>
                    </table>
                </div>
                </div>
          	 </div>
        </div>
    </div>
</div>
<!---End Middle Section--->