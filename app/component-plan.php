<?php
$objComponent				 	   =	new component_plans();
$getPageDetails                 	 =	$objPages->getRowSql("SELECT page_id,page_title,page_content FROM pages WHERE page_id=4");
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3><?php echo $objCommon->html2text($getPageDetails['page_title'])?></h3>
                    </div>
                </div>
                <div class="spcl-component">
                	<div class="spl-cotent-sec">
                    	<?php echo $objCommon->displayEditor($getPageDetails['page_content']);?>
                    </div>
					<?php
					$getComponent			=	$objComponent->listQuery("SELECT plan_id,plan_title,plan_alias FROM component_plans WHERE plan_status=1 ORDER  BY plan_title");
					if(count($getComponent)>0){
					?>
                    <div class="table-details">
                	<table class="table">
                        <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Special Component Plan</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody class="border-under">
                            <?php foreach($getComponent as $keyComponent=>$allGetComponent){ ?>
                            <tr>
                                <td><?php echo ($keyComponent+1) ?></td>
                                <td><?php echo $objCommon->html2text($allGetComponent['plan_title']);?></td>
                                <td><a href="<?php echo SITE_ROOT.'single-component-plan/'.$objCommon->html2text($allGetComponent['plan_alias']).'-'.$objCommon->html2text($allGetComponent['plan_id'])?>" class="read_more read-alt red-gradient">View</a></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
					<?php
					}
					?>
                </div>
          	 </div>
             <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->