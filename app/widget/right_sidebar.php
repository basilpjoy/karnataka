<div class="col-sm-4">
            	<?php 
				if($page != 'notifications'){ 
				$objNotifications		=	new notifications();
				$getNotificationAll	  =	$objNotifications->listQuery("SELECT note_id,notification_title,notification_alias,notification_content,notification_date,notification_status FROM notifications WHERE status=1 ORDER BY notification_status ASC LIMIT 15");
				?>
                <div class="sidebar_right">
                    <div class="head-sidebar yellowGradient">
                        <h4>Notifications
                        	<span class="pull-right nav-news"><i class="fa fa-chevron-left" id="news-left"></i><i class="fa fa-chevron-right" id="news-right"></i></span>
                            <div class="clearfix"></div>
                         </h4>
                    </div>
                    <div class="body-sidebar">
                        <ul class="list-unstyled" id="nt-example1">
                            <?php 
							if(count($getNotificationAll)>0){
								foreach($getNotificationAll as $allNotification){ 
								?>
									<li>
										<div class="date_published text-center">
											<div class="day">
												<h4><?php echo date("d",strtotime($allNotification['notification_date']));?></h4>
											</div>
											<div class="month">
												<span><?php echo date("M",strtotime($allNotification['notification_date']));?></span>
											</div>
										</div>
										<div class="notification-list">
											<h5><a href="<?php echo SITE_ROOT.'single-notification/'.$objCommon->html2text($allNotification['notification_alias']).'-'.$objCommon->html2text($allNotification['note_id'])?>" title="News Heading">
												<?php echo $objCommon->limitWords($allNotification['notification_title'],50); echo ($allNotification['notification_status']==1)?'<span class="newNotify">New</span>':''?></a>
											</h5>
											<p><a href="<?php echo SITE_ROOT.'single-notification/'.$objCommon->html2text($allNotification['notification_alias']).'-'.$objCommon->html2text($allNotification['note_id'])?>" title="News Deatils"><?php echo $objCommon->limitWords(strip_tags($allNotification['notification_content']),70);?></a></p>
											<a href="<?php echo SITE_ROOT.'single-notification/'.$objCommon->html2text($allNotification['notification_alias']).'-'.$objCommon->html2text($allNotification['note_id'])?>" class="read_more" title="Read More">Read More <i class="fa fa-caret-right"></i></a>
										</div>
									</li>
								<?php 
								}
							}
							?>
                        </ul>
                    </div>
                </div>
                <?php }?>
                <div class="sidebar_right">
                	<div class="pin_locale text-center">
                    	<a href="<?php echo SITE_ROOT.'pin'?>" title="pins of all SC n ST">Pins of all SC n ST colonies</a>
                    </div>
                </div>
    <div class="sidebar_right">
        <div class="head-sidebar yellowGradient">
            <h4>Officers Contacts </h4>
        </div>
        <div class="body-sidebar">
            <ul class="list-unstyled cast-list">
                <?php
                $objContactOfc = new contact_offices();
                $ContactList = $objContactOfc->listQuery("SELECT * FROM contact_offices  WHERE cont_sidebar = 1 ORDER BY ccat_id LIMIT 5");
                foreach($ContactList as $allContactLis){
                ?>
                    <li>
                        <div class="notification-list full-width">
                            <h5><a href="<?php echo SITE_ROOT.'contactus' ?>" title="News Heading"><?php echo strip_tags($objCommon->displayEditor($allContactLis['officer_info'])); ?></a></h5>
                        </div>
                    </li>
                <?php } ?>
            </ul>
            <a href="<?php echo SITE_ROOT.'contactus'; ?>" class="read_more pull-right" title="Read More">Read More <i class="fa fa-caret-right"></i></a>

            <div class="clearfix"></div>
        </div>
    </div>
</div>