<?php
$objHostels						   =	new hostels();
$id									=	$objCommon->esc($_GET['id']);
if($id){
	$idSlug							=	$objCommon->getIdAlias($id);
	$getHostalDetails				  =	$objHostels->getRowSql("SELECT h.*,t.t_name,d.d_name,hCat.cat_name FROM hostels AS h LEFT JOIN districts AS d ON h.d_id = d.d_id LEFT JOIN taluk AS t ON h.t_id = t.t_id 
	LEFT JOIN hostel_categories AS hCat ON hCat.cat_id = h.cat_id
	WHERE h.hostel_id=".$idSlug[0]." AND h.hostel_alias='".$idSlug[1]."' AND h.hostel_status=1");
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
			<?php
			if($getHostalDetails['hostel_id']){
			?>
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3><?php echo $objCommon->html2text($getHostalDetails['hostel_name'])?></h3>
                    </div>
                </div>
				<?php
				$getHostImages			=	$objHostels->listQuery("SELECT * FROM hostel_images WHERE hostel_id=".$getHostalDetails['hostel_id']);
				if(count($getHostImages)>0){
				?>
                <div class="img-pins-org">
                	<div class="img-s">
                    	<div class="new-carsouel">
                        	<?php foreach($getHostImages as $allHostImages){ ?>
                        	<div class="car-items">
                            	<div class="album-sec img-sec-inside" style="background-image:url('<?php echo SITE_ROOT ?>assets/uploads/hostelImages/<?php echo $objCommon->html2text($allHostImages['hi_image'])?>')">
                                    <a href="<?php echo SITE_ROOT ?>assets/uploads/hostelImages/<?php echo $objCommon->html2text($allHostImages['hi_image'])?>" data-group="mygroup" title="<?php echo $objCommon->html2text($allHostImages['hi_title'])?>" class="overlay_album new-img"></a>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
				<?php
				}
				?>
                <div class="table-details pin-inside">
                	<table class="table table-responsive">
                        <tbody class="border-under">
                           <?php 
						   echo ($getHostalDetails['hostal_name'])?'<tr><td width="200">Hostel Name</td><td>'.$objCommon->html2text($getHostalDetails['hostal_name']).'</td></tr>':'';
						   echo ($getHostalDetails['d_name'])?'<tr><td width="200">District</td><td>'.$objCommon->html2text($getHostalDetails['d_name']).'</td></tr>':'';
						   echo ($getHostalDetails['t_name'])?'<tr><td width="200">Taluk</td><td>'.$objCommon->html2text($getHostalDetails['t_name']).'</td></tr>':'';
						   echo ($getHostalDetails['cat_name'])?'<tr><td width="200">Hostel Category</td><td>'.$objCommon->html2text($getHostalDetails['cat_name']).'</td></tr>':'';
						   if($getHostalDetails['hostel_type']){
							   $commutype		=	'';
							   if($getHostalDetails['hostel_type']==1){
								   $hostFor    =	'Boys';
							   }else if($getHostalDetails['hostel_type']==2){
								   $hostFor    =	'Girls';
							   }
						   ?>
						   <tr><td width="200">Hostel For</td><td><?php echo $hostFor?></td></tr>
						   <?php
						   }
						   echo ($getHostalDetails['hostel_place'])?'<tr><td width="200">Place</td><td>'.$objCommon->html2text($getHostalDetails['hostel_place']).'</td></tr>':'';
						   echo ($getHostalDetails['hostel_strength'])?'<tr><td width="200">Sanctioned Strength </td><td>'.$objCommon->html2text($getHostalDetails['hostel_strength']).'</td></tr>':'';
						   echo ($getHostalDetails['hostel_godate'] !='' && $getHostalDetails['hostel_godate'] !='0000-00-00')?'<tr><td width="200">GO Dated</td><td>'.date('d-m-Y',strtotime($getHostalDetails['hostel_godate'])).'</td></tr>':'';
						   if($getHostalDetails['building_type']){
							   $buildType		=	'';
							   if($getHostalDetails['building_type']==1){
								   $buildType    =	'Own';
							   }else if($getHostalDetails['building_type']==2){
								   $buildType    =	'Rent';
							   }else if($getHostalDetails['building_type']==3){
								   $buildType    =	'Ren';
							   }
						   ?>
						   <tr><td width="200">Building Type</td><td><?php echo $buildType?></td></tr>
						   <?php
						   }
						    echo ($getHostalDetails['hostel_content'])?'<tr><td width="200">Description</td><td>'.$objCommon->displayEditor($getHostalDetails['hostel_content']).'</td></tr>':'';
						   ?>
						  
                        </tbody>
                    </table>
                </div>
				<?php
			}else{
				echo '<p>No content found....</p>';
			}
			?>
          	 </div>
			<?php
				include_once(DIR_ROOT."app/widget/right_sidebar.php");
            ?>
        </div>
    </div>
</div>
<!---End Middle Section--->