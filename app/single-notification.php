<?php
$objNotifications		   =	new notifications();
$id					  	 =	$objCommon->esc($_GET['id']);
if($id){
	$idSlug			  	 =	$objCommon->getIdAlias($id);
	$getNotificationSingle  =	$objNotifications->getRowSql("SELECT note_id,notification_title,notification_alias,notification_content,notification_date,notification_file,notification_status FROM notifications WHERE status=1 AND note_id=".$idSlug[0]." AND notification_alias='".$idSlug[1]."'");
}
?>

<!--mini banners -->
<div class="mini-banners" style="background-image:url('assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners-->
<!---Middle Section-->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Notifications</h3>
                    </div>
                    	<div class="notification-lists">
							<?php
							if($getNotificationSingle['note_id']){
							?>
                        	<ul class="list-unstyled">
                            	<li class="notication-sort">
                                	<div class="date_published text-center">
                                    	<div class="day">
                                        	<h4><?php echo date("d",strtotime($getNotificationSingle['notification_date']));?></h4>
                                        </div>
                                        <div class="month">
                                        	<span><?php echo date("M",strtotime($getNotificationSingle['notification_date']));?></span>
                                        </div>
                                    </div>
                                    <div class="notify-details">
                                    	<div class="notication-detials">
                                             <h4 class="media-heading"><?php echo $objCommon->html2text($getNotificationSingle['notification_title']);?></h4>
                                         </div>
                                         <div class="footer-notofication">
                                             <span class="date-full pull-left"><?php echo date("d/m/Y",strtotime($getNotificationSingle['notification_date']));?></span>
                                             <?php if ($getNotificationSingle['notification_file']!="") {?>
                                             <a target="_blank" href="<?php echo SITE_ROOT.'assets/uploads/reports/'.$getNotificationSingle['notification_file'] ?>" class="read_more read-alt red-gradient pull-right" title="Read more">View File</a>
                                             <?php } ?>
                                             <div class="clearfix"></div>
                                         </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="notification-in-detail"><?php echo $objCommon->displayEditor($getNotificationSingle['notification_content']);?>
                            </div>
							<?php
							}else{
								echo '<p>No notification found</p>';
							}
							?>
                        </div>
                     <div class="clearfix"></div>
                </div>
           </div>
            <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section-->