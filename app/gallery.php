<?php
$objGalleryCategory					=	new gallery_category();
$getAlbumNames						 =	$objGalleryCategory->listQuery("SELECT tab.* FROM (SELECT cat.*,img.gi_url,img.gi_title FROM gallery_category AS cat LEFT JOIN gallery_images AS img ON cat.gc_id = img.gc_id ORDER BY img.gi_createdon DESC) AS tab GROUP BY tab.gc_id ");
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Image Gallery</h3>
                    </div>
                    <div class="galleria text-center">
                    	<div class="row">
							<?php
							if(count($getAlbumNames)>0){
								foreach($getAlbumNames as $allAlbumImages){
									?>
									<div class="col-sm-4">
										<div class="album-sec" style="background-image:url('<?php echo SITE_ROOT.'assets/uploads/gallery/'.$objCommon->html2text($allAlbumImages['gi_url'])?>');">
										<a href="<?php echo SITE_ROOT.'single-gallery/'.$objCommon->html2text($allAlbumImages['gc_alias']).'-'.$objCommon->html2text($allAlbumImages['gc_id'])?>" class="overlay_album" title="<?php echo $objCommon->html2text($allAlbumImages['gc_name'])?>"><?php echo $objCommon->html2text($allAlbumImages['gc_name'])?></a>
										</div>
									</div>
									<?php
								}
							}else{
								echo '<p>No Photos Found</p>';
							}
							?>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
           </div>
		   <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->