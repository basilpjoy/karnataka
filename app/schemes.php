<?php
$objSchemes			   	     =	new schemes();
include_once(DIR_ROOT."config/class/pagination-class-front.php");
$pageIdUri		=	htmlspecialchars($_SERVER['REQUEST_URI']);
$pageIdUriExl	 =	explode('?page_id=',$pageIdUri);
$pageId		   =	$pageIdUriExl[1];
$pageIdUriSearch  =	explode('?search=',$pageIdUriExl[0]);
if(count($pageIdUriSearch)>0){
	$searchKeyword			 =	trim($pageIdUriSearch[1],"/");
	if($searchKeyword){
		$searchSql			  =	" AND (scheme_title LIKE '%".$searchKeyword."%' OR scheme_alias LIKE '%".$searchKeyword."%') ";
	}
}
$sqlSchemes              	   	 =	"SELECT * FROM schemes WHERE scheme_status=1 ".$searchSql." ORDER BY scheme_order";
$num_results_per_page           =	10;
$num_page_links_per_page        =	5;
$pg_param 					   =	"";
$pagesection				    =	'';
pagination($sqlSchemes,$num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$schemesList			   	   =	$objSchemes->listQuery($pg_result);
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
	<div class="middle_sec">
        <div class="local-search">
            <div class="search-bar pull-right form-group">
            	<form action="schemes" method="GET" id="searchCircular">
                    <input type="text" placeholder="Search Schemes" class="form-control" id="searchval" value="<?php echo $searchKeyword?>" />
                    <input type="image" class="search-btn" src="<?php echo SITE_ROOT ?>assets/images/search.png" alt="Search" />
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Schemes</h3>
                    </div>
                    <?php
						if(count($schemesList)>0){
							$num	  =	1;
							$mult	 =	(int)($pageId)?($pageId-1):0;
							$num	  =	($num_results_per_page*$mult)+1;
							foreach($schemesList as $allSchemes){
								$getImgArr		=	preg_match_all('/<img[^>]+>/i',html_entity_decode($allSchemes['scheme_content']), $matches);
							?>
                    <div class="schemes-list">
                        <div class="media schemesList">
							<?php
							if($matches[0][0]){
							?>
                            <div class="media-left">
                                <a href="<?php echo SITE_ROOT.'single-scheme/'. $objCommon->html2text($allSchemes['scheme_alias']).'-'.$objCommon->html2text($allSchemes['scheme_id'])?>">
                                    <?php echo $matches[0][0]?>
                                </a>
                            </div>
							<?php
							}
							?>
                            <div class="media-body">
                                <h4 class="media-heading"><?php echo $objCommon->html2text($allSchemes['scheme_title']);?></h4>
                                <p><?php echo $objCommon->limitWords(strip_tags(html_entity_decode($allSchemes['scheme_content'])),150);?></p>
                                 <a href="<?php echo SITE_ROOT.'single-scheme/'. $objCommon->html2text($allSchemes['scheme_alias']).'-'.$objCommon->html2text($allSchemes['scheme_id'])?>" class="read_more read-alt red-gradient pull-right" title="Read more">Read More</a>
                                 <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <?php 
					$num++;
						}
						?>
						 <div class="paginationDiv pull-right"><?php echo $pagination_output;?></div>
						 <?php
					}else{
						echo "<p>No content found</p>";
					}
					?>
                    
                    <div class="clearfix"></div>
                </div>
           </div>
            <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->