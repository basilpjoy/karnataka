<?php
$objHostels			   	   			=	new hostels();
$id									=	$objCommon->esc($_GET['id']);
$id2								   =	$objCommon->esc($_GET['id2']);
if($id !='' && $id2 !=''){
	if($id2=='pre-metric-hostels'){
		$hostCategory				  =	1;
	}else if($id2=='post-metric-hostels'){
		$hostCategory				  =	2;
	}
	$idSlug							=	$objCommon->getIdAlias($id);
	$getHostelTaluksBoys			   =	$objHostels->listQuery("SELECT h.*,t.t_name,d.d_name FROM hostels AS h LEFT JOIN districts AS d ON h.d_id = d.d_id LEFT JOIN taluk AS t ON h.t_id = t.t_id WHERE h.t_id=".$idSlug[0].' AND h.cat_id='.$hostCategory.' AND h.hostel_type=1 AND h.hostel_status=1 ORDER BY t.t_name');
	$getHostelTaluksGirls			  =	$objHostels->listQuery("SELECT h.*,t.t_name,d.d_name FROM hostels AS h LEFT JOIN districts AS d ON h.d_id = d.d_id LEFT JOIN taluk AS t ON h.t_id = t.t_id WHERE h.t_id=".$idSlug[0].' AND h.cat_id='.$hostCategory.' AND h.hostel_type=2 AND h.hostel_status=1 ORDER BY t.t_name');
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-12">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Hostels</h3>
                    </div>
                </div>
                <div>
                <div class="table-details">
                	<table class="table">
                        <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>District</th>
                                <th>Taluk</th>
                                <th>Place</th>
                                <th>Sanctioned Strength </th>
                                <th>GO Dated (Add):</th>
                                <th>Own/Rent/Ren</th>
								<th>Action</th>
                            </tr>
							</thead>
							<tbody class="border-under">
							<?php
							if(count($getHostelTaluksBoys)>0){
							?>
                            <tr>
                            	<th colspan="7">Boys</th>
                            </tr>
                            <?php 
							foreach($getHostelTaluksBoys as $keyBoys=>$allHostelTaluksBoys){ 
								if($allHostelTaluksBoys['building_type']==1){
									$hostelType				=	'Own';
								}else if($allHostelTaluksBoys['building_type']==2){
									$hostelType				=	'Rent';
								}if($allHostelTaluksBoys['building_type']==3){
									$hostelType				=	'Ren';
								}
								?>
								<tr>
									<td><?php echo ($keyBoys+1) ?></td>
									<td><?php echo $objCommon->html2text($allHostelTaluksBoys['d_name'])?></td>
									<td><?php echo $objCommon->html2text($allHostelTaluksBoys['t_name'])?></td>
									<td><?php echo $objCommon->html2text($allHostelTaluksBoys['hostel_place'])?></td>
									<td><?php echo $objCommon->html2text($allHostelTaluksBoys['hostel_strength'])?></td>
									<td><?php echo date('d-m-Y',strtotime($allHostelTaluksBoys['hostel_godate']));?></td>
									<td><?php echo $hostelType;?></td>
									<td><a href="<?php echo SITE_ROOT.'show-hostel/'.$objCommon->html2text($allHostelTaluksBoys['hostel_alias']).'-'.$objCommon->html2text($allHostelTaluksBoys['hostel_id'])?>" class="read_more">View</a></td>
								</tr>
                            <?php 
							}
							}
							if(count($getHostelTaluksGirls)>0){
							?>
                                <tr>
                            		<th colspan="7">Girls</th>
								</tr>
								<?php 
									foreach($getHostelTaluksGirls as $keyGirls=>$allHostelTaluksGirls){ 
										if($allHostelTaluksGirls['building_type']==1){
											$hostelTypeGirls				=	'Own';
										}else if($allHostelTaluksGirls['building_type']==2){
											$hostelTypeGirls				=	'Rent';
										}if($allHostelTaluksGirls['building_type']==3){
											$hostelTypeGirls				=	'Ren';
										}
								?>
								<tr>
									<td><?php echo ($keyGirls+1) ?></td>
									<td><?php echo $objCommon->html2text($allHostelTaluksGirls['d_name'])?></td>
									<td><?php echo $objCommon->html2text($allHostelTaluksGirls['t_name'])?></td>
									<td><?php echo $objCommon->html2text($allHostelTaluksGirls['hostel_place'])?></td>
									<td><?php echo $objCommon->html2text($allHostelTaluksGirls['hostel_strength'])?></td>
									<td><?php echo date('d-m-Y',strtotime($allHostelTaluksGirls['hostel_godate']));?></td>
									<td><?php echo $hostelTypeGirls?></td>
									<td><a href="<?php echo SITE_ROOT.'show-hostel/'.$objCommon->html2text($allHostelTaluksBoys['hostel_alias']).'-'.$objCommon->html2text($allHostelTaluksBoys['hostel_id'])?>" class="read_more">View</a></td>
								</tr>
                            <?php 
							}
							}
							?>
                        </tbody>
                    </table>
                </div>
                </div>
          	 </div>
        </div>
    </div>
</div>
<!---End Middle Section--->