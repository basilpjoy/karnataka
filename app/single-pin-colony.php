<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=true"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>assets/js/lib/maplace.js"></script>
<!--mini banners--->

<?php
$id											=		$objCommon->esc($_GET['id']);
$id2										=		$objCommon->esc($_GET['id2']);//gcat_id
$idSlug									=		$objCommon->getIdAlias($id);
//echo $idSlug[0]; //second parameter
//echo "<br>".$idSlug[1];//first parameter
$community_type			 		=	($idSlug[0]=='sc')?1:2;
if($idSlug[1]=='taluk'){
	$gcat_type						=		'1';
}else if($idSlug[1]=='ulb'){
	$gcat_type						=		'2';
}else if($idSlug[1]=='corporation'){
	$gcat_type						=		'3';
}
$pinImg					 				=	'';
if($community_type==1 && $gcat_type==1){ //SC in Taluk
	$pinImg								=	SITE_ROOT.'assets/images/pins/blue.png';
}else if($community_type==2 && $gcat_type==1){ //ST in Taluk
	$pinImg							=	SITE_ROOT.'assets/images/pins/red.png';
}else if($community_type==1 && $gcat_type==2){ //SC in ULB
	$pinImg							=	SITE_ROOT.'assets/images/pins/green.png';
}else if($community_type==2 && $gcat_type==2){ //ST in ULB
	$pinImg							=	SITE_ROOT.'assets/images/pins/yellow.png';
}else if($community_type==1 && $gcat_type==3){ //SC in Corporation
	$pinImg							=	SITE_ROOT.'assets/images/pins/lightblue.png';
}else if($community_type==2 && $gcat_type==3){ //ST in Corporation
	$pinImg							=	SITE_ROOT.'assets/images/pins/grey.png';
}
$objColonies						=		new colonies();
$getColonies						=		$objColonies->listQuery("SELECT * FROM colonies WHERE community_type=".$community_type." AND gcat_id=".$id2);
$colonyTablelist				=		'';
if(count($getColonies)>0){
	$getTalukName					=		$objColonies->getRowSql("SELECT gcat_name FROM geo_categories WHERE gcat_id=".$id2);
	foreach($getColonies as $keyColonies=>$allColonies){
		$RedLink			   		=	'<a href="'.SITE_ROOT.'single-pin/colony-'.$allColonies['colony_id'].'">'.$allColonies['colony_name'].'</a>';
		$mapString			 		.=	'{lat: '.$allColonies['latitude'].',lon: '.$allColonies['longitude'].',title: \''.$allColonies['colony_name'].'\',html:\''.$RedLink.'\',zoom: 0,icon:\''.$pinImg.'\',animation:google.maps.Animation.DROP},';
		$colonyTablelist		.=	'<tr><td>'.($keyColonies+1).'</td><td>'.$objCommon->html2text($allColonies['colony_name']).'</td><td>'.$objCommon->html2text($allColonies['community_name']).'</td><td>
		<a href="'.SITE_ROOT.'single-pin/colony-'.$allColonies['colony_id'].'">View</a></td></tr>';
	}
$mapString			   =	trim($mapString,",");
?>
<div class="map-s" style="width:100%; height:500px; position:relative">
	  <div id="gmap-big"></div>
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec pin-map yellowGradient">
        <div class="row">
            <div class="col-sm-12">
                <div class="about_mangalore">
                    <div class="head-message pin-head text-center ">
                        <h3><?php echo $objCommon->html2text($getTalukName['gcat_name']).' : '.strtoupper($idSlug[0])?> colonies</h3>
                        <p> Navigate above map to reach desited point </p>
                    </div>
                </div>
             </div>
        </div>
    </div>
    <div class="describe-geo">
        <div>


        <!-- Tab panes -->
            <div class="tab-content pins-area">
                <div role="tabpanel" class="tab-pane active" id="taluk">
                	<div class="table-details ">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Colony Name</th>
                                    <th>Community Name</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody class="border-under">
                                <?php
																echo $colonyTablelist;
																?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!---End Middle Section--->
<script>
$(function(){
var LocsA = [<?php echo $mapString?>];
  new Maplace({
    locations: LocsA,
	type: 'marker',
    map_div: '#gmap-big',
    controls_type: 'none',
	zoom: 0,
    shared: {
        html: '%index'
    }
}).Load();

});

</script>
<?php
}else{
	echo '<p>Oops...Something went wrong</p>';
}
 ?>
