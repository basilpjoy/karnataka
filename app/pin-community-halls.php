<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=true"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>assets/js/lib/maplace.js"></script>
<!--mini banners--->
<div class="map-s" style="width:100%; height:500px; position:relative">
	  <div id="gmap-big"></div>
</div>
<?php
$objCommHalls					=	new community_hall();
$getCommuHalls					=	$objCommHalls->listQuery("SELECT hall.*,cat.gcat_type,cat.gcat_name FROM community_hall AS hall LEFT JOIN geo_categories AS cat ON hall.gcat_id = cat.gcat_id WHERE hall.ch_status=1");
if(count($getCommuHalls)>0){
	$pinImg					 =	'';
	foreach($getCommuHalls as $keyCommuHalls=>$allCommuHalls){
		if($allCommuHalls['community_type']==1 && $allCommuHalls['gcat_type']==1){ //SC in Taluk
			$pinImg			=	SITE_ROOT.'assets/images/pins/blue.png';
		}else if($allCommuHalls['community_type']==2 && $allCommuHalls['gcat_type']==1){ //ST in Taluk
			$pinImg			=	SITE_ROOT.'assets/images/pins/red.png';
		}else if($allCommuHalls['community_type']==1 && $allCommuHalls['gcat_type']==2){ //SC in ULB
			$pinImg			=	SITE_ROOT.'assets/images/pins/green.png';
		}else if($allCommuHalls['community_type']==2 && $allCommuHalls['gcat_type']==2){ //ST in ULB
			$pinImg			=	SITE_ROOT.'assets/images/pins/yellow.png';
		}else if($allCommuHalls['community_type']==1 && $allCommuHalls['gcat_type']==3){ //SC in Corporation
			$pinImg			=	SITE_ROOT.'assets/images/pins/lightblue.png';
		}else if($allCommuHalls['community_type']==2 && $allCommuHalls['gcat_type']==3){ //ST in Corporation
			$pinImg			=	SITE_ROOT.'assets/images/pins/grey.png';
		}
		$RedLink			   =	'<a href="'.SITE_ROOT.'single-community/hall-'.$allCommuHalls['ch_id'].'">'.$allCommuHalls['ch_name'].'</a>';
		$mapString			 .=	'{lat: '.$allCommuHalls['latitude'].',lon: '.$allCommuHalls['longitude'].',title: \''.$allCommuHalls['ch_name'].'\',html:\''.$RedLink.'\',zoom: 0,icon:\''.$pinImg.'\',animation:google.maps.Animation.DROP},';
	}
}
$mapString			   =	trim($mapString,",");
?>

<!---End mini banners-->
<!---Middle Section-->
<div class="container">
    <div class="middle_sec pin-map yellowGradient">
        <div class="row">
            <div class="col-sm-12">
                <div class="about_mangalore">
                    <div class="head-message pin-head text-center ">
                        <h3>Pins of all Community Halls</h3>
                        <p> Navigate above map to reach desited point </p>
                    </div>
                </div>
             </div>
        </div>
    </div>
	
		<!--- button groups-->
        <div class="btn-group btn-group-justified optn-pins" role="group" aria-label="...">
        	<a href="<?php echo SITE_ROOT.'pin'?>" class="btn btn-default">Colonies</a>
        	<a href="<?php echo SITE_ROOT.'pin-hostel'?>" class="btn btn-default">Hostels</a>
        	<a href="javascript:;" class="btn btn-default active">Community Halls</a>
        </div>
        <!--- End Button Group -->
	
    <div class="describe-geo pin-labels text-center">
        <div class="pin-overlay"></div>
        <div class="pins-labelled">
        	<span class="map-labelled">
                <?php echo $ScTalukPin=	'<img src="'.SITE_ROOT.'assets/images/pins/blue.png" /><h4>SC Taluk</h4>';?>
            </span>
        	<span class="map-labelled">
                 <?php echo $StTalukPin		=	'<img src="'.SITE_ROOT.'assets/images/pins/red.png" /><h4>ST Taluk</h4>';?>
            </span>
        	<span class="map-labelled">
                 <?php echo $ScULBPin		=	'<img src="'.SITE_ROOT.'assets/images/pins/green.png" /><h4>SC ULB</h4>';?>
            </span>
        	<span class="map-labelled">
                 <?php echo $StULBPin		=	'<img src="'.SITE_ROOT.'assets/images/pins/yellow.png" /><h4>ST ULB</h4>';?>
            </span>
        	<span class="map-labelled">
                 <?php echo $ScCorPin		=	'<img src="'.SITE_ROOT.'assets/images/pins/lightblue.png" /><h4>SC Corporation</h4>';?>
            </span>
        	<span class="map-labelled">
                 <?php echo $StCorPin		=	'<img src="'.SITE_ROOT.'assets/images/pins/grey.png" /><h4>ST Corporation</h4>';?>
            </span>
        </div>
    </div>
    <div class="describe-geo">
        <div>
        	 <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#taluk" aria-controls="home" role="tab" data-toggle="tab">Taluk</a></li>
                <li role="presentation"><a href="#ULB" aria-controls="profile" role="tab" data-toggle="tab">ULB</a></li>
                <li role="presentation"><a href="#city" aria-controls="messages" role="tab" data-toggle="tab">City Corporation</a></li>
            </ul>
        
        <!-- Tab panes -->
            <div class="tab-content pins-area">
                <div role="tabpanel" class="tab-pane active" id="taluk">
                	<div class="table-details ">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Taluk</th>
                                    <th>Grama Panchayath</th>
                                    <th>SC Colony</th>
                                    <th>ST Colony</th>
                                </tr>
                            </thead>
                            <tbody class="border-under">
                                <?php 
								$getTalukCount				=	$objCommHalls->listQuery("SELECT tab.*,SUM(case when community_type=1 then 1 else 0 end) AS scCount,SUM(case when community_type=2 then 1 else 0 end) AS stCount FROM (SELECT cat.gcat_id,cat.gcat_type,cat.gcat_name,hall.community_type,gsb.gscat_name FROM geo_categories AS cat LEFT JOIN community_hall AS hall ON cat.gcat_id = hall.gcat_id LEFT JOIN geo_sub_categories AS gsb ON hall.gscat_id=gsb.gscat_id WHERE cat.gcat_type=1 AND community_type IS NOT NULL ) AS tab WHERE tab.community_type IS NOT NULL ORDER BY tab.gcat_name");
								if(count($getTalukCount)>0 && $getTalukCount[0]['gcat_id']>0){
								foreach($getTalukCount as $keyTalukCount=>$allTalukCount){
								?>
                                <tr>
                                    <td><?php echo ($keyTalukCount+1)?></td>
                                    <td><?php echo $objCommon->html2text($allTalukCount['gcat_name'])?></td>
                                    <td><?php echo $objCommon->html2text($allTalukCount['gscat_name'])?></td>
                                    <td><?php echo $objCommon->html2text($allTalukCount['scCount'])?></td>
                                    <td><?php echo $objCommon->html2text($allTalukCount['stCount'])?></td>
                                </tr>
                                <?php 
								}
								}else{
									echo '<tr><td colspan="4"><p>No content found...</p></td></tr>';
								}
								?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="ULB">
                	<div class="table-details ">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>ULB</th>
                                    <th>SC Colony</th>
                                    <th>ST Colony</th>
                                </tr>
                            </thead>
                            <tbody class="border-under">
							<?php
                                $getUlbCount				=	$objCommHalls->listQuery("SELECT tab.*,SUM(case when community_type=1 then 1 else 0 end) AS scCount,SUM(case when community_type=2 then 1 else 0 end) AS stCount FROM (SELECT cat.gcat_id,cat.gcat_type,cat.gcat_name,hall.community_type FROM geo_categories AS cat LEFT JOIN community_hall AS hall ON cat.gcat_id = hall.gcat_id WHERE cat.gcat_type=2 ) AS tab WHERE tab.community_type IS NOT NULL ORDER BY tab.gcat_name");
								if(count($getUlbCount)>0 && $getUlbCount[0]['gcat_id']>0){
								foreach($getUlbCount as $keyUlbCount=>$allUlbCount){
								?>
                                 <tr>
                                    <td><?php echo ($keyUlbCount+1)?></td>
                                    <td><?php echo $objCommon->html2text($allUlbCount['gcat_name'])?></td>
                                    <td><?php echo $objCommon->html2text($allUlbCount['scCount'])?></td>
                                    <td><?php echo $objCommon->html2text($allUlbCount['stCount'])?></td>
                                </tr>
                                <?php 
								}
								}else{
									echo '<tr><td colspan="4"><p>No content found...</p></td></tr>';
								}
								?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="city">
                	<div class="table-details ">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>City Corporation</th>
                                    <th>SC Colony</th>
                                    <th>ST Colony</th>
                                </tr>
                            </thead>
                            <tbody class="border-under">
                               <?php
                                $getCorCount				=	$objCommHalls->listQuery("SELECT tab.*,SUM(case when community_type=1 then 1 else 0 end) AS scCount,SUM(case when community_type=2 then 1 else 0 end) AS stCount FROM (SELECT cat.gcat_id,cat.gcat_type,cat.gcat_name,hall.community_type FROM geo_categories AS cat LEFT JOIN community_hall AS hall ON cat.gcat_id = hall.gcat_id WHERE cat.gcat_type=3 ) AS tab WHERE tab.community_type IS NOT NULL ORDER BY tab.gcat_name");
								if(count($getCorCount)>0 && $getCorCount[0]['gcat_id']>0){
								foreach($getCorCount as $keyCorCount=>$allCorCount){
								?>
                                 <tr>
                                    <td><?php echo ($keyCorCount+1)?></td>
                                    <td><?php echo $objCommon->html2text($allCorCount['gcat_name'])?></td>
                                    <td><?php echo $objCommon->html2text($allCorCount['scCount'])?></td>
                                    <td><?php echo $objCommon->html2text($allCorCount['stCount'])?></td>
                                </tr>
                                <?php 
								}
								}else{
									echo '<tr><td colspan="4"><p>No content found...</p></td></tr>';
								}
								?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
</div>

<!---End Middle Section-->
<script>
$(function(){

var LocsA = [<?php echo $mapString?>];
  new Maplace({
    locations: LocsA,
	type: 'marker',
    map_div: '#gmap-big',
    controls_type: 'none',
	zoom: 0,
    shared: {
        html: '%index'
    }
}).Load();
  
});

</script>