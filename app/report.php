<?php
$objReports			   		 =	new reports();
$getReportCat				   =	$objReports->listQuery("SELECT rcat_id,rcat_name FROM report_categories WHERE rcat_status=1 ORDER BY rcat_name");
include_once(DIR_ROOT."config/class/pagination-class-front.php");
$pageIdUri		=	htmlspecialchars($_SERVER['REQUEST_URI']);
$pageIdUriExl	 =	explode('?page_id=',$pageIdUri);
$pageId		   =	$pageIdUriExl[1];
$pageIdUriSearch  =	explode('?searchCat=',$pageIdUriExl[0]);
if(count($pageIdUriSearch)>0){
	$searchKeyword			  =	trim($pageIdUriSearch[1],"/");
	$searchCatExpl			  =	explode('searchYear=',$searchKeyword);
	$searchCat 				  =	trim($searchCatExpl[0],'&amp;');
	$searchYearExpl			 =	explode('searchMonth=',$searchCatExpl[1]);
	$searchYear 				 =	trim($searchYearExpl[0],'&amp;');
	$searchMonth 				 =	$searchYearExpl[1];
	if($searchKeyword){
		if($searchCat){
			$searchSql		  .=	" AND reports.rcat_id= ".$searchCat;
		}
		if($searchYear){
			$searchSql		  .=	" AND reports.report_date LIKE '%".$searchYear."-%' ";
		}
		if($searchMonth){
			$searchSql		  .=	" AND reports.report_date LIKE '%-".$searchMonth."-%' ";
		}
	}
}

$sqlreports               		 =	" SELECT reports.*,cat.rcat_name FROM reports LEFT JOIN report_categories AS cat ON reports.rcat_id = cat.rcat_id WHERE reports.report_status=1 ".$searchSql." ORDER BY reports.report_id DESC";
$num_results_per_page           =	10;
$num_page_links_per_page        =	5;
$pg_param 					   =	"";
$pagesection				    =	'';
pagination($sqlreports,$num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$reportList			   		 =	$objReports->listQuery($pg_result);
$months 						 =	array('01' => 'Jan', '02' => 'Feb', '03' => 'Mar', '04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="local-search report-search-bar">
            <div class="search-bar  pull-right form-group">
            	<form action="report" method="GET" id="searchCircular">
                	<select class="form-control" id="selectCat">
                    	<option value="">Select Category</option>
						<?php
						foreach($getReportCat as $allReportCat){
						?>
                    	<option value="<?php echo $objCommon->html2text($allReportCat['rcat_id'])?>" <?php echo ($searchCat==$allReportCat['rcat_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allReportCat['rcat_name'])?></option>
						<?php
						}
						?>
                    </select>
                    <select class="form-control" id="selectYear">
                    	<option value="">Select Year</option>
                    	<?php
						for($year=1980;$year<2020;$year++){
						?>
						<option value="<?php echo $year?>" <?php echo ($searchYear==$year)?'selected="selected"':''?>><?php echo $year?></option>
						<?php
						}
						?>
                    </select>
                    <select class="form-control" id="selectMonth">
                    	<option value="">Select Month</option>
                    	<?php
						foreach($months as $keymonths=>$allMonths){
						?>
						<option value="<?php echo $keymonths?>" <?php echo ($searchMonth==$keymonths)?'selected="selected"':''?>><?php echo $allMonths?></option>
						<?php
						}
						?>
                    </select>
                    <input type="image" class="search-btn" src="<?php echo SITE_ROOT ?>assets/images/search.png" alt="Search" />
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Reports</h3>
                    </div>
                </div>
                <div>
                <div class="table-details table-responsive">
                	<table class="table">
                        <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Report Name</th>
                                <th>Category</th>
                                <th>View</th>
                                <th>Download</th>
                            </tr>
                        </thead>
                        <tbody class="border-under">
                            <?php 
								if(count($reportList)>0){
									$num	  =	1;
									$mult	 =	(int)($pageId)?($pageId-1):0;
									$num	  =	($num_results_per_page*$mult)+1;
									foreach($reportList as $allReport){
							?>
                            <tr>
                                <td><?php echo $num ?></td>
                                <td><?php echo $objCommon->html2text($allReport['report_name']);?></td>
                                <td><?php echo $objCommon->html2text($allReport['rcat_name']);?></td>
                                <td><a href="<?php echo SITE_ROOT.'assets/uploads/reports/'.$objCommon->html2text($allReport['report_file']);?>" target="_blank" class="read_more read-alt red-gradient">View</a></td>
                                <td><a href="<?php echo SITE_ROOT.'library/download_file/download_email_attach.php?attach_file='.$objCommon->html2text($allReport['report_file']).'&file_name=reports'?>" class="read_more read-alt red-gradient">Download</a></td>
                            </tr>
                            <?php
							$num++;
							 }
							 ?>
							 <tr><td colspan="5"><div class="paginationDiv pull-right"><?php echo $pagination_output;?></div></td></tr>
							 <?php
						}else{
							echo '<tr><td colspan="5">No Content found</td></tr>';
						}
						?>
                        </tbody>
                    </table>
                </div>
                
                    <div class="clearfix"></div>
                </div>
          	 </div>
           
        </div>
    </div>
</div>
<!---End Middle Section--->