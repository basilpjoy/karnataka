<!--mini banners-->
<div class="mini-banners" style="background-image:url('assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners-->
<!---Middle Section-->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>SPECIAL COMPONENT PLAN</h3>
                    </div>
                    	<div class="notification-lists">
                        	<ul class="list-unstyled">
                            	<li class="notication-sort">
                                	<div class="date_published text-center">
                                    	<div class="day">
                                        	<h4>08</h4>
                                        </div>
                                        <div class="month">
                                        	<span>DEC</span>
                                        </div>
                                    </div>
                                    <div class="notify-details">
                                    	<div class="notication-detials">
                                             <h4 class="media-heading">DIY writers pick out the biggest and best new songs</h4>
                                         </div>
                                         <div class="footer-notofication">
                                             <span class="date-full pull-left">08/12/1993</span>
                                             <a href="<?php echo SITE_ROOT ?>index.php?page=single-notification" class="read_more read-alt red-gradient pull-right" title="View Pdf">View PDF</a>
                                             <a href="<?php echo SITE_ROOT ?>index.php?page=single-notification" class="read_more read-alt red-gradient pull-right" title="Download Pdf">Download</a>
                                             <div class="clearfix"></div>
                                         </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="notification-in-detail">
                            	<h5>Lorem Ipsum is simply dummy text of</h5>
                            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                        </div>
                    <div class="clearfix"></div>
                </div>
           </div>
            <div class="col-sm-4">
            	<?php if($page != 'notifications'){ ?>
                <div class="sidebar_right">
                    <div class="head-sidebar yellowGradient">
                        <h4>Notifications
                        	<span class="pull-right nav-news"><i class="fa fa-chevron-left" id="news-left"></i><i class="fa fa-chevron-right" id="news-right"></i></span>
                            <div class="clearfix"></div>
                         </h4>
                    </div>
                    <div class="body-sidebar">
                        <ul class="list-unstyled" id="nt-example1">
                            <?php for($i=0; $i<4; $i++){ ?>
                                <li>
                                    <div class="date_published text-center">
                                        <div class="day">
                                            <h4>08</h4>
                                        </div>
                                        <div class="month">
                                            <span>DEC</span>
                                        </div>
                                    </div>
                                    <div class="notification-list">
                                        <h5><a href="#" title="News Heading">DIY writers pick out the biggest and best new songs..</a></h5>
                                        <p><a href="#" title="News Deatils">Lorem Ipsum is simply dummy text of the printing and typesetting industry</a></p>
                                        <a href="#" class="read_more" title="Read More">Read More <i class="fa fa-caret-right"></i></a>
                                    </div>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
                <?php }?>
                <div class="sidebar_right">
                	<div class="pin_locale text-center">
                    	<a href="#" title="pins of all SC n ST">Pins of all SC n ST colonies</a>
                    </div>
                </div>
                <div class="sidebar_right">
                    <div class="head-sidebar yellowGradient">
                        <h4>Scheduled Castes/Scheduled Tribes Caste list</h4>
                    </div>
                    <div class="body-sidebar">
                        <ul class="list-unstyled cast-list">
                            <?php for($i=0; $i<3; $i++){ ?>
                                <li>
                                    <div class="notification-list full-width">
                                        <h5><a href="#" title="News Heading">DIY writers pick out the biggest and best new songs best new songs.. <span class="newNotify">New</span></a></h5>
                                    </div>
                                </li>
                            <?php }?>
                            <li>
                                <div class="notification-list full-width">
                                    <h5><a href="#" title="News Heading">DIY writers pick out the biggest and best new songs best new songs.. </a></h5>
                                </div>
                            </li>
                        </ul>
                        <a href="#" class="read_more pull-right" title="Read More">Read More <i class="fa fa-caret-right"></i></a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---End Middle Section-->