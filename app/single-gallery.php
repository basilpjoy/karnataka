<?php
$objGallery							=	new gallery_images();
$id									=	$objCommon->esc($_GET['id']);
if($id){
	$idSlug							=	$objCommon->getIdAlias($id);
	$getAlbumName					  =	$objGallery->getRowSql("SELECT * FROM gallery_category WHERE gc_id=".$idSlug[0]." AND gc_alias='".$idSlug[1]."'");
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Image Gallery</h3>
                    </div>
					<?php
					if($getAlbumName['gc_id']){
					$getImages						 =	$objGallery->getAll("gc_id=".$getAlbumName['gc_id'],"gi_createdon desc");
					?>
                    <div class="album-titles">
                        <h4><?php echo $objCommon->html2text($getAlbumName['gc_name'])?></h4>
                    </div>
					<?php
					}
					?>
                    <div class="galleria inside-galeria text-center">
                    	<div class="row">
                        	<?php
							if(count($getImages)>0){ 
								foreach($getImages as $allImages){ 
								?>
								<div class="col-sm-3">
									<div class="album-sec" style="background-image:url('<?php echo SITE_ROOT.'assets/uploads/gallery/'.$objCommon->html2text($allImages['gi_url'])?>');">
									<a href="<?php echo SITE_ROOT.'assets/uploads/gallery/'.$objCommon->html2text($allImages['gi_url'])?>" class="overlay_album html5lightbox" title="<?php echo $objCommon->html2text($allImages['gi_title'])?>" data-group="mygroup"><?php echo $objCommon->html2text($allImages['gi_title'])?></a>
									</div>
								</div>
                            	<?php 
								} 
							}else{
								echo '<div class="col-sm-12">No photos found</div>';
							}
							?>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
           </div>
            <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->