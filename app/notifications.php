<?php
$objNotifications			   =	new notifications();
include_once(DIR_ROOT."config/class/pagination-class-front.php");
$pageIdUri		=	htmlspecialchars($_SERVER['REQUEST_URI']);
$pageIdUriExl	 =	explode('?page_id=',$pageIdUri);
$pageId		   =	$pageIdUriExl[1];
$pageIdUriSearch  =	explode('?search=',$pageIdUriExl[0]);
if(count($pageIdUriSearch)>0){
	$searchKeyword			 =	trim($pageIdUriSearch[1],"/");
	if($searchKeyword){
		$searchSql			  =	" AND (notification_title LIKE '%".$searchKeyword."%' OR notification_alias LIKE '%".$searchKeyword."%') ";
	}
}
$sqlNotification                =	" SELECT note_id,notification_title,notification_alias,notification_content,notification_date,notification_status FROM notifications WHERE status=1 ".$searchSql." ORDER BY notification_status ASC";
$num_results_per_page           =	10;
$num_page_links_per_page        =	5;
$pg_param 					   =	"";
$pagesection				    =	'';
pagination($sqlNotification,$num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$notificationList			   =	$objNotifications->listQuery($pg_result);
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="local-search">
            <div class="search-bar pull-right form-group">
            	<form action="notifications" method="GET" id="searchCircular">
                    <input type="text" placeholder="Search Notifications" class="form-control" id="searchval" value="<?php echo $searchKeyword?>" />
                    <input type="image" class="search-btn" src="<?php echo SITE_ROOT ?>assets/images/search.png" alt="Search" />
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Notifications</h3>
                    </div>
                    	<div class="notification-lists">
                        	<ul class="list-unstyled">
                            	<?php 
								if(count($notificationList)>0){
									foreach($notificationList as $allNotification){
									?>
									<li class="notication-sort">
										<div class="date_published text-center">
											<div class="day">
												<h4><?php echo date("d",strtotime($allNotification['notification_date']));?></h4>
											</div>
											<div class="month">
												<span><?php echo date("M",strtotime($allNotification['notification_date']));?></span>
											</div>
										</div>
										<div class="notify-details">
											<div class="notication-detials">
												 <h4 class="media-heading"><?php echo $objCommon->html2text($allNotification['notification_title']);?></h4>
												 <p><?php echo $objCommon->limitWords(strip_tags($allNotification['notification_content']),150);?></p>
											 </div>
											 <div class="footer-notofication">
												 <span class="date-full pull-left"><?php echo date("d/m/Y",strtotime($allNotification['notification_date']));?></span>
												 <a href="<?php echo SITE_ROOT.'single-notification/'.$objCommon->html2text($allNotification['notification_alias']).'-'.$objCommon->html2text($allNotification['note_id'])?>" class="read_more read-alt red-gradient pull-right" title="Read more">Read More</a>
												 <div class="clearfix"></div>
											 </div>
										</div>
									</li>
									<?php 
									}
									?>
									 <div class="paginationDiv pull-right"><?php echo $pagination_output;?></div>
									 <?php
								}else{
									echo "<p>No content found</p>";
								}
								?>
                            </ul>
                        </div>
                    <div class="clearfix"></div>
                </div>
           </div>
             <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->