<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="#" type="image/png">
    <title>Social welfare DK</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="<?php echo SITE_ROOT?>assets/js/packages/<?php echo $page ?>.min.js"  type="text/javascript" ></script>
	<link href="<?php echo SITE_ROOT?>assets/css/style.css" type="text/css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lte IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!---Header-->
<header id="header-sec">
    <div class="container">
        <div class="logo text-center">
            <a href="<?php echo SITE_ROOT; ?>" title="logo">
                <img class="center-block" src="<?php echo SITE_ROOT ?>assets/images/logo.png" width="100" />
                <span class="logo-name">Commissionarate of Social Welfare - Dakshina Kannada, Mangalore</span>
            </a>
        </div>
    </div>
    <nav class="yellowGradient">
    	<div class="mob-menu text-right hidden-md hidden-lg">
            <i class="fa fa-bars fa-lg"></i>
        </div>
        <div class="container">
            <ul class="list-unstyled list-inline text-center nav-ul">
                <li><a href="<?php echo SITE_ROOT ?>" <?php echo ($page == 'home')?' class="active"': ''; ?> title="home">Home</a></li>
                <li>
                    <a href="<?php echo SITE_ROOT?>pages/about-us" <?php echo ($page == 'pages' || $page == 'report')?' class="active"': ''; ?>  title="About us">About Us</a>
                    <ul class="list-unstyled text-left drop_menu">
                    	 <li><a href="<?php echo SITE_ROOT?>pages/organisational-structure"  title="Organisational Structure">Organisational Structure</a></li>
                         <li><a href="<?php echo SITE_ROOT.'report'?>" title="Reports">Reports</a></li>
                    </ul>
                </li>
               
                <li><a href="<?php echo SITE_ROOT.'schemes'?>"  <?php echo ($page == 'schemes')?' class="active"': ''; ?>  title="Schemes">Schemes</a></li>
                <li><a href="<?php echo SITE_ROOT.'hostels'?>"  <?php echo ($page == 'hostels')?' class="active"': ''; ?>   title="Hostels">Hostels</a></li>
                <li><a href="<?php echo SITE_ROOT.'circular'?>"  <?php echo ($page == 'circular')?' class="active"': ''; ?>   title="Government Circulars">Government Circulars</a></li>
				<li><a href="<?php echo SITE_ROOT.'notifications'?>" <?php echo ($page == 'notifications')?' class="active"': ''; ?>   title="Notifications">Notifications</a></li>
                <li><a href="<?php echo SITE_ROOT.'rti'?>" <?php echo ($page == 'rti')?' class="active"': ''; ?>   title="RTI">RTI</a></li>
                <li><a href="<?php echo SITE_ROOT.'component-plan'?>" <?php echo ($page == 'component-plan')?' class="active"': ''; ?>   title="Special Component Plan">Special Component Plan</a></li>
                <li><a href="<?php echo SITE_ROOT.'contactus'?>" <?php echo ($page == 'contactus')?' class="active"': ''; ?> title="Contact US">Contact US</a></li>
               <!-- <li><a href="<?php echo SITE_ROOT.'gallery'?>" title="Image Gallery">Image Gallery</a></li>-->
            </ul>
        </div>
    </nav>
</header>
<!---End Header-->
<!--Goodle Translater-->
<div id="google_translate_element"></div>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'kn', includedLanguages: 'en,kn', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
