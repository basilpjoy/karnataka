<!---Footer-->
        <div class="footer-sec">
            <div class="top-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="heading-footer">
                                <h4>Social welfare DK</h4>
                            </div>
                            <div class="nav-links">
                                <div class="row">
                                    <ul class="col-sm-6 list-unstyled">
                                        <li><a href="<?php echo SITE_ROOT?>" class="active" title="home">Home</a></li>
                                        <li><a href="<?php echo SITE_ROOT?>pages/about-us" title="About us">About Us</a></li>
                                        <li><a href="<?php echo SITE_ROOT?>pages/organisational-structure" title="Organisational Structure">Organisational Structure</a></li>
                                        <li><a href="<?php echo SITE_ROOT.'schemes'?>" title="Schemes">Schemes</a></li>
                                        <li><a href="<?php echo SITE_ROOT.'hostels'?>" title="Hostels">Hostels</a></li>
                                        <li><a href="<?php echo SITE_ROOT.'circular'?>" title="Government Circulars">Government Circulars</a></li>
                                    </ul>
                                    <ul class="col-sm-6 list-unstyled">
                                         <li><a href="<?php echo SITE_ROOT.'notifications'?>" title="Notifications">Notifications</a></li>
                                        <li><a href="<?php echo SITE_ROOT.'rti'?>" title="RTI">RTI</a></li>
                                        <li><a href="<?php echo SITE_ROOT.'component-plan'?>" title="Special Component Plan">Special Component Plan</a></li>
                                        <li><a href="<?php echo SITE_ROOT.'gallery'?>" title="Image Gallery">Image Gallery</a></li>
                                        <li><a href="<?php echo SITE_ROOT.'report'?>" title="Reports">Reports</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="heading-footer">
                                <h4>Other Pages</h4>
                            </div>
                            <div class="nav-links">
                                <ul class="list-unstyled">
                                    <?php 
                                    $footerPageList =   $objPages->getAll("footer_page=1");
                                    if(count($footerPageList)>0){
                                    foreach ($footerPageList as $footerPage) {
                                    ?>
                                    <li><a href="<?php echo SITE_ROOT.'pages/'.$footerPage['page_alias'];?>" title="home"><?php echo $footerPage['page_title']; ?></a></li>   
                                    <?php }} ?>                                 
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="search_bar">
                               <script>
							  (function() {
								var cx = '008394787133320892955:9watykjkrve';
								var gcse = document.createElement('script');
								gcse.type = 'text/javascript';
								gcse.async = true;
								gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
									'//cse.google.com/cse.js?cx=' + cx;
								var s = document.getElementsByTagName('script')[0];
								s.parentNode.insertBefore(gcse, s);
							  })();
							</script>
							<gcse:search></gcse:search>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-footer text-center">
                <p> &copy; Copyright @ 2016</p>
            </div>
        </div>
    </body>
</html>
<!---End Footer-->