<?php
$objComponent				 	   =	new component_plans();
$id								 =	$objCommon->esc($_GET['id']);
if($id){
	$idSlug			  			 =	$objCommon->getIdAlias($id);
	$getComponent				   =	$objComponent->getRow("plan_id=".$idSlug[0]." AND plan_alias='".$idSlug[1]."' AND plan_status=1");
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Component Plan</h3>
                    </div>
                </div>
                <div class="dynamic-cotents text-justify">
                    <h2><?php echo $objCommon->html2text($getComponent['plan_title']);?></h2>
                    <?php echo $objCommon->displayEditor($getComponent['plan_content']);?>
                    <div class="clearfix"></div>
                </div>
                
           </div>
             <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->