<?php
$objSchemes			   	 =	new schemes();
$id					  	 =	$objCommon->esc($_GET['id']);
if($id){
	$idSlug			  	 =	$objCommon->getIdAlias($id);
	$getScheme  			  =	$objSchemes->getRowSql("SELECT * FROM schemes WHERE scheme_status=1 AND scheme_id=".$idSlug[0]." AND scheme_alias='".$idSlug[1]."'");
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Schemes</h3>
                    </div>
                </div>
				<?php
				if($getScheme['scheme_id']){
				?>
                <div class="dynamic-cotents text-justify">
                    <h2><?php echo $objCommon->html2text($getScheme['scheme_title']);?></h2>
                   <?php echo $objCommon->displayEditor($getScheme['scheme_content']);?>
                    <div class="clearfix"></div>
                </div>
				<?php
				$objSchemesFiles			   	 =	new scheme_files();
				$getScehemesFiles				=	$objSchemesFiles->getAll("scheme_id=".$idSlug[0],"sp_id DESC");
				if(count($getScehemesFiles)>0){
				?>
					<div class="table-responsive">
					  <table class="table">
						<tr>
							<th>Sl.No</th>
							<th>Schemes</th>
							<th>View Pdf</th>
							<th>Downlaod</th>
						</tr>
						<?php foreach($getScehemesFiles as $keyScehemes=>$allScehemesFiles){ ?>
						<tr>
							<td><?php echo ($keyScehemes+1)?></td>
							<td><?php echo $objCommon->html2text($allScehemesFiles['sp_title']);?></td>
							<td> <a href="<?php echo SITE_ROOT.'assets/uploads/pdf/'.$objCommon->html2text($allScehemesFiles['sp_file']);?>" target="_blank"  class="read_more read-alt red-gradient" title="Read more">View Pdf</a></td>
							<td> <a href="<?php echo SITE_ROOT.'library/download_file/download_email_attach.php?attach_file='.$objCommon->html2text($allScehemesFiles['sp_file']).'&file_name=pdf'?>" class="read_more read-alt red-gradient" title="Read more">Download</a></td>
						</tr>
						<?php }?>
					  </table>
					</div>
				<?php
					}
				}else{
					echo '<p>No schemes found</p>';
				}
				?>
           </div>
            <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->