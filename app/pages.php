<?php
$id					  	 		 =	$objCommon->esc($_GET['id']);
if($id){
	$getPageDetails                 =	$objPages->getRowSql("SELECT page_id,page_title,page_content FROM pages WHERE page_alias='".$id."'");
}
?>
<!--mini banners-->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners-->
<!---Middle Section-->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
			<?php
			if($getPageDetails['page_id']){
			?>
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3><?php echo $objCommon->html2text($getPageDetails['page_title']);?></h3>
                    </div>
                </div>
                <div class="dynamic-cotents text-justify">
                   <?php echo $objCommon->displayEditor($getPageDetails['page_content']);?>
                    <div class="clearfix"></div>
                </div>
			<?php
			}
			?>
           </div>
            <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section-->