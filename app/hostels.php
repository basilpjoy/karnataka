<?php
$objHostels			   	   =	new hostels();
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-12">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Hostels</h3>
                    </div>
                </div>
                <div>
                <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#pre" aria-controls="home" role="tab" data-toggle="tab">Pre Metric Hostels</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Post Metric Hostels</a></li>
                        <li role="presentation"><a href="#residential" aria-controls="settings" role="tab" data-toggle="tab">Residential Schools</a></li>
                    </ul>
                
                <!-- Tab panes -->
                    <div class="tab-content">
						<?php
						$getPremetricHost			=	$objHostels->listQuery("SELECT tab1.t_name,tab1.t_id,tab1.t_alias,SUM(tab1.boysHostels) AS totalBhostels,SUM(tab1.girlsHostels) AS totalGhostels,SUM(tab1.boysCount) AS boysCount,SUM(tab1.girlsCount) AS totalGirls FROM
(
SELECT tab.t_name,tab.t_id,tab.t_alias,CASE  WHEN tab.hostel_type=1 THEN tab.hostelCount  ELSE  0  END AS boysHostels ,CASE  WHEN tab.hostel_type=2 THEN tab.hostelCount  ELSE  0  END AS girlsHostels,CASE  WHEN tab.hostel_type=1 THEN tab.studentCount  ELSE  0  END AS boysCount ,CASE  WHEN tab.hostel_type=2 THEN tab.studentCount  ELSE  0  END AS girlsCount 
FROM (
SELECT h.*,t.t_name,t.t_alias,COUNT(h.hostel_type) AS hostelCount,SUM(h.hostel_strength) AS studentCount FROM hostels AS h 
LEFT JOIN taluk AS t ON h.t_id = t.t_id
WHERE cat_id=1 AND hostel_status=1  GROUP BY h.hostel_type,h.t_id ORDER by t.t_name
) AS tab 
)AS tab1 GROUP BY t_id");
						?>
                        <div role="tabpanel" class="tab-pane active" id="pre">
						<?php 
						if(count($getPremetricHost)>0){
						?>
                        <div class="table-responsive">
                            <table class="table hostels-table">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Taluk</th>
                                    <th>
                                        <table width="100%" class="inside-table">
                                            <tr class="main-head-table">
                                                <th colspan="3">Pre Metric Hostels</th>
                                            </tr>
                                        <tr>
                                            <th width="30.33%">Boys</th>
                                            <th width="30.33%">Girls</th>
                                            <th width="30.33%">Total</th>
                                        </tr>
                                        </table>
                                    </th>
                                    <th>
                                        <table width="100%" class="inside-table">
                                            <tr class="main-head-table">
                                                <th colspan="3">Pre Metric Hostels Students</th>
                                            </tr>
                                        <tr>
                                            <th width="30.33%">Boys</th>
                                            <th width="30.33%">Girls</th>
                                            <th width="30.33%">Total</th>
                                        </tr>
                                        </table>
                                    </th>
                                    </tr>
                                    </thead>
                                    <tbody class="border-under">
									<?php 
									$totalHostels=$totalStudentCount						=	0;
									foreach($getPremetricHost as $keyPreHost=>$allPremetricHost){
										$totalBoysHostels				=	($allPremetricHost['totalBhostels'] >0)?$allPremetricHost['totalBhostels']:0;
										$totalGirlsHostels			   =	($allPremetricHost['totalGhostels'] >0)?$allPremetricHost['totalGhostels']:0;
										$totalHostels					=	$totalBoysHostels+$totalGirlsHostels;
										$totalBoysCount				  =	($allPremetricHost['boysCount'] >0)?$allPremetricHost['boysCount']:0;
										$totalGirlsCount				 =	($allPremetricHost['totalGirls'] >0)?$allPremetricHost['totalGirls']:0;
										$totalStudentCount			   =	$totalBoysCount+$totalGirlsCount;	
									?>
                                    <tr>
                                    <td><?php echo($keyPreHost+1);?></td>
                                    <td><?php echo $objCommon->html2text($allPremetricHost['t_name'])?></td>
                                    <td> 
                                    	<table width="100%" class="nested-table">
                                            <tr>
                                            	<td width="30.33%">
                                                   <?php echo $totalBoysHostels;?> 
                                                </td>
                                                <td width="30.33%">
                                                    <?php echo $totalGirlsHostels;?> 
                                                </td>
                                                <td width="30.33%">
                                                    <a href="<?php echo SITE_ROOT.'single-hostels/'.$objCommon->html2text($allPremetricHost['t_alias']).'-'.$objCommon->html2text($allPremetricHost['t_id']).'/pre-metric-hostels'?>" class="" title="Read more"><?php echo $totalHostels; ?></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td> 
                                    	<table width="100%" class="nested-table">
                                            <tr>
                                            	<td width="30.33%">
                                                    <?php echo $totalBoysCount ?>
                                                </td>
                                                <td width="30.33%">
                                                    <?php echo $totalGirlsCount ?>
                                                </td>
                                                <td width="30.33%">
                                                    <?php echo $totalStudentCount ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
						<?php
						}
						?>
                        </div>
                        <?php
						$getPostmetricHost			=	$objHostels->listQuery("SELECT tab1.t_name,tab1.t_id,tab1.t_alias,SUM(tab1.boysHostels) AS totalBhostels,SUM(tab1.girlsHostels) AS totalGhostels,SUM(tab1.boysCount) AS boysCount,SUM(tab1.girlsCount) AS totalGirls FROM
(
SELECT tab.t_name,tab.t_id,tab.t_alias,CASE  WHEN tab.hostel_type=1 THEN tab.hostelCount  ELSE  0  END AS boysHostels ,CASE  WHEN tab.hostel_type=2 THEN tab.hostelCount  ELSE  0  END AS girlsHostels,CASE  WHEN tab.hostel_type=1 THEN tab.studentCount  ELSE  0  END AS boysCount ,CASE  WHEN tab.hostel_type=2 THEN tab.studentCount  ELSE  0  END AS girlsCount 
FROM (
SELECT h.*,t.t_name,t.t_alias,COUNT(h.hostel_type) AS hostelCount,SUM(h.hostel_strength) AS studentCount FROM hostels AS h 
LEFT JOIN taluk AS t ON h.t_id = t.t_id
WHERE cat_id=2 AND hostel_status=1  GROUP BY h.hostel_type,h.t_id ORDER by t.t_name
) AS tab 
)AS tab1 GROUP BY t_id");
						?>
                        <div role="tabpanel" class="tab-pane" id="messages">
						<?php
						if(count($getPostmetricHost) >0){
						?>
                        <table class="table hostels-table">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Taluk</th>
                                    <th>
                                        <table width="100%" class="inside-table">
                                            <tr class="main-head-table">
                                                <th colspan="3">Post Metric Hostels</th>
                                            </tr>
                                        <tr>
                                            <th width="30.33%">Boys</th>
                                            <th width="30.33%">Girls</th>
                                            <th width="30.33%">Total</th>
                                        </tr>
                                        </table>
                                    </th>
                                    <th>
                                        <table width="100%" class="inside-table">
                                            <tr class="main-head-table">
                                                <th colspan="3">Post Metric Hostels Students</th>
                                            </tr>
                                        <tr>
                                            <th width="30.33%">Boys</th>
                                            <th width="30.33%">Girls</th>
                                            <th width="30.33%">Total</th>
                                        </tr>
                                        </table>
                                    </th>
                                    </tr>
                                    </thead>
                                     <tbody class="border-under">
									<?php 
									$totalHostelsPostMetric	=	$totalStudentCountPostMetric	=	0;
									foreach($getPostmetricHost as $keyPostHost=>$allPostmetricHost){
										$totalBoysHostelsPostMetric				=	($allPostmetricHost['totalBhostels'] >0)?$allPostmetricHost['totalBhostels']:0;
										$totalGirlsHostelsPostMetric			   =	($allPostmetricHost['totalGhostels'] >0)?$allPostmetricHost['totalGhostels']:0;
										$totalHostelsPostMetric					=	$totalBoysHostelsPostMetric+$totalGirlsHostelsPostMetric;
										$totalBoysCountPostMetric				  =	($allPostmetricHost['boysCount'] >0)?$allPostmetricHost['boysCount']:0;
										$totalGirlsCountPostMetric				 =	($allPostmetricHost['totalGirls'] >0)?$allPostmetricHost['totalGirls']:0;
										$totalStudentCountPostMetric			   =	$totalBoysCountPostMetric+$totalGirlsCountPostMetric;	
									?>
                                    <tr>
                                    <td><?php echo ($keyPostHost+1)?></td>
                                    <td><?php echo $objCommon->html2text($allPostmetricHost['t_name'])?></td>
                                    <td> 
                                    	<table width="100%" class="nested-table">
                                            <tr>
                                            	<td width="30.33%">
                                                    <?php echo $totalBoysHostelsPostMetric; ?>
                                                </td>
                                                <td width="30.33%">
                                                    <?php echo $totalGirlsHostelsPostMetric; ?>
                                                </td>
                                                <td width="30.33%">
                                                    <a href="<?php echo SITE_ROOT.'single-hostels/'.$objCommon->html2text($allPostmetricHost['t_alias']).'-'.$objCommon->html2text($allPostmetricHost['t_id']).'/post-metric-hostels'?>" class="" title="Read more"><?php echo $totalHostelsPostMetric ?></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td> 
                                    	<table width="100%" class="nested-table">
                                            <tr>
                                            	<td width="30.33%">
                                                    <?php echo $totalBoysCountPostMetric;?>
                                                </td>
                                                <td width="30.33%">
                                                    <?php echo $totalGirlsCountPostMetric; ?>
                                                </td>
                                                <td width="30.33%">
                                                    <?php echo $totalStudentCountPostMetric?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
						<?php
						}
						?>
                        </div>
						
                      <div role="tabpanel" class="tab-pane" id="residential">
					  <?php
						$getResidHost			=	$objHostels->listQuery("SELECT tab1.t_name,tab1.t_id,tab1.t_alias,SUM(tab1.boysHostels) AS totalBhostels,SUM(tab1.girlsHostels) AS totalGhostels,SUM(tab1.boysCount) AS boysCount,SUM(tab1.girlsCount) AS totalGirls FROM
(
SELECT tab.t_name,tab.t_id,tab.t_alias,CASE  WHEN tab.hostel_type=1 THEN tab.hostelCount  ELSE  0  END AS boysHostels ,CASE  WHEN tab.hostel_type=2 THEN tab.hostelCount  ELSE  0  END AS girlsHostels,CASE  WHEN tab.hostel_type=1 THEN tab.studentCount  ELSE  0  END AS boysCount ,CASE  WHEN tab.hostel_type=2 THEN tab.studentCount  ELSE  0  END AS girlsCount 
FROM (
SELECT h.*,t.t_name,t.t_alias,COUNT(h.hostel_type) AS hostelCount,SUM(h.hostel_strength) AS studentCount FROM hostels AS h 
LEFT JOIN taluk AS t ON h.t_id = t.t_id
WHERE cat_id=3 AND hostel_status=1  GROUP BY h.hostel_type,h.t_id ORDER by t.t_name
) AS tab 
)AS tab1 GROUP BY t_id");
if(count($getResidHost) >0){
						?>
                        <table class="table hostels-table">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Taluk</th>
                                    <th>
                                    <table width="100%" class="inside-table">
                                        <tr>
                                            <th width="50%">Residential Schools</th>
                                            <th width="50%">Residentail Student</th>
                                        </tr>
                                    </table>
                                    </th>
                                    </tr>
                                    </thead>
                                     <tbody class="border-under">
									<?php 
									$totalHostelsResid	=	$totalStudentCountResid	=	0;
									foreach($getResidHost as $keyResid=>$allResidHost){
										$totalBoysResid							=	($allResidHost['totalBhostels'] >0)?$allResidHost['totalBhostels']:0;
										$totalGirlsResid			   			   =	($allResidHost['totalGhostels'] >0)?$allResidHost['totalGhostels']:0;
										$totalHostelsResid						 =	$totalBoysResid+$totalGirlsResid;
										$totalBoysResid				  			=	($allResidHost['boysCount'] >0)?$allResidHost['boysCount']:0;
										$totalGirlsResid						   =	($allResidHost['totalGirls'] >0)?$allResidHost['totalGirls']:0;
										$totalStudentCountResid			   		=	$totalBoysResid+$totalGirlsResid;	
									?>
                                    <tr>
                                    <td><?php echo ($keyResid+1);?></td>
                                    <td><?php echo $objCommon->html2text($allResidHost['t_name'])?></td>
                                    <td> 
                                    	<table width="100%" class="nested-table">
                                            <tr>
                                            	<td width="30.33%">
                                                    <a href="<?php echo SITE_ROOT.'residential-schools/'.$objCommon->html2text($allPostmetricHost['t_alias']).'-'.$objCommon->html2text($allPostmetricHost['t_id'])?>"><?php echo $totalHostelsResid; ?></a>
                                                </td>
                                                <td width="30.33%">
                                                    <?php echo $totalStudentCountResid; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
							<?php
							}
							?>
                        </div>
                    </div>
                
                </div>
          	 </div>
        </div>
    </div>
</div>
<!---End Middle Section--->