<!--mini banners-->
<div class="mini-banners" style="background-image:url('assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners-->
<!---Middle Section-->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Organisational Structure</h3>
                    </div>
                </div>
                
          	 </div>
            <div class="col-sm-4">
                <div class="sidebar_right">
                    <div class="head-sidebar yellowGradient">
                        <h4>Notifications
                        	<span class="pull-right nav-news"><i class="fa fa-chevron-left" id="news-left"></i><i class="fa fa-chevron-right" id="news-right"></i></span>
                            <div class="clearfix"></div>
                         </h4>
                    </div>
                    <div class="body-sidebar">
                        <ul class="list-unstyled" id="nt-example1">
                            <?php for($i=0; $i<4; $i++){ ?>
                                <li>
                                    <div class="date_published text-center">
                                        <div class="day">
                                            <h4>08</h4>
                                        </div>
                                        <div class="month">
                                            <span>DEC</span>
                                        </div>
                                    </div>
                                    <div class="notification-list">
                                        <h5><a href="#" title="News Heading">DIY writers pick out the biggest and best new songs..</a></h5>
                                        <p><a href="#" title="News Deatils">Lorem Ipsum is simply dummy text of the printing and typesetting industry</a></p>
                                        <a href="#" class="read_more" title="Read More">Read More <i class="fa fa-caret-right"></i></a>
                                    </div>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
                <div class="sidebar_right">
                	<div class="pin_locale text-center">
                    	<a href="#" title="pins of all SC n ST">Pins of all SC n ST colonies</a>
                    </div>
                </div>
                <div class="sidebar_right">
                    <div class="head-sidebar yellowGradient">
                        <h4>Scheduled Castes/Scheduled Tribes Caste list</h4>
                    </div>
                    <div class="body-sidebar">
                        <ul class="list-unstyled cast-list">
                            <?php for($i=0; $i<3; $i++){ ?>
                                <li>
                                    <div class="notification-list full-width">
                                        <h5><a href="#" title="News Heading">DIY writers pick out the biggest and best new songs best new songs.. <span class="newNotify">New</span></a></h5>
                                    </div>
                                </li>
                            <?php }?>
                            <li>
                                <div class="notification-list full-width">
                                    <h5><a href="#" title="News Heading">DIY writers pick out the biggest and best new songs best new songs.. </a></h5>
                                </div>
                            </li>
                        </ul>
                        <a href="#" class="read_more pull-right" title="Read More">Read More <i class="fa fa-caret-right"></i></a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---End Middle Section-->