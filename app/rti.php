<?php
$objRti			   			 =	new rti();
include_once(DIR_ROOT."config/class/pagination-class-front.php");
$sqlrti               			 =	" SELECT * FROM rti_main WHERE rtmain_status=1 ORDER BY rtmain_order ASC";
$num_results_per_page           =	10;
$num_page_links_per_page        =	5;
$pg_param 					   =	"";
$pagesection				    =	'';
pagination($sqlrti,$num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$rtiList			   			=	$objRti->listQuery($pg_result);
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-12">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>RTI FORMAT</h3>
                    </div>
                </div>
                <div>
                <div class="table-details table-responsive">
                	<table class="table">
                        <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>RTI Name</th>
                                <th width="30%">RTI Number</th>
                                <th width="15%">View</th>
                                <th width="15%">Download</th>
                            </tr>
                        </thead>
                        <tbody class="border-under">
							<?php
							$pageIdUri		=	htmlspecialchars($_SERVER['REQUEST_URI']);
							$pageIdUriExl	 =	explode('?page_id=',$pageIdUri);
							$pageId		   =	$pageIdUriExl[1];
								if(count($rtiList)>0){
									$num	  =	1;
									$mult	 =	(int)($pageId)?($pageId-1):0;
									$num	  =	($num_results_per_page*$mult)+1;
									foreach($rtiList as $allRti){
									?>
									<tr class="vMiddele">
										<td><?php echo $num ?></td>
										<td><?php echo $objCommon->html2text($allRti['rtmain_title']);?></td>
                              <td colspan="3" style="padding:0;">
   										<table class="table table-bordered" style="margin-bottom:0; background:none;">
                                    <?php
                                       $rtiFiles   =  $objRti->getAll("rtmain_id=".$allRti['rtmain_id']);
                                       foreach ($rtiFiles as $files) {
                                    ?>
   										   <tr>
   										      <td width="30%"><?php echo $objCommon->html2text($files['rti_number']);?></td>
   										      <td width="15%"><a href="<?php echo SITE_ROOT.'assets/uploads/pdf/'.$objCommon->html2text($files['rti_file']);?>" target="_blank" class="read_more read-alt red-gradient">View</a></td>
   										      <td width="15%"><a href="<?php echo SITE_ROOT.'library/download_file/download_email_attach.php?attach_file='.$objCommon->html2text($files['rti_file']).'&file_name=pdf'?>" class="read_more read-alt red-gradient">Download</a></td>
   										   </tr>
                                    <?php } ?>
   										</table>
                              </td>
									</tr>
									<?php
									$num++;
									 }
									 ?>
									 <tr><td colspan="5"><div class="paginationDiv pull-right"><?php echo $pagination_output;?></div></td></tr>
									 <?php
								}else{
									echo '<tr><td colspan="5">No Content found</td></tr>';
								}
								?>
                        </tbody>
                    </table>
                </div>

                    <div class="clearfix"></div>
                </div>
          	 </div>

        </div>
    </div>
</div>
<!---End Middle Section--->
