<?php
$objContactCategory     =   new contact_category();
$objContactOfficers     =   new contact_offices();
$cateList               =   $objContactCategory->getAll("ccat_status=1");
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalor">
                    <div class="head-message">
                        <h3>District</h3>
                    </div>
                </div>
                <div class="contact-page">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                    <?php
                        $i=0;
                        foreach($cateList as $cat) {
                            ?>
                            <li role="presentation"<?php echo ($i==0)?' class="active"':''; ?>><a href="#<?php echo $cat['ccat_alias']; ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $cat['ccat_name']; ?></a></li>
                            <?php
                            $i=1;
                        }
                    ?>
                    </ul>
                    
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php $j=0; foreach($cateList as $catCont){ ?>
                        <div role="tabpanel" class="tab-pane<?php echo ($j==0)?' active':''; ?>" id="<?php echo $catCont['ccat_alias']; ?>">
                        	<div class="table-responsive">
                                <table class="table contact-table">
                                    <thead>
                                        <tr>
                                            <th>Officer Name & Designation</th>
                                            <th>Contact Info</th>
											<th>&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody class="border-under contact-table">
                                        <?php
                                        $officerList    =   $objContactOfficers->getAll("ccat_id=".$catCont['ccat_id']);
                                        foreach($officerList as $officer){ ?>
                                        <tr>
										<td><?php echo $objCommon->displayEditor($officer['officer_info']); ?></td>
                                        <td><?php echo $objCommon->displayEditor($officer['contact_info']); ?></td>
										 <td>
                                            <div class="officer-image">
                                                <img class="img-responsive center-block" width="200" src="<?php echo SITE_ROOT?>assets/uploads/officers/<?php echo ($officer['officer_image']!="")?$officer['officer_image']:'user.jpg'; ?>" />
                                            </div>
                                        </td>
                                        </tr>
                                        <?php $j=1;}?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
          	 </div>
			<?php
				include_once(DIR_ROOT."app/widget/right_sidebar.php");
            ?>
          
        </div>
    </div>
</div>
<!---End Middle Section--->