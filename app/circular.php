<?php
$objCirculars			   	   =	new circulars();
include_once(DIR_ROOT."config/class/pagination-class-front.php");
$pageIdUri		=	htmlspecialchars($_SERVER['REQUEST_URI']);
$pageIdUriExl	 =	explode('?page_id=',$pageIdUri);
$pageId		   =	$pageIdUriExl[1];
$pageIdUriSearch  =	explode('?search=',$pageIdUriExl[0]);
if(count($pageIdUriSearch)>0){
	$searchKeyword			 =	trim($pageIdUriSearch[1],"/");
	if($searchKeyword){
		$searchSql			  =	" AND (circular_title LIKE '%".$searchKeyword."%' OR circular_alias LIKE '%".$searchKeyword."%') ";
	}
}
$sqlCirculars               	   =	"SELECT * FROM circulars WHERE circular_status=1 ".$searchSql." ORDER BY circular_order";
$num_results_per_page           =	10;
$num_page_links_per_page        =	5;
$pg_param 					   =	"";
$pagesection				    =	'';
pagination($sqlCirculars,$num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$circularList			   	   =	$objCirculars->listQuery($pg_result);
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('<?php echo SITE_ROOT ?>assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="local-search">
            <div class="search-bar pull-right form-group">
            	<form action="circular" method="GET" id="searchCircular">
                    <input type="text" placeholder="Search Circular" class="form-control" id="searchval" value="<?php echo $searchKeyword?>" />
                    <input type="image" class="search-btn" src="<?php echo SITE_ROOT ?>assets/images/search.png" alt="Search" />
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Goverment Circulars</h3>
                    </div>
                    	<div class="notification-lists">
                        	<ul class="list-unstyled">
							<?php 
								if(count($circularList)>0){
									$num	  =	1;
									$mult	 =	(int)($pageId)?($pageId-1):0;
									$num	  =	($num_results_per_page*$mult)+1;
									foreach($circularList as $allCirculars){
									?>
                            	<li class="notication-sort pdf-lg">
                                	<div class="date_published text-center">
                                    	<div class="day pdf-lg">
                                        	<h4><?php echo $num; ?></h4>
                                        </div>
                                    </div>
                                    <div class="notify-details">
                                    	<div class="notication-detials">
                                             <h4 class="media-heading"><?php echo $objCommon->html2text($allCirculars['circular_title']);?></h4>
                                             <p><?php echo strip_tags($objCommon->limitWords($allCirculars['circular_content'],150));?></p>
                                         </div>
                                         <span class="date-full pull-left"><?php echo date("d/m/Y",strtotime($allCirculars['circular_date']));?></span>
										 <a href="<?php echo SITE_ROOT.'single-circular/'.$objCommon->html2text($allCirculars['circular_alias']).'-'.$objCommon->html2text($allCirculars['circular_id'])?>" class="read_more read-alt red-gradient pull-right" title="Read more">Read More</a>
                                         <a href="<?php echo SITE_ROOT.'assets/uploads/circulars/'.$objCommon->html2text($allCirculars['circular_file']);?>" target="_blank" class="read_more read-alt red-gradient pull-right" title="View Pdf">View PDF</a>
                                         <a href="<?php echo SITE_ROOT.'library/download_file/download_email_attach.php?attach_file='.$objCommon->html2text($allCirculars['circular_file']).'&file_name=circulars'?>" class="read_more read-alt red-gradient pull-right" title="Download Pdf">Download</a>
                                         <div class="clearfix"></div>
                                    </div>
                                </li>
                                <?php 
								$num++;
									}
									?>
									 <div class="paginationDiv pull-right"><?php echo $pagination_output;?></div>
									 <?php
								}else{
									echo "<p>No content found</p>";
								}
								?>
                            </ul>
                        </div>
                    <div class="clearfix"></div>
                </div>
           </div>
             <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->