<?php
$objCirculars			   	  		  =	new circulars();
$id									=	$objCommon->esc($_GET['id']);
if($id){
	$idSlug							=	$objCommon->getIdAlias($id);
	$getCirculars					  =	$objCirculars->getRowSql("SELECT * FROM circulars WHERE circular_id=".$idSlug[0]." AND circular_alias='".$idSlug[1]."'");
}
?>
<!--mini banners--->
<div class="mini-banners" style="background-image:url('assets/images/mini-banner/1.jpg');">
</div>
<!---End mini banners--->
<!---Middle Section--->
<div class="container">
    <div class="middle_sec">
        <div class="row">
            <div class="col-sm-8">
                <div class="about_mangalore">
                    <div class="head-message">
                        <h3>Goverment Circulars</h3>
                    </div>
                    	<div class="notification-lists">
						<?php
						if($getCirculars['circular_id']){
						?>
                        	<ul class="list-unstyled">
                            	<li class="notication-sort">
                                	<div class="date_published text-center">
                                    	<div class="day">
                                        	<h4><?php echo date("d",strtotime($getCirculars['circular_date']));?></h4>
                                        </div>
                                        <div class="month">
                                        	<span><?php echo date("M",strtotime($getCirculars['circular_date']));?></span>
                                        </div>
                                    </div>
                                    <div class="notify-details">
                                    	<div class="notication-detials">
                                             <h4 class="media-heading"><?php echo $objCommon->html2text($getCirculars['circular_title']);?></h4>
                                         </div>
                                         <div class="footer-notofication">
                                             <span class="date-full pull-left"><?php echo date("d/m/Y",strtotime($getCirculars['circular_date']));?></span>
                                             <a href="<?php echo SITE_ROOT.'assets/uploads/circulars/'.$objCommon->html2text($getCirculars['circular_file']);?>" target="_blank" class="read_more read-alt red-gradient pull-right" title="View Pdf">View PDF</a>
                                             <a href="<?php echo SITE_ROOT.'library/download_file/download_email_attach.php?attach_file='.$objCommon->html2text($getCirculars['circular_file']).'&file_name=circulars'?>" class="read_more read-alt red-gradient pull-right" title="Download Pdf">Download</a>
                                             <div class="clearfix"></div>
                                         </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="notification-in-detail">
                            	<?php echo $objCommon->displayEditor($getCirculars['circular_content']);?>
                            </div>
						<?php
						}else{
							echo '<p>No content found</p>';
						}
						?>
                        </div>
                    <div class="clearfix"></div>
                </div>
           </div>
             <?php
		   include_once(DIR_ROOT."app/widget/right_sidebar.php");
		   ?>
        </div>
    </div>
</div>
<!---End Middle Section--->